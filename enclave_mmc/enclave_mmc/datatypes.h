#pragma once

#include "sgx_report.h"
#include "sgx_eid.h"
#include "sgx_ecp_types.h"
#include "sgx_dh.h"
#include "sgx_tseal.h"

#define FOR(i_, n_) for(int (i_)=0; (i_)<(n_); i_++)

#define MK_ERROR(x)              (0x00000000|(x))

typedef enum _enclave_status_t {
	ENCLAVE_SUCCESS = MK_ERROR(0x0000),

	ENCLAVE_ERROR_DIMENSIONS = MK_ERROR(0x0001),
	ENCLAVE_ERROR_UNSEALING = MK_ERROR(0x0002),  // Some problems happened while unsealing
	ENCLAVE_ERROR_PARAMETERS = MK_ERROR(0x0003),
	ENCLAVE_ERROR_INDEX = MK_ERROR(0x0004),
	ENCLAVE_ERROR_SEALING = MK_ERROR(0x0005),
	ENCLAVE_ERROR_NOT_IMPLEMENTED = MK_ERROR(0x0006),
	ENCLAVE_ERROR_MEMORY_ALLOCATION = MK_ERROR(0x0007),
} enclave_status_t;

typedef enum _ocall_status_t {
	OCALL_SUCCESS = MK_ERROR(0x0000),

} ocall_status_t;

typedef struct _sealed_block_matrix_t {
	uint32_t n_rows;
	uint32_t n_cols;
	size_t sz_sealed_data; // not include the meta-data
	uint8_t sealed_data[]; // only the matrix, may be meta-data should also be GSM-protected?
} sealed_block_matrix_t;

// TODO: Should it have a integrity check? 
// Maybe do it with remote attestation
typedef struct _sealed_divided_matrix_t {
	uint32_t n_rows;
	uint32_t n_cols;
	uint32_t n_rows_blocks;
	uint32_t n_cols_blocks;
	uint32_t n_rows_each_block;
	uint32_t n_cols_each_block;
	size_t sz_each_plain_block;
	size_t sz_each_sealed_block;
	size_t sz_itself;
	sealed_block_matrix_t* sealed_blocks[];
} sealed_divided_matrix_t;

typedef struct _sealed_tensor_t {
	uint32_t n_batches;
	uint32_t n_channels;
	uint32_t n_rows;
	uint32_t n_cols;
	size_t sz_each_plain_block;
	size_t sz_each_sealed_block;
	size_t sz_itself;
	sealed_block_matrix_t* sealed_blocks[];
} sealed_tensor_t;

typedef enum _elem_funcname_t {
	SIGMOID,
	SIGMOID_DIFF,
	TANH,
	TANH_DIFF,
	RELU,
	RELU_DIFF,
	BIN_CROSSENTROPY_WITH_SIGMOID,
	BIN_CROSSENTROPY_WITH_SIGMOID_DIFF,
	EXPONENTIAL_ADD,
	EXPONENTIAL_DIVIDE,
	MAX,
	MIN,
	ONES,
	ADD,
	ADD_TO_A,
	SUBTRACT_AB,
	SUBTRACT_BA,
	PRODUCT,
	IDENTICAL,
	SIGMOID_DIFFA_B,
	MAX_POOLING,
	BACKWARD_MAX_POOLING,
	AVERAGE_POOLING,
	BACKWARD_AVERAGE_POOLING,
	NUM_TYPE_ELEM_FUNC
} elem_funcname_t;

typedef enum _index_function_t {
	ROW_MAJOR,
	COL_MAJOR,
	NUM_TYPE_INDEX_FUNC
} index_function_t;
