#include "datatypes.h"
#include "enclave_mmc_t.h"

#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "stdlib.h"
#include <algorithm>
#include <cstring>
#include <cmath>
#include <functional>

#define ABS(X_) (((X_) > 0) ? (X_) : -(X_))
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define CLIP(n, min, max) MIN((MAX((n),(min))), (max))
#define SWAP(x, y, T) do { T TEMP = x; x = y; y = TEMP; } while (0)

#define CHECK_ENCLAVE(x_) \
	enclave_ret = (x_); \
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

static inline void free_allocated_memory(void *pointer)
{
	if (pointer != NULL)
	{
		free(pointer);
		pointer = NULL;
	}
}

uint32_t get_last_n_cols(sealed_divided_matrix_t* mat) {
	return mat->n_cols - (mat->n_cols_blocks - 1) * mat->n_cols_each_block;
}

uint32_t get_last_n_rows(sealed_divided_matrix_t* mat) {
	return mat->n_rows - (mat->n_rows_blocks - 1) * mat->n_rows_each_block;
}

// This function is for debugging and testing. Remove it from release version to reduce exposure surface.
uint32_t enclave_unseal_block_pointer(sealed_block_matrix_t* sealed_block, sealed_block_matrix_t* retrieve_sealed_block, size_t sz_sealed_block) {
	sgx_status_t status_ret;
	sealed_block_matrix_t* enclave_sealed_block = (sealed_block_matrix_t*)malloc(sz_sealed_block);
	status_ret = get_memory_ocall(NULL, (uint8_t*) sealed_block, (uint8_t*) enclave_sealed_block, sz_sealed_block);
	memcpy(retrieve_sealed_block, enclave_sealed_block, sz_sealed_block);
	return ENCLAVE_SUCCESS;
}

uint32_t enclave_calc_sealed_block_matrix_size(uint32_t n_rows, uint32_t n_cols) {
	uint32_t sz_sealed_data = sgx_calc_sealed_data_size(0, n_rows * n_cols * sizeof(float));
	return sizeof(sealed_block_matrix_t) + sz_sealed_data;
}

uint32_t enclave_create_sealed_block_matrix(
	sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, 
	uint32_t n_rows, uint32_t n_cols,
	float* plain_matrix, size_t sz_plain_matrix) 
{
	uint32_t sz_sealed_data = sgx_calc_sealed_data_size(0, n_rows * n_cols * sizeof(float));
	sealed_matrix->n_rows = n_rows;
	sealed_matrix->n_cols = n_cols;
	sealed_matrix->sz_sealed_data = sz_sealed_data;
	if (n_rows * n_cols * sizeof(float) != sz_plain_matrix) {
		return ENCLAVE_ERROR_DIMENSIONS;
	}
	sgx_seal_data(
		0, NULL, 
		sz_plain_matrix, 
		(uint8_t*)plain_matrix, 
		sz_sealed_data, 
		(sgx_sealed_data_t*)sealed_matrix->sealed_data);
	return ENCLAVE_SUCCESS;
}

uint32_t get_sealed_block_from_untrusted( 
	sealed_divided_matrix_t* sealed_divided_matrix, 
	sealed_block_matrix_t* sealed_block_matrix, 
	float* plain_matrix, uint32_t index_block) 
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// copy the sealed block from untrusted zone
	proc_ret = get_memory_ocall(
		&ocall_ret, 
		(uint8_t*) sealed_divided_matrix->sealed_blocks[index_block], 
		(uint8_t*) sealed_block_matrix, 
		sealed_divided_matrix->sz_each_sealed_block);
	if ((proc_ret != SGX_SUCCESS) || (ocall_ret != OCALL_SUCCESS)) {
		memset(sealed_block_matrix, 0, sealed_divided_matrix->sz_each_sealed_block);
		return ENCLAVE_ERROR_UNSEALING;
	}

	// Unseal the matrix
	proc_ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_block_matrix->sealed_data, NULL, 0, (uint8_t*) plain_matrix, &(sealed_divided_matrix->sz_each_plain_block));
	if (proc_ret != SGX_SUCCESS) {
		memset(sealed_block_matrix, 0, sealed_divided_matrix->sz_each_sealed_block);
		memset(plain_matrix, 0, sealed_divided_matrix->sz_each_plain_block);
		return ENCLAVE_ERROR_UNSEALING;
	}

	return ENCLAVE_SUCCESS;
}

uint32_t tensor_get_sealed_block_from_untrusted( 
	sealed_tensor_t* sealed_tensor, 
	sealed_block_matrix_t* sealed_block_matrix, 
	float* plain_matrix, uint32_t index_block) 
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// copy the sealed block from untrusted zone
	proc_ret = get_memory_ocall(
		&ocall_ret, 
		(uint8_t*) sealed_tensor->sealed_blocks[index_block], 
		(uint8_t*) sealed_block_matrix, 
		sealed_tensor->sz_each_sealed_block);
	if ((proc_ret != SGX_SUCCESS) || (ocall_ret != OCALL_SUCCESS)) {
		memset(sealed_block_matrix, 0, sealed_tensor->sz_each_sealed_block);
		return ENCLAVE_ERROR_UNSEALING;
	}

	// Unseal the matrix
	proc_ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_block_matrix->sealed_data, NULL, 0, (uint8_t*) plain_matrix, &(sealed_tensor->sz_each_plain_block));
	if (proc_ret != SGX_SUCCESS) {
		memset(sealed_block_matrix, 0, sealed_tensor->sz_each_sealed_block);
		memset(plain_matrix, 0, sealed_tensor->sz_each_plain_block);
		return ENCLAVE_ERROR_UNSEALING;
	}

	return ENCLAVE_SUCCESS;
}

uint32_t place_block_to_untrusted(
	sealed_divided_matrix_t* sealed_divided_matrix, sealed_block_matrix_t* sealed_block_matrix, 
	float* plain_matrix, uint32_t index_block) {
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	enclave_ret = enclave_create_sealed_block_matrix(
		sealed_block_matrix, sealed_divided_matrix->sz_each_sealed_block,
		sealed_divided_matrix->n_rows_each_block, sealed_divided_matrix->n_cols_each_block,
		plain_matrix, sealed_divided_matrix->sz_each_plain_block);
	if (enclave_ret != ENCLAVE_SUCCESS) return ENCLAVE_ERROR_SEALING;

	proc_ret = place_memory_ocall(&ocall_ret, (uint8_t*) sealed_block_matrix, (uint8_t*) sealed_divided_matrix->sealed_blocks[index_block], sealed_divided_matrix->sz_each_sealed_block);
	if (proc_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;

	return ENCLAVE_SUCCESS;
}

uint32_t tensor_place_block_to_untrusted(
	sealed_tensor_t* sealed_tensor, sealed_block_matrix_t* sealed_block_matrix, 
	float* plain_matrix, uint32_t index_block) {
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	enclave_ret = enclave_create_sealed_block_matrix(
		sealed_block_matrix, sealed_tensor->sz_each_sealed_block,
		sealed_tensor->n_rows, sealed_tensor->n_cols,
		plain_matrix, sealed_tensor->sz_each_plain_block);
	if (enclave_ret != ENCLAVE_SUCCESS) return ENCLAVE_ERROR_SEALING;

	proc_ret = place_memory_ocall(&ocall_ret, (uint8_t*) sealed_block_matrix, (uint8_t*) sealed_tensor->sealed_blocks[index_block], sealed_tensor->sz_each_sealed_block);
	if (proc_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;

	return ENCLAVE_SUCCESS;
}

// uint32_t unload_to_sealed_block_matrix(
// 	sealed_divided_matrix_t* sealed_divided_matrix,
// 	sealed_block_matrix_t* sealed_block_matrix,
// 	float* plain_matrix,
// 	uint32_t idx_block)
// {
// 	sealed_block_matrix->n_rows = sealed_divided_matrix->n_rows_each_block;
// 	sealed_block_matrix->n_cols = sealed_divided_matrix->n_cols_each_block;
// 	sealed_block_matrix->sz_sealed_data = sealed_divided_matrix->sz_each_sealed_block;
// 	size_t sz_sealed_data;
// 	uint8_t sealed_data[]; // only the matrix, may be meta-data should also be GSM-protected?

// }

uint32_t enclave_unseal_block_matrix(
	sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, 
	float* plain_matrix, size_t sz_plain_matrix) 
{
	uint32_t ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_matrix->sealed_data, NULL, 0, (uint8_t*) plain_matrix, &sz_plain_matrix);
	return ret;
}

// Unary functions
float elem_sigmoid( float a ) { 
	return 1.0f / (1.0f + expf(-(a)));
}
float elem_sigmoid_diff( float a ) { 
	float neg_ex = exp(-(a));
	return (neg_ex / (1.0 + neg_ex)) / (1.0 + neg_ex);
}
float elem_tanh( float a ) { return tanh(a); }
float elem_tanh_diff( float a ) {
	float v = exp(a) + exp(-(a));
	return (4.0 / v) / v;
}
// TODO: the MAX by oblivious version
float elem_relu( float a ) {
	return fmaxf(0, a);
}
// TODO: Replace it by an oblivious version
float elem_relu_diff(float a) {
	return ((a) > 0.0f) ? 1.0f : 0.0f;
}
float elem_identical_unary(float a) { return a; }

typedef float(*process_func_t)( float );
typedef uint32_t(*indexing_func_t)( uint32_t, uint32_t, uint32_t, uint32_t );

uint32_t get_process_func(elem_funcname_t funcname, process_func_t* res_op) {
	switch (funcname) {
		case SIGMOID: *res_op = elem_sigmoid; break;
		case SIGMOID_DIFF: *res_op = elem_sigmoid_diff; break;
		case TANH: *res_op = elem_tanh; break;
		case TANH_DIFF: *res_op = elem_tanh_diff; break;
		case RELU: *res_op = elem_relu; break;
		case RELU_DIFF: *res_op = elem_relu_diff; break;
		case IDENTICAL: *res_op = elem_identical_unary; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}
}

uint32_t no_trans_indexing(uint32_t M, uint32_t K, uint32_t idx_M, uint32_t idx_K) {
	return idx_M * K + idx_K;
}

uint32_t trans_indexing(uint32_t M, uint32_t K, uint32_t idx_M, uint32_t idx_K) {
	return idx_K * M + idx_M;
}
	
// C = alpha * op(A) op(B) + beta * op(C)
uint32_t enclave_sealed_divided_gemm_with_functions(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C,
	uint32_t is_trans_A, uint32_t is_trans_B,
	float alpha, float beta,
	elem_funcname_t preA_function_name, elem_funcname_t preB_function_name,
	elem_funcname_t post_function_name) 
{
	// if (sealed_A->n_rows != sealed_C->n_rows) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_A->n_cols != sealed_B->n_rows) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_B->n_cols != sealed_C->n_cols) return ENCLAVE_ERROR_DIMENSIONS;

	// if (sealed_A->n_rows_each_block != sealed_C->n_rows_each_block) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_A->n_cols_each_block != sealed_B->n_rows_each_block) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_B->n_cols_each_block != sealed_C->n_cols_each_block) return ENCLAVE_ERROR_DIMENSIONS;

	// if (sealed_A->n_rows_blocks != sealed_C->n_rows_blocks) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_A->n_cols_blocks != sealed_B->n_rows_blocks) return ENCLAVE_ERROR_DIMENSIONS;
	// if (sealed_B->n_cols_blocks != sealed_C->n_cols_blocks) return ENCLAVE_ERROR_DIMENSIONS;

	// Setup the function
	process_func_t preA_func = NULL;
	process_func_t preB_func = NULL;
	process_func_t post_func = NULL;

	get_process_func(preA_function_name, &preA_func);
	get_process_func(preB_function_name, &preB_func);
	get_process_func(post_function_name, &post_func);

	uint32_t M, N, K, m, n, k;

	// TODO: Check if the following parameters consistent with all matrices
	if (is_trans_A) {
		M = sealed_A->n_cols_blocks;
		K = sealed_A->n_rows_blocks;
		m = sealed_A->n_cols_each_block;
		k = sealed_A->n_rows_each_block;
	} else {
		M = sealed_A->n_rows_blocks;
		K = sealed_A->n_cols_blocks;
		m = sealed_A->n_rows_each_block;
		k = sealed_A->n_cols_each_block;
	}

	if (is_trans_B) {
		N = sealed_B->n_rows_blocks;
		n = sealed_B->n_rows_each_block;
	} else {
		N = sealed_B->n_cols_blocks;
		n = sealed_B->n_cols_each_block;
	}


	indexing_func_t indexing_A = (is_trans_A) ? trans_indexing : no_trans_indexing;
	indexing_func_t indexing_B = (is_trans_B) ? trans_indexing : no_trans_indexing;

	float *A, *B, *C;
	size_t sz_A = sealed_A->sz_each_plain_block;
	size_t sz_B = sealed_B->sz_each_plain_block;
	size_t sz_C = sealed_C->sz_each_plain_block;
	A = (float*)malloc(sz_A);
	B = (float*)malloc(sz_B);
	C = (float*)malloc(sz_C);

	sealed_block_matrix_t *block_A, *block_B, *block_C;
	size_t sz_sealed_block_A = sealed_A->sz_each_sealed_block;
	size_t sz_sealed_block_B = sealed_B->sz_each_sealed_block;
	size_t sz_sealed_block_C = sealed_C->sz_each_sealed_block;
	block_A = (sealed_block_matrix_t*)malloc(sz_sealed_block_A);
	block_B = (sealed_block_matrix_t*)malloc(sz_sealed_block_B);
	block_C = (sealed_block_matrix_t*)malloc(sz_sealed_block_C);

	// TODO: Also perform dimensions check on the size to defend overflow attacks

	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	uint32_t unsealing_ret = SGX_SUCCESS;
	uint32_t sealing_ret = SGX_SUCCESS;
	float temp_c;

	// TODO: erease all the data when failed to unseal
	FOR(idx_M, M) FOR(idx_N, N) {

		// Unsealing C[I, J]
		// It may leak the information that beta is zero, but it is acceptable since this hyperparameter is known by the server
		int idx_block_C = idx_M * N + idx_N;
		if (beta == 0.0) {
			memset(C, 0, sz_C);
		} else { 
			enclave_ret = get_sealed_block_from_untrusted(sealed_C, block_C, C, idx_block_C);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			FOR(idx_m, m) FOR(idx_n, n) C[idx_m * n + idx_n] *= beta;
		}

		FOR(idx_K, K) {
			// Unsealing A[I, K] and B[K, J]
			//int idx_block_A = idx_M * K + idx_K;
			int idx_block_A = indexing_A(M, K, idx_M, idx_K);
			// int idx_block_B = idx_K * N + idx_N;
			int idx_block_B = indexing_B(K, N, idx_K, idx_N);

			enclave_ret = get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block_A);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
			enclave_ret = get_sealed_block_from_untrusted(sealed_B, block_B, B, idx_block_B);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			// Add A * B to C
			FOR(idx_m, m) FOR(idx_n, n) {
				temp_c = 0.0f;
				// [indexing_A(m, k, idx_m, idx_k)][indexing_B(k, n, idx_k, idx_n)]
				if (is_trans_A && is_trans_B) {
					FOR(idx_k, k) temp_c += A[idx_k * m + idx_m] * B[idx_n * k + idx_k];
				} else if (is_trans_A && !is_trans_B) {
					FOR(idx_k, k) 
						temp_c += A[idx_k * m + idx_m] * B[idx_k * n + idx_n];
				} else if (!is_trans_A && is_trans_B) {
					FOR(idx_k, k) temp_c += A[idx_m * k + idx_k] * B[idx_n * k + idx_k];
				} else {
					FOR(idx_k, k) temp_c += A[idx_m * k + idx_k] * B[idx_k * n + idx_n];
				}
				C[idx_m * n + idx_n] += alpha * temp_c;
			}
		}

		if (post_function_name != IDENTICAL) {
			FOR(idx_m, m) FOR(idx_n, n) {
				C[idx_m * n + idx_n] = post_func(C[idx_m * n + idx_n]);
			}
		}

		// Sealing the updated C[I, J]
		enclave_ret = place_block_to_untrusted(sealed_C, block_C, C, idx_block_C);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(B);
	free_allocated_memory(C);
	free_allocated_memory(block_A);
	free_allocated_memory(block_B);
	free_allocated_memory(block_C);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_divided_gemm(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C,
	uint32_t is_trans_A, uint32_t is_trans_B,
	float alpha, float beta) 
{
	return enclave_sealed_divided_gemm_with_functions(
		sealed_A, sz_sealed_A,
		sealed_B, sz_sealed_B,
		sealed_C, sz_sealed_C,
		is_trans_A, is_trans_B,
		alpha, beta,
		IDENTICAL, IDENTICAL,
		IDENTICAL);
}

// TODO: Also perform dimensions check on the size to defend overflow attacks
uint32_t enclave_sealed_divided_dimensions_check_AB(
	sealed_divided_matrix_t* sealed_A, sealed_divided_matrix_t* sealed_B, 
	uint32_t *M, uint32_t *N, uint32_t *m, uint32_t *n)
{
	// Check the dimensions
	if (sealed_A->n_rows != sealed_B->n_rows) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_A->n_cols != sealed_B->n_cols) return ENCLAVE_ERROR_DIMENSIONS;

	if (sealed_A->n_rows_each_block != sealed_B->n_rows_each_block) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_A->n_cols_each_block != sealed_B->n_cols_each_block) return ENCLAVE_ERROR_DIMENSIONS;

	if (sealed_A->n_rows_blocks != sealed_B->n_rows_blocks) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_A->n_cols_blocks != sealed_B->n_cols_blocks) return ENCLAVE_ERROR_DIMENSIONS;

	*M = sealed_A->n_rows_blocks;
	*N = sealed_A->n_cols_blocks;

	*m = sealed_A->n_rows_each_block;
	*n = sealed_A->n_cols_each_block;

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_divided_memory_allocation( 
	sealed_divided_matrix_t *sealed_A, 
	float **A, 
	sealed_block_matrix_t **block_A) {
	uint32_t sz_A_ = sealed_A->sz_each_plain_block;
	*A = (float*)malloc(sz_A_);
	memset(*A, 0, sz_A_);

	uint32_t sz_sealed_block_A_ = sealed_A->sz_each_sealed_block;
	*block_A = (sealed_block_matrix_t*)malloc(sz_sealed_block_A_);

	return ENCLAVE_SUCCESS;
}

uint32_t sealed_tensor_memory_allocation( 
	sealed_tensor_t *sealed_A, 
	float **A, 
	sealed_block_matrix_t **block_A) {
	uint32_t sz_A_ = sealed_A->sz_each_plain_block;
	*A = (float*)malloc(sz_A_);
	memset(*A, 0, sz_A_);

	uint32_t sz_sealed_block_A_ = sealed_A->sz_each_sealed_block;
	*block_A = (sealed_block_matrix_t*)malloc(sz_sealed_block_A_);

	return ENCLAVE_SUCCESS;
}

// Unary functions

void elem_add(const float *a, float *b) { *b += (*a); }
void elem_subtract_ab(const float *a, float *b) { *b = (*a) - (*b); }
void elem_subtract_ba(const float *a, float *b) { *b = (*b) - (*a); }
void elem_product(const float *a, float *b) { *b = (*b) * (*a); }
void elem_product_sigmoid_diffa_b(const float *a, float *b) { 
	float neg_ex = exp(-(*a));
	// *b = (*b) * (neg_ex / (1.0 + neg_ex)) / (1.0 + neg_ex);
	*b *= (neg_ex / (1.0 + neg_ex));
	*b /= (1.0 + neg_ex); 
}
void elem_bin_crossentropy_with_sigmoid(const float *a, float *b) {
	float neg_ex = exp(-(*b));
	(*b) = ((*a) * -log(1.0 + neg_ex) + (1 - (*a)) * (-(*b) - log(1.0 + neg_ex))) / -log(2.0);
}
void elem_bin_crossentropy_with_sigmoid_diff(const float *a, float *b) {
	float neg_ex = exp(-(*b));
	*b = 1.0 / (1.0 + neg_ex);
	(*b) = ((*a) * (1.0 - *b) + (*a - 1) * (*b)) / -log(2.0);
}

void agg_add( float *a, float *agg ) { *agg = (*a) + (*agg); }
void agg_max(float *a, float *agg) { *agg = (*a > *agg) ? (*a) : (*agg); }
void agg_min(float *a, float *agg) { *agg = (*a < *agg) ? (*a) : (*agg); }
void agg_exponential_divide(float *a, float *agg) { *a = expf(*a) / *agg; }
void agg_exponential_add( float *a, float *agg ) { *agg += expf(*a); }

// C = op(A, B)
uint32_t enclave_sealed_divided_elem_copab_alpha_beta(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C,
	float alpha, float beta,
	elem_funcname_t func_name)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Setup the function
	typedef void(*funcp_t)(const float*, float*);
	funcp_t op = NULL;

	switch (func_name) {
		case ADD: op = elem_add; break;
		case SUBTRACT_AB: op = elem_subtract_ab; break;
		case SUBTRACT_BA: op = elem_subtract_ba; break;
		case PRODUCT: op = elem_product; break;
		case BIN_CROSSENTROPY_WITH_SIGMOID: op = elem_bin_crossentropy_with_sigmoid; break;
		case BIN_CROSSENTROPY_WITH_SIGMOID_DIFF: op = elem_bin_crossentropy_with_sigmoid_diff; break;
		case SIGMOID_DIFFA_B: op = elem_product_sigmoid_diffa_b; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}

	// Dimension check
	uint32_t M, N, m, n;
	enclave_ret = enclave_sealed_divided_dimensions_check_AB(sealed_A, sealed_B, &M, &N, &m, &n);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// Memory allocation
	float *A, *B;
	sealed_block_matrix_t *block_A, *block_B;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_A, &A, &block_A);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_B, &B, &block_B);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR(idx_M, M) FOR(idx_N, N) {
		int idx_block = idx_M * N + idx_N;
		enclave_ret = get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
		enclave_ret = get_sealed_block_from_untrusted(sealed_B, block_B, B, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

		uint32_t n_rows = ((idx_M + 1) * m) > sealed_A->n_rows ? sealed_A->n_rows - idx_M * m : m;
		uint32_t n_cols = ((idx_N + 1) * n) > sealed_A->n_cols ? sealed_A->n_cols - idx_N * n : n;

		// op(A, B)
		FOR(i, n_rows) FOR(j, n_cols){
			if (func_name == ADD) {
				A[i * n + j] *= alpha;
				B[i * n + j] *= beta;
			}
			op(&A[i * n + j], &B[i * n + j]);
		}

		enclave_ret = place_block_to_untrusted(sealed_C, block_B, B, idx_block);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(B);
	free_allocated_memory(block_A);
	free_allocated_memory(block_B);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_divided_elem_bopab_alpha_beta(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	float alpha, float beta,
	elem_funcname_t func_name)
{
	return enclave_sealed_divided_elem_copab_alpha_beta(
		sealed_A, sz_sealed_A,
		sealed_B, sz_sealed_B,
		sealed_B, sz_sealed_B,
		alpha, beta,
		func_name);
}

// B = op(A, B)
uint32_t enclave_sealed_divided_elem_bopab(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	elem_funcname_t func_name) 
{
	return enclave_sealed_divided_elem_bopab_alpha_beta(
		sealed_A, sz_sealed_A,
		sealed_B, sz_sealed_B,
		1.0f, 1.0f,
		func_name);
}

uint32_t enclave_sealed_divided_geadd(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B,
	float alpha, float beta,
	elem_funcname_t func_name) 
{
	return enclave_sealed_divided_elem_bopab_alpha_beta(
		sealed_A, sz_sealed_A,
		sealed_B, sz_sealed_B,
		alpha, beta,
		ADD);
}

uint32_t enclave_sealed_divided_elem_bopa(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, 
	sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, 
	elem_funcname_t func_name)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Setup the function
	// typedef void(*funcp_t)(float*);
	process_func_t op = NULL;

	switch (func_name) {
		case SIGMOID: op = elem_sigmoid; break;
		case SIGMOID_DIFF: op = elem_sigmoid_diff; break;
		case TANH: op = elem_tanh; break;
		case TANH_DIFF: op = elem_tanh_diff; break;
		case RELU: op = elem_relu; break;
		case RELU_DIFF: op = elem_relu_diff; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}

	// Dimension check
	uint32_t M, N, m, n;
	M = sealed_A->n_rows_blocks;
	N = sealed_A->n_cols_blocks;
	m = sealed_A->n_rows_each_block;
	n = sealed_A->n_cols_each_block;

	// Memory allocation
	float *A;
	sealed_block_matrix_t *block_A;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_A, &A, &block_A);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR(idx_M, M) FOR(idx_N, N) {
		int idx_block = idx_M * N + idx_N;
		enclave_ret = get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

		uint32_t n_rows = ((idx_M + 1) * m) > sealed_A->n_rows ? sealed_A->n_rows - idx_M * m : m;
		uint32_t n_cols = ((idx_N + 1) * n) > sealed_A->n_cols ? sealed_A->n_cols - idx_N * n : n;

		// op(A, B)
		FOR(i, n_rows) FOR(j, n_cols) A[i * n + j] = op(A[i * n + j]);;

		enclave_ret = place_block_to_untrusted(sealed_B, block_A, A, idx_block);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(block_A);

	return ENCLAVE_SUCCESS;
}

// A = op(A)
uint32_t enclave_sealed_divided_elem_aopa( 
	sealed_divided_matrix_t* sealed_A, 
	size_t sz_sealed_A, 
	elem_funcname_t func_name ) 
{
	return enclave_sealed_divided_elem_bopa(
		sealed_A, sz_sealed_A,
		sealed_A, sz_sealed_A,
		func_name);
}

// Index functions, assuming it is used in a for loop with index i
//uint32_t row_major_first( uint32_t M, uint32_t N) { return M; }
//uint32_t col_major_first( uint32_t M, uint32_t N) { return N; }
//uint32_t row_major_second( uint32_t M, uint32_t N) { return N; }
//uint32_t col_major_second( uint32_t M, uint32_t N) { return M; }
//uint32_t row_index( uint32_t N, uint32_t index_first, uint32_t index_second) { return index_first * N + index_second; }
//uint32_t col_index( uint32_t N, uint32_t index_first, uint32_t index_second) { return index_second * N + index_first; }

// B = op(A), aggregation
// res_agg is the vector of aggragating results
// pre_add is the vector of numbers that should add to each element before aggragation
// TODO: Remove sz_sealed_A, which is useless for an internal function
uint32_t sealed_divided_aggregate_rows(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	float* res_agg,
	float* pre_add,
	elem_funcname_t agg_func_name)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Setup the function
	typedef void(*agg_func_t)( float*, float* );
	agg_func_t agg_op;

	switch (agg_func_name) {
		case EXPONENTIAL_ADD: agg_op = agg_exponential_add; break;
		case MAX: agg_op = agg_max; break;
		case MIN: agg_op = agg_min; break;
		case ADD: agg_op = agg_add; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}

	// Dimension check
	uint32_t M, N, m, n;
	M = sealed_A->n_rows_blocks;
	N = sealed_A->n_cols_blocks;
	m = sealed_A->n_rows_each_block;
	n = sealed_A->n_cols_each_block;

	// Memory allocation
	float *A;
	sealed_block_matrix_t *block_A;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_A, &A, &block_A);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR(idx_M, M) {
		// memset(B, 0, sealed_B->sz_each_plain_block);
		FOR(idx_N, N) {
			int idx_block = idx_M * N + idx_N;
			enclave_ret = get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			uint32_t n_rows = ((idx_M + 1) * m) > sealed_A->n_rows ? sealed_A->n_rows - idx_M * m : m;
			uint32_t n_cols = ((idx_N + 1) * n) > sealed_A->n_cols ? sealed_A->n_cols - idx_N * n : n;

			// Use the first elem in each row as the base case
			if (idx_N == 0) {
				if (agg_func_name == MAX || agg_func_name == MIN) {
					FOR(i, n_rows) 
						res_agg[idx_M * m + i] = A[i * n];
				}
			}

			if (pre_add == NULL) {
				FOR(i, n_rows) FOR(j, n_cols) 
					agg_op(&A[i * n + j], &res_agg[idx_M * m + i]);
			} else {
				float temp_a;
				FOR(i, n_rows) FOR(j, n_cols) {
					temp_a = A[i * n + j] + pre_add[idx_M * m + i];
					agg_op(&temp_a, &res_agg[idx_M * m + i]);
				}
			}
		}
		// enclave_ret = place_block_to_untrusted(sealed_B, block_B, B, idx_M);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(block_A);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_divided_aggregate_rows(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	float* res_agg, size_t sz_sealed_res_agg,
	elem_funcname_t agg_func_name) 
{
	return sealed_divided_aggregate_rows(
		sealed_A, sz_sealed_A,
		res_agg,
		NULL,
		agg_func_name);
}

uint32_t enclave_sealed_divided_aggregate_rows_exponetial_add(
	sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A,
	float* res_agg, size_t sz_res_agg,
	float* pre_add, size_t sz_pre_add)
{
	return sealed_divided_aggregate_rows(
		sealed_A, sz_sealed_A,
		res_agg,
		pre_add,
		EXPONENTIAL_ADD);
}

// C = alpha * op(A) op(B) + beta * op(C)
uint32_t enclave_sealed_block_gemm(
	sealed_block_matrix_t* sealed_A, size_t sz_sealed_A,
	sealed_block_matrix_t* sealed_B, size_t sz_sealed_B,
	sealed_block_matrix_t* sealed_C, size_t sz_sealed_C,
	uint32_t is_trans_A, uint32_t is_trans_B,
	float alpha, float beta) 
{
	uint32_t unsealing_ret = SGX_SUCCESS;
	float *A, *B, *C;
	size_t sz_A = sealed_A->n_rows * sealed_A->n_cols * sizeof(float);
	size_t sz_B = sealed_B->n_rows * sealed_B->n_cols * sizeof(float);
	size_t sz_C = sealed_C->n_rows * sealed_C->n_cols * sizeof(float);
	A = (float*)malloc(sz_A);
	B = (float*)malloc(sz_B);
	C = (float*)malloc(sz_C);
	unsealing_ret |= sgx_unseal_data((sgx_sealed_data_t*) sealed_A->sealed_data, NULL, 0, (uint8_t*) A, &sz_A);
	unsealing_ret |= sgx_unseal_data((sgx_sealed_data_t*) sealed_B->sealed_data, NULL, 0, (uint8_t*) B, &sz_B);
	unsealing_ret |= sgx_unseal_data((sgx_sealed_data_t*) sealed_C->sealed_data, NULL, 0, (uint8_t*) C, &sz_C);

	// TODO: erase the unsealed data
	if (unsealing_ret != SGX_SUCCESS) {
		return ENCLAVE_ERROR_UNSEALING;
	}

	int trans_m, trans_n, trans_k;
	trans_m = sealed_A->n_rows;
	trans_n = sealed_B->n_cols;
	trans_k = sealed_B->n_rows;

	// TODO: Perform dimensions check

	float tmp_res;
	for (int i = 0; i < trans_m; i++) {
		for (int j = 0; j < trans_n; j++) {
			tmp_res = 0.0;
			for (int p = 0; p < trans_k; p++) {
				tmp_res += A[i * trans_k + p] * B[p * trans_n + j];
			}
			C[i * trans_n + j] *= beta;
			C[i * trans_n + j] += alpha * tmp_res;
		}
	}

	unsealing_ret |= sgx_seal_data(0, NULL, sz_C, (uint8_t*) C, sealed_C->sz_sealed_data, (sgx_sealed_data_t*) sealed_C->sealed_data);
	free_allocated_memory(A);
	free_allocated_memory(B);
	free_allocated_memory(C);
	return 0;
}

// C = alpha * op(A) op(B) + beta * op(C)
uint32_t enclave_sealed_gemm(
	uint8_t* sealed_A, uint8_t* sealed_B, uint8_t* sealed_C,
	uint32_t is_A_trans, uint32_t is_B_trans,
	float alpha, float beta,
	size_t sz_sealed_A, size_t sz_sealed_B, size_t sz_sealed_C, 
	size_t sz_A, size_t sz_B, size_t sz_C,
	int m, int n, int k)
{
	uint32_t unsealing_ret = SGX_SUCCESS;
	float *A, *B, *C;
	A = (float*)malloc(sz_A);
	B = (float*)malloc(sz_B);
	C = (float*)malloc(sz_C);
	unsealing_ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_A, NULL, 0, (uint8_t*) A, &sz_A);
	unsealing_ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_B, NULL, 0, (uint8_t*) B, &sz_B);
	unsealing_ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_C, NULL, 0, (uint8_t*) C, &sz_C);

	int trans_m, trans_n, trans_k;
	trans_m = m;
	trans_n = n;
	trans_k = k;
	float tmp_res;
	for (int i = 0; i < trans_m; i++) {
		for (int j = 0; j < trans_n; j++) {
			tmp_res = 0.0;
			for (int p = 0; p < trans_k; p++) tmp_res += A[i * trans_k + p] * B[p * trans_n + j];
			C[i * trans_n + j] *= beta;
			C[i * trans_n + j] += alpha * tmp_res;
		}
	}
	unsealing_ret |= sgx_seal_data(0, NULL, sz_C, (uint8_t*) C, sz_sealed_C, (sgx_sealed_data_t*) sealed_C);
	free_allocated_memory(A);
	free_allocated_memory(B);
	free_allocated_memory(C);
	return 0;
}

inline uint32_t get_n_iters_in_block(uint32_t idx_M, uint32_t m, uint32_t n_total_row) 
{
	return ((idx_M + 1) * m) > n_total_row ? n_total_row - idx_M * m : m;
}

// The ecall for output layer of single label
// If diff_preY or acc is null, it will not be calculated
// trueY should be a 1D vector consisting of ground truth label
uint32_t enclave_output_layer(
	sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY,
	sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY,
	sealed_divided_matrix_t* sealed_diff_preY, size_t sz_sealed_diff_preY,
	float* loss)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Dimension check
	uint32_t M, N, m, n;
	M = sealed_preY->n_rows_blocks;
	N = sealed_preY->n_cols_blocks;
	m = sealed_preY->n_rows_each_block;
	n = sealed_preY->n_cols_each_block;

	uint32_t n_batches, n_labels;
	n_batches = sealed_preY->n_rows;
	n_labels = sealed_preY->n_cols;

	// Memory allocation
	float *preY;
	sealed_block_matrix_t *block_preY;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_preY, &preY, &block_preY);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	sealed_block_matrix_t *block_trueY;
	uint32_t sz_trueY = sealed_trueY->n_cols_blocks * sealed_trueY->n_cols_each_block * sizeof(float);
	block_trueY = (sealed_block_matrix_t*)malloc(sealed_trueY->sz_each_sealed_block);
	float *trueY = (float*)malloc(sz_trueY);

	float *diff_preY;
	size_t sz_diff_preY = sealed_diff_preY->sz_each_plain_block;
	sealed_block_matrix_t *block_diff_preY;
	if (sealed_diff_preY != NULL) {
		enclave_ret = enclave_sealed_divided_memory_allocation(sealed_diff_preY, &diff_preY, &block_diff_preY);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	}

	// Get the decrypted trueY
	if (sealed_trueY->n_rows > 1 || sealed_trueY->n_rows_blocks > 1) return ENCLAVE_ERROR_DIMENSIONS;
	FOR(idx_block, sealed_trueY->n_cols_blocks) {
		enclave_ret = get_sealed_block_from_untrusted(
			sealed_trueY, 
			block_trueY, 
			// trueY + (sealed_trueY->sz_each_plain_block) * idx_block, 
			&trueY[idx_block * m],
			idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	}

	size_t sz_max_each_row = n_batches * sizeof(float);
	float* max_each_row = (float*)malloc(sz_max_each_row);
	if (max_each_row == NULL) return ENCLAVE_ERROR_MEMORY_ALLOCATION;

	size_t sz_expsum = n_batches * sizeof(float);
	float* expsum = (float*)calloc(n_batches, sizeof(float));
	memset(expsum, 0, sizeof(expsum)/sizeof(float));
	if (expsum == NULL) return ENCLAVE_ERROR_MEMORY_ALLOCATION;
	
	proc_ret = (sgx_status_t) sealed_divided_aggregate_rows(
		sealed_preY, sz_sealed_preY,
		max_each_row,
		NULL,
		MAX);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// Find the maximum of each row
	FOR(i, n_batches) max_each_row[i] *= -1.0f;
	proc_ret = (sgx_status_t) sealed_divided_aggregate_rows(
		sealed_preY, sz_sealed_preY,
		expsum,
		max_each_row,
		EXPONENTIAL_ADD);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// loss = mean batch {-(a_y(i) + D[i]) + log(sum_j e^{a[j] + D[i]})}
	float sum_loss = 0.0f;
	FOR(i, n_batches) {
		sum_loss += -max_each_row[i] + logf(MIN(MAX(1e-12,expsum[i]), 1e12));
	}
	FOR(idx_M, M) FOR(idx_N, N) {
		int idx_block = idx_M * N + idx_N;
		enclave_ret = get_sealed_block_from_untrusted(sealed_preY, block_preY, preY, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

		uint32_t n_rows = get_n_iters_in_block(idx_M, m, sealed_diff_preY->n_rows);
		uint32_t n_cols = get_n_iters_in_block(idx_N, n, sealed_diff_preY->n_cols);

		// Using an unefficient loop to prevent timing attack.
		if (loss != NULL) {
			FOR(i, n_rows) {
				int idx_batch = idx_M * m + i;
				if (idx_batch >= n_batches) continue;
				int y = trueY[idx_batch];
				if (idx_N * n <= y  && y < (idx_N + 1) * n) sum_loss -= preY[i * n + (y - idx_N * n)];
			}
		}

		if (sealed_diff_preY != NULL) {
			FOR(i, n_rows) {
				int idx_batch = idx_M * m + i;
				int y = trueY[idx_batch];
				FOR(j, n_cols) {
					diff_preY[i * n + j] = exp(preY[i * n + j] + max_each_row[idx_batch]) / expsum[idx_batch];
					if (y == j + idx_N * n) diff_preY[i * n + j] -= 1.0f;
					diff_preY[i * n + j] /= (float)n_batches;
				}
			}
			// Sealing the updated diff_preY[I, J]
			enclave_ret = place_block_to_untrusted(sealed_diff_preY, block_diff_preY, diff_preY, idx_block);
			if (enclave_ret != SGX_SUCCESS) return proc_ret;
		}
	}
	*loss = sum_loss / (n_batches);

	free_allocated_memory(preY);
	free_allocated_memory(block_preY);
	free_allocated_memory(trueY);
	free_allocated_memory(block_trueY);
	if (sealed_diff_preY != NULL) {
		free_allocated_memory(diff_preY);
		free_allocated_memory(block_diff_preY);
	}

	return ENCLAVE_SUCCESS;
}

uint32_t get_the_whole_row(
	sealed_divided_matrix_t* sealed_divided_matrix, 
	float** vector,
	uint32_t* sz_vector,
	uint32_t n_elem)
{
	sealed_block_matrix_t *block_matrix;
	*sz_vector = sealed_divided_matrix->n_cols_blocks * sealed_divided_matrix->n_cols_each_block * sizeof(float);
	block_matrix = (sealed_block_matrix_t*)malloc(sealed_divided_matrix->sz_each_sealed_block);
	*vector = (float*)malloc(*sz_vector);

	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Get the decrypted vector
	if (sealed_divided_matrix->n_rows > 1 || sealed_divided_matrix->n_rows_blocks > 1) return ENCLAVE_ERROR_DIMENSIONS;
	FOR(idx_block, sealed_divided_matrix->n_cols_blocks) {
		enclave_ret = get_sealed_block_from_untrusted(
			sealed_divided_matrix, 
			block_matrix, 
			&((*vector)[idx_block * sealed_divided_matrix->n_cols_each_block]),
			idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	}

	free(block_matrix);

	return ENCLAVE_SUCCESS;
}

uint32_t get_MNmn(
	sealed_divided_matrix_t* sealed_preY,
	uint32_t* M, uint32_t* N,
	uint32_t* m, uint32_t* n)
{
	*M = sealed_preY->n_rows_blocks;
	*N = sealed_preY->n_cols_blocks;
	*m = sealed_preY->n_rows_each_block;
	*n = sealed_preY->n_cols_each_block;
	return ENCLAVE_SUCCESS;
}

// The ecall for calculating the acc of output layer of single label
uint32_t enclave_output_layer_acc(
	sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY,
	sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY,
	float* acc)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	uint32_t n_batches = sealed_preY->n_rows;

	float* trueY = NULL;
	uint32_t sz_trueY = NULL;
	enclave_ret = get_the_whole_row(
		sealed_trueY,
		&trueY,
		&sz_trueY,
		n_batches);

	size_t sz_max_indices = n_batches * sizeof(float);
	float *max_indices = (float*)malloc(sz_max_indices);
	FOR(i, n_batches) max_indices[i] = 0;
	if (max_indices == NULL) return ENCLAVE_ERROR_MEMORY_ALLOCATION;

	size_t sz_max_values = n_batches * sizeof(float);
	float* max_values = (float*)malloc(sz_max_values);
	if (max_values == NULL) return ENCLAVE_ERROR_MEMORY_ALLOCATION;

	// Dimension check
	uint32_t M, N, m, n;
	get_MNmn(sealed_preY, &M, &N, &m, &n);

	// Memory allocation
	float *preY;
	sealed_block_matrix_t *block_preY;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_preY, &preY, &block_preY);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR(idx_M, M) {
		FOR(idx_N, N) {
			int idx_block = idx_M * N + idx_N;
			enclave_ret = get_sealed_block_from_untrusted(sealed_preY, block_preY, preY, idx_block);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			uint32_t n_rows = ((idx_M + 1) * m) > sealed_preY->n_rows ? sealed_preY->n_rows - idx_M * m : m;
			uint32_t n_cols = ((idx_N + 1) * n) > sealed_preY->n_cols ? sealed_preY->n_cols - idx_N * n : n;

			// Take the first element for comparison
			if (idx_N == 0) FOR(i, n_rows) max_values[idx_M * m + i] = preY[i * n];

			FOR(i, n_rows) FOR(j, n_cols) {
				// if (preY[i * m + j] > max_values[idx_M * m + i]) {
				// 	max_values[idx_M * m + i] = preY[i * m + j];
				// 	max_indices[idx_M * m + i] = j;
				// }
				if (preY[i * n + j] > max_values[idx_M * m + i]) {
					max_values[idx_M * m + i] = preY[i * n + j];
					max_indices[idx_M * m + i] = j;
				}
			}
		}
		// enclave_ret = place_block_to_untrusted(sealed_B, block_B, B, idx_M);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	int32_t sum_acc = 0;
	FOR(i, n_batches) if (ABS(max_indices[i] - trueY[i]) < 1e-3) sum_acc += 1;
	*acc = (float)sum_acc / n_batches;

	free_allocated_memory(trueY);
	free_allocated_memory(preY);
	free_allocated_memory(block_preY);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_to_zeros(float* A, size_t sz_A, int n_elem) {
	memset(A, 0, n_elem);
	return 0;
}

uint32_t enclave_sealed_size(const uint32_t add_mac_txt_size, const uint32_t txt_encrypt_size) {
	return sgx_calc_sealed_data_size(add_mac_txt_size, txt_encrypt_size);
}

uint32_t enclave_seal_matrix(float* A, size_t sz_A, uint8_t* sealed_data, size_t sz_sealed_data) {
	uint32_t ret;
	ret = sgx_seal_data(0, NULL, sz_A, (uint8_t*) A, sz_sealed_data, (sgx_sealed_data_t*) sealed_data);
	return 0;
}

uint32_t enclave_unseal_matrix(float* A, size_t sz_A, uint8_t* sealed_data, size_t sz_sealed_data) { 
	uint32_t ret; ret = sgx_unseal_data((sgx_sealed_data_t*) sealed_data, NULL, 0, (uint8_t*) A, &sz_A); return 0;
}

// C = alpha * input conv kernal + beta * op(output)
uint32_t convolute_tensor_kernal(
	sealed_tensor_t* sealed_input, sealed_tensor_t* sealed_kernal, sealed_tensor_t* sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta,
	uint32_t is_rot180_input, uint32_t is_rot180_kernal)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_kernal, *plain_output;
	sealed_block_matrix_t *block_input, *block_kernal, *block_output;
	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_kernal, &plain_kernal, &block_kernal);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// The n_batches of kernal is regarded as the number of output channels of convolution
	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_kernal->n_channels) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_kernal->n_batches != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t kernal_M = sealed_kernal->n_rows;
	uint32_t kernal_N = sealed_kernal->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	uint32_t extra_M = ((input_M - kernal_M + 2 * padding_y) % stride_y == 0) ? 1 : 0;
	uint32_t extra_N = ((input_N - kernal_N + 2 * padding_x) % stride_x == 0) ? 1 : 0;

	if (sealed_output->n_rows != ((int) input_M - kernal_M + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) input_N - kernal_N + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t working_input_M = input_M + 2 * padding_y;
	uint32_t working_input_N = input_N + 2 * padding_x;

	size_t sz_working_input = working_input_M * working_input_N * sizeof(float);
	float* working_input = (float*)malloc(working_input_M * working_input_N * sizeof(float));

	FOR (idx_output_channel, sealed_output->n_channels) {
		FOR (idx_batches, sealed_input->n_batches) {
			// Unseal for output channel
			if (beta == 0.0f) {
				memset(plain_output, 0, sealed_output->sz_each_plain_block);
			} else { 
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_output_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
				if (beta != 1.0f) {
					FOR (y, sealed_output->n_rows) FOR (x, sealed_output->n_cols) plain_output[y * output_N + x] *= beta;
				}
			}

			FOR (idx_input_channel, sealed_input->n_channels) {

				// Unseal an input's block
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				float temp;

				if (is_rot180_input) {
					int n_elem = input_M * input_N;
					FOR(i, (input_M * input_N) / 2) {
						temp = plain_input[i];
						plain_input[i] = plain_input[n_elem - i - 1];
						plain_input[n_elem - i - 1] = temp;
					}
				}
				memset(working_input, 0, sz_working_input);
				FOR(i, input_M) {
					memcpy(&working_input[(i + padding_y) * working_input_N + padding_y], &plain_input[i * input_N], input_N * sizeof(float));
				}

				// Unseal an kernal's block
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_kernal, block_kernal, plain_kernal, idx_output_channel * sealed_kernal->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				if (is_rot180_kernal) {
					int n_elem = kernal_M * kernal_N;
					FOR(i, (kernal_M * kernal_N) / 2) {
						temp = plain_kernal[i];
						plain_kernal[i] = plain_kernal[n_elem - i - 1];
						plain_kernal[n_elem - i - 1] = temp;
					}
				}

				for (int anchor_y = 0, output_y = 0; anchor_y + kernal_M < working_input_M + extra_M; anchor_y += stride_y, output_y++) {
					for (int anchor_x = 0, output_x = 0; anchor_x + kernal_N < working_input_N + extra_N; anchor_x += stride_x, output_x++) {
						FOR(kernal_y, kernal_M) FOR(kernal_x, kernal_N) {
							int input_x = anchor_x + kernal_x;
							int input_y = anchor_y + kernal_y;

							plain_output[output_y * output_N + output_x] += alpha * working_input[input_y * working_input_N + input_x] * plain_kernal[kernal_y * kernal_N + kernal_x];
						}
					}
				}
			}

			enclave_ret = tensor_place_block_to_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_output_channel);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}

	free_allocated_memory(working_input);
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_kernal);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_kernal);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

// C = alpha * input conv kernal + beta * op(output)
uint32_t enclave_convolute_tensor_kernal(
	sealed_tensor_t* sealed_input, size_t sz_sealed_input,
	sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal,
	sealed_tensor_t* sealed_output, size_t sz_sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta,
	uint32_t is_rot180_input, uint32_t is_rot180_kernal) 
{
	return convolute_tensor_kernal(
		sealed_input, sealed_kernal, sealed_output,
		padding_y, padding_x,
		stride_y, stride_x,
		alpha, beta,
		is_rot180_input, is_rot180_kernal);
}

uint32_t backward_convolute_tensor_kernal(
	sealed_tensor_t* sealed_input, sealed_tensor_t* sealed_kernal, sealed_tensor_t* sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_kernal, *plain_output;
	sealed_block_matrix_t *block_input, *block_kernal, *block_output;
	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_kernal, &plain_kernal, &block_kernal);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_kernal->n_channels) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_kernal->n_batches != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t kernal_M = sealed_kernal->n_rows;
	uint32_t kernal_N = sealed_kernal->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	uint32_t extra_M = ((input_M - kernal_M + 2 * padding_y) % stride_y == 0) ? 1 : 0;
	uint32_t extra_N = ((input_N - kernal_N + 2 * padding_x) % stride_x == 0) ? 1 : 0;

	if (sealed_output->n_rows != ((int) input_M - kernal_M + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) input_N - kernal_N + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t working_input_M = input_M + 2 * padding_y;
	uint32_t working_input_N = input_N + 2 * padding_x;

	size_t sz_working_input = working_input_M * working_input_N * sizeof(float);
	float* working_input = (float*)malloc(sz_working_input);

	uint32_t working_output_M = output_M * stride_y - stride_y + 1;
	uint32_t working_output_N = output_N * stride_x - stride_x + 1;

	size_t sz_working_output = working_output_M * working_output_M * sizeof(float);
	float* working_output = (float*)malloc(sz_working_output);

	FOR (idx_output_channel, sealed_output->n_channels) {
		FOR (idx_input_channel, sealed_input->n_channels) { 
			// Unseal an kernal's block
			if (beta == 0.0f) {
				memset(plain_kernal, 0, sealed_kernal->sz_each_plain_block);
			} else {
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_kernal, block_kernal, plain_kernal, idx_output_channel * sealed_kernal->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
				FOR(i, sealed_kernal->n_rows * sealed_kernal->n_cols) plain_kernal[i] *= beta;
			}
			
			FOR (idx_batches, sealed_input->n_batches) {
				// Unseal for output channel
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_output_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				// int n_elem = output_M * output_N;
				// float temp;
				// FOR(i, (output_M * output_N) / 2) {
				// 	temp = plain_output[i];
				// 	plain_output[i] = plain_output[n_elem - i - 1];
				// 	plain_output[n_elem - i - 1] = temp;
				// }

				if (stride_x > 1 || stride_y > 1) {
					memset(working_output, 0, sz_working_output);
					for (uint32_t y = 0; y < output_M; y++) {
						for (uint32_t x = 0; x < output_N; x++) {
							working_output[(y * stride_y) * working_output_N + (x * stride_x)] = plain_output[y * output_N + x];
						}
					}
				} else {
					memcpy(working_output, plain_output, sz_working_output);
				}

				// Unseal an input's block
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				memset(working_input, 0, sz_working_input);
				FOR(i, input_M) {
					memcpy(&working_input[(i + padding_y) * working_input_N + padding_x], &plain_input[i * input_N], input_N * sizeof(float));
				}

				for (int anchor_y = 0, kernal_y = 0; anchor_y + working_output_M < working_input_M + extra_M; anchor_y++, kernal_y++) {
					for (int anchor_x = 0, kernal_x = 0; anchor_x + working_output_N < working_input_N + extra_N; anchor_x++, kernal_x++) {
						FOR(output_y, working_output_M) FOR(output_x, working_output_N) {
							int input_x = anchor_x + output_x;
							int input_y = anchor_y + output_y;

							plain_kernal[kernal_y * kernal_N + kernal_x] += 
								alpha * 
								working_input[input_y * working_input_N + input_x] * 
								working_output[output_y * working_output_N + output_x];
						}
					}
				}
			}

			enclave_ret = tensor_place_block_to_untrusted(sealed_kernal, block_kernal, plain_kernal, idx_output_channel * sealed_kernal->n_channels + idx_input_channel);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}

	free_allocated_memory(working_input);
	free_allocated_memory(working_output);
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_kernal);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_kernal);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_backward_convolute_tensor_kernal(
	sealed_tensor_t* sealed_input, size_t sz_sealed_input,
	sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal,
	sealed_tensor_t* sealed_output, size_t sz_sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta)
{
	return backward_convolute_tensor_kernal(
		sealed_input, sealed_kernal, sealed_output,
		padding_y, padding_x,
		stride_y, stride_x,
		alpha, beta);
}

uint32_t backward_convolute_tensor_input(
	sealed_tensor_t* sealed_input, sealed_tensor_t* sealed_kernal, sealed_tensor_t* sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_kernal, *plain_output;
	sealed_block_matrix_t *block_input, *block_kernal, *block_output;
	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_kernal, &plain_kernal, &block_kernal);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_kernal->n_channels) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_kernal->n_batches != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t kernal_M = sealed_kernal->n_rows;
	uint32_t kernal_N = sealed_kernal->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	uint32_t extra_M = ((input_M - kernal_M + 2 * padding_y) % stride_y == 0) ? 1 : 0;
	uint32_t extra_N = ((input_N - kernal_N + 2 * padding_x) % stride_x == 0) ? 1 : 0;

	if (sealed_output->n_rows != ((int) input_M - kernal_M + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) input_N - kernal_N + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t working_output_M = output_M * stride_y + 2 * (sealed_kernal->n_rows - 1 - padding_y);
	uint32_t working_output_N = output_N * stride_x + 2 * (sealed_kernal->n_cols - 1 - padding_x);

	size_t sz_working_output = working_output_M * working_output_M * sizeof(float);
	float* working_output = (float*)malloc(sz_working_output);

	FOR (idx_batches, sealed_input->n_batches) {
		FOR (idx_input_channel, sealed_input->n_channels) {

			// Unseal an input's block
			if (beta == 0.0f) {
				memset(plain_input, 0, sealed_input->sz_each_plain_block);
			} else {
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
				uint32_t n_elem = sealed_input->n_rows * sealed_input->n_cols;
				if (beta != 1.0f) FOR(i, n_elem) plain_input[i] *= beta;
			}

			FOR (idx_output_channel, sealed_output->n_channels) {
				// Unseal for output channel
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_output_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				if (stride_x > 1 || stride_y > 1) {
					memset(working_output, 0, sz_working_output);
					FOR(y, output_M) FOR(x, output_N) {
						uint32_t working_idx = (y * stride_y + kernal_M - 1 - padding_y) * working_output_N + (x * stride_x + kernal_N - 1 - padding_x);
						working_output[working_idx] = plain_output[y * output_N + x];
					}
				} else {
					memcpy(working_output, plain_output, sz_working_output);
				}

				// Unseal an kernal's block
				enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_kernal, block_kernal, plain_kernal, idx_output_channel * sealed_kernal->n_channels + idx_input_channel);
				if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

				int n_elem = kernal_M * kernal_N;
				float temp;
				FOR(i, n_elem / 2) {
					temp = plain_kernal[i];
					plain_kernal[i] = plain_kernal[n_elem - i - 1];
					plain_kernal[n_elem - i - 1] = temp;
				}

				for (int anchor_y = 0, input_y = 0; anchor_y + kernal_M < working_output_M + extra_M; anchor_y++, input_y++) {
					for (int anchor_x = 0, input_x = 0; anchor_x + kernal_N <= working_output_N + extra_N; anchor_x++, input_x++) {
						FOR(kernal_y, kernal_M) FOR(kernal_x, kernal_N) {
							int output_x = anchor_x + kernal_x;
							int output_y = anchor_y + kernal_y;

							plain_input[input_y * input_N + input_x] += alpha * working_output[output_y * working_output_N + output_x] * plain_kernal[kernal_y * kernal_N + kernal_x];
						}
					}
				}
			}

			enclave_ret = tensor_place_block_to_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_input_channel);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}

	free_allocated_memory(working_output);
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_kernal);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_kernal);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_backward_convolute_tensor_input(
	sealed_tensor_t* sealed_input, size_t sz_sealed_input,
	sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal,
	sealed_tensor_t* sealed_output, size_t sz_sealed_output,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	float alpha, float beta)
{
	return backward_convolute_tensor_input(
		sealed_input, sealed_kernal, sealed_output,
		padding_y, padding_x,
		stride_y, stride_x,
		alpha, beta);
}

// C = op(A, B)
uint32_t sealed_tensor_elem_copab_alpha_beta(
	sealed_tensor_t* sealed_A,
	sealed_tensor_t* sealed_B,
	sealed_tensor_t* sealed_C,
	float alpha, float beta,
	elem_funcname_t func_name)
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Setup the function
	typedef void(*funcp_t)(const float*, float*);
	funcp_t op = NULL;

	switch (func_name) {
		case ADD: op = elem_add; break;
		case SUBTRACT_AB: op = elem_subtract_ab; break;
		case SUBTRACT_BA: op = elem_subtract_ba; break;
		case PRODUCT: op = elem_product; break;
		case BIN_CROSSENTROPY_WITH_SIGMOID: op = elem_bin_crossentropy_with_sigmoid; break;
		case BIN_CROSSENTROPY_WITH_SIGMOID_DIFF: op = elem_bin_crossentropy_with_sigmoid_diff; break;
		case SIGMOID_DIFFA_B: op = elem_product_sigmoid_diffa_b; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}

	// Dimension check
	if (sealed_A->n_batches != sealed_B->n_batches) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_B->n_batches != sealed_C->n_batches) return ENCLAVE_ERROR_DIMENSIONS;

	if (sealed_A->n_channels != sealed_B->n_channels) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_B->n_channels != sealed_C->n_channels) return ENCLAVE_ERROR_DIMENSIONS;

	if (sealed_A->n_rows != sealed_B->n_rows) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_B->n_rows != sealed_C->n_rows) return ENCLAVE_ERROR_DIMENSIONS;

	if (sealed_A->n_cols != sealed_B->n_cols) return ENCLAVE_ERROR_DIMENSIONS;
	if (sealed_B->n_cols != sealed_C->n_cols) return ENCLAVE_ERROR_DIMENSIONS;

	uint32_t n_batches = sealed_A->n_batches;
	uint32_t n_channels = sealed_A->n_channels;
	uint32_t n_rows = sealed_A->n_rows;
	uint32_t n_cols = sealed_A->n_cols;

	// Memory allocation
	float *A, *B;
	sealed_block_matrix_t *block_A, *block_B;
	enclave_ret = sealed_tensor_memory_allocation(sealed_A, &A, &block_A);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_B, &B, &block_B);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR (idx_batches, n_batches) FOR (idx_channel, n_channels) {
		int idx_block = idx_batches * n_channels + idx_channel;
		enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
		enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_B, block_B, B, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

		// op(A, B)
		FOR(i, n_rows) FOR(j, n_cols){
			if (func_name == ADD) {
				A[i * n_cols + j] *= alpha;
				B[i * n_cols + j] *= beta;
			}
			op(&A[i * n_cols + j], &B[i * n_cols + j]);
		}

		enclave_ret = tensor_place_block_to_untrusted(sealed_C, block_B, B, idx_block);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(B);
	free_allocated_memory(block_A);
	free_allocated_memory(block_B);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_tensor_elem_copab_alpha_beta(
	sealed_tensor_t* sealed_A, size_t sz_sealed_A,
	sealed_tensor_t* sealed_B, size_t sz_sealed_B,
	sealed_tensor_t* sealed_C, size_t sz_sealed_C,
	float alpha, float beta,
	elem_funcname_t func_name) 
{
	return sealed_tensor_elem_copab_alpha_beta(
		sealed_A,
		sealed_B,
		sealed_C,
		alpha, beta,
		func_name);
}

uint32_t enclave_sealed_tensor_elem_bopa(
	sealed_tensor_t* sealed_A,
	sealed_tensor_t* sealed_B,
	elem_funcname_t func_name) 
{
	// Status flags
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t ocall_ret = OCALL_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;

	// Setup the function
	// typedef void(*funcp_t)(float*);
	process_func_t op = NULL;

	switch (func_name) {
		case SIGMOID: op = elem_sigmoid; break;
		case SIGMOID_DIFF: op = elem_sigmoid_diff; break;
		case TANH: op = elem_tanh; break;
		case TANH_DIFF: op = elem_tanh_diff; break;
		case RELU: op = elem_relu; break;
		case RELU_DIFF: op = elem_relu_diff; break;
		default: return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}

	// Dimension check
	uint32_t M, N, m, n;
	M = sealed_A->n_batches;
	N = sealed_A->n_channels;
	m = sealed_A->n_rows;
	n = sealed_A->n_cols;

	if (M != sealed_B->n_batches) return ENCLAVE_ERROR_DIMENSIONS;
	if (N != sealed_B->n_channels) return ENCLAVE_ERROR_DIMENSIONS;
	if (m != sealed_B->n_rows) return ENCLAVE_ERROR_DIMENSIONS;
	if (n != sealed_B->n_cols) return ENCLAVE_ERROR_DIMENSIONS;

	// Memory allocation
	float *A;
	sealed_block_matrix_t *block_A;
	enclave_ret = sealed_tensor_memory_allocation(sealed_A, &A, &block_A);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// TODO: Erease all the data when failed to unseal
	FOR(idx_M, M) FOR(idx_N, N) {
		int idx_block = idx_M * N + idx_N;
		enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_A, block_A, A, idx_block);
		if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

		FOR(i, m) FOR(j, n) A[i * n + j] = op(A[i * n + j]);;

		enclave_ret = tensor_place_block_to_untrusted(sealed_B, block_A, A, idx_block);
		if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
	}

	free_allocated_memory(A);
	free_allocated_memory(block_A);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_sealed_tensor_elem_bopa(
	sealed_tensor_t* sealed_A, size_t sz_sealed_A,
	sealed_tensor_t* sealed_B, size_t sz_sealed_B,
	elem_funcname_t func_name) 
{
	return enclave_sealed_tensor_elem_bopa(
		sealed_A,
		sealed_B,
		func_name);
}

uint32_t tensor_average_pooling(
	sealed_tensor_t* sealed_input, sealed_tensor_t* sealed_output,
	uint32_t field_y, uint32_t field_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_auxiliary, *plain_output;
	sealed_block_matrix_t *block_input, *block_auxiliary, *block_output;

	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	if (sealed_output->n_rows != ((int) sealed_input->n_rows - field_y + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) sealed_input->n_cols - field_x + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	// if (pooling_name == MAX_POOLING) {
	// 	if (sealed_auxiliary->n_channels != sealed_auxiliary->n_channels) return ENCLAVE_ERROR_PARAMETERS;
	// 	enclave_ret = sealed_tensor_memory_allocation(sealed_auxiliary, &plain_auxiliary, &block_auxiliary);
	// 	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	// 	if (sealed_auxiliary->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	// }

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	FOR (idx_channel, sealed_output->n_channels) {
		FOR (idx_batches, sealed_input->n_batches) {
			// Unseal for output channel
			memset(plain_output, 0, sealed_output->sz_each_plain_block);

			// Unseal an input's block
			enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_channel);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			for (int anchor_y = -padding_y, output_y = 0; anchor_y + field_y - 1 < (int) (sealed_input->n_rows + padding_y); anchor_y += stride_y, output_y++) {
				for (int anchor_x = -padding_x, output_x = 0; anchor_x + field_x - 1 < (int) (sealed_input->n_cols + padding_x); anchor_x += stride_x, output_x++) {
					// float output_pixel = (pooling_name == MAX_POOLING) ? plain_input[CLIP(anchor_y, 0, input_M) * input_N + CLIP(anchor_x, 0, input_N)] : 0.0f;
					float output_pixel = 0.0f;
					FOR(moving_y, field_y) FOR(moving_x, field_x) {
						int input_x = anchor_x + moving_x;
						int input_y = anchor_y + moving_y;

						if (input_x < 0 || input_x >= input_N || input_y < 0 || input_y >= input_M) {
							continue;
						} else {
							output_pixel += plain_input[input_y * input_N + input_x];
						}
					}
					plain_output[output_y * output_N + output_x] = output_pixel / (field_x * field_y);
				}
			}

			enclave_ret = tensor_place_block_to_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_channel);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

uint32_t tensor_backward_average_pooling(
	sealed_tensor_t* sealed_output, sealed_tensor_t* sealed_input,
	uint32_t field_y, uint32_t field_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_auxiliary, *plain_output;
	sealed_block_matrix_t *block_input, *block_auxiliary, *block_output;

	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	if (sealed_output->n_rows != ((int) sealed_input->n_rows - field_y + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) sealed_input->n_cols - field_x + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	FOR (idx_channel, sealed_input->n_channels) {
		FOR (idx_batches, sealed_input->n_batches) {

			memset(plain_input, 0, sealed_input->sz_each_plain_block);

			// Unseal an input's block
			enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_channel);
			if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

			for (int anchor_y = -padding_y, output_y = 0; anchor_y + field_y - 1 < (int) (sealed_input->n_rows + padding_y); anchor_y += stride_y, output_y++) {
				for (int anchor_x = -padding_x, output_x = 0; anchor_x + field_x - 1 < (int) (sealed_input->n_cols + padding_x); anchor_x += stride_x, output_x++) {
					float output_pixel = plain_output[output_y * output_N + output_x] / (field_x * field_y);
					FOR(moving_y, field_y) FOR(moving_x, field_x) {
						int input_x = anchor_x + moving_x;
						int input_y = anchor_y + moving_y;

						if (input_x < 0 || input_x >= input_N || input_y < 0 || input_y >= input_M) {
							continue;
						} else {
							plain_input[input_y * input_N + input_x] += output_pixel;
						}
					}
				}
			}

			enclave_ret = tensor_place_block_to_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_channel);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

uint32_t tensor_max_pooling(
	sealed_tensor_t* sealed_input, sealed_tensor_t* sealed_output, sealed_tensor_t* sealed_auxiliary,
	uint32_t field_y, uint32_t field_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	float *plain_input, *plain_auxiliary, *plain_output;
	sealed_block_matrix_t *block_input, *block_auxiliary, *block_output;

	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input, &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_input->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_input->n_channels != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;

	if (sealed_output->n_rows != ((int) sealed_input->n_rows - field_y + 2 * padding_y) / stride_y + 1) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != ((int) sealed_input->n_cols - field_x + 2 * padding_x) / stride_x + 1) return ENCLAVE_ERROR_PARAMETERS;

	if (sealed_auxiliary->n_channels != sealed_output->n_channels) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_auxiliary->n_batches != sealed_output->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	CHECK_ENCLAVE(sealed_tensor_memory_allocation(sealed_auxiliary, &plain_auxiliary, &block_auxiliary));

	uint32_t input_M = sealed_input->n_rows;
	uint32_t input_N = sealed_input->n_cols;
	uint32_t output_M = sealed_output->n_rows;
	uint32_t output_N = sealed_output->n_cols;

	FOR (idx_channel, sealed_output->n_channels) {
		FOR (idx_batches, sealed_input->n_batches) {
			// Unseal for output channel
			memset(plain_output, 0, sealed_output->sz_each_plain_block);
			memset(plain_auxiliary, 0, sealed_auxiliary->sz_each_plain_block);

			// Unseal an input's block
			CHECK_ENCLAVE(tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input, idx_batches * sealed_input->n_channels + idx_channel));

			for (int anchor_y = -padding_y, output_y = 0; anchor_y + field_y - 1 < (int) (sealed_input->n_rows + padding_y); anchor_y += stride_y, output_y++) {
				for (int anchor_x = -padding_x, output_x = 0; anchor_x + field_x - 1 < (int) (sealed_input->n_cols + padding_x); anchor_x += stride_x, output_x++) {
					float pos_pixel = 0;
					float output_pixel = plain_input[CLIP(anchor_y, 0, input_M) * input_N + CLIP(anchor_x, 0, input_N)];
					FOR(moving_y, field_y) FOR(moving_x, field_x) {
						int input_x = anchor_x + moving_x;
						int input_y = anchor_y + moving_y;

						if (input_x < 0 || input_x >= input_N || input_y < 0 || input_y >= input_M) {
							continue;
						} else {
							if (output_pixel <= plain_input[input_y * input_N + input_x]) {
								output_pixel = plain_input[input_y * input_N + input_x];
								pos_pixel = input_y * input_N + input_x;
							}
						}
					}
					plain_output[output_y * output_N + output_x] = output_pixel;
					plain_auxiliary[output_y * output_N + output_x] = pos_pixel;
				}
			}

			CHECK_ENCLAVE(tensor_place_block_to_untrusted(sealed_output, block_output, plain_output, idx_batches * sealed_output->n_channels + idx_channel));
			CHECK_ENCLAVE(tensor_place_block_to_untrusted(sealed_auxiliary, block_auxiliary, plain_auxiliary, idx_batches * sealed_output->n_channels + idx_channel));
		}
	}
	free_allocated_memory(plain_input);
	free_allocated_memory(plain_output);
	free_allocated_memory(block_input);
	free_allocated_memory(block_output);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_tensor_pooling(
	sealed_tensor_t* sealed_input, size_t sz_sealed_input,
	sealed_tensor_t* sealed_output, size_t sz_sealed_output,
	sealed_tensor_t* sealed_auxiliary, size_t sz_sealed_auxiliary,
	uint32_t field_y, uint32_t field_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t stride_y, uint32_t stride_x,
	elem_funcname_t pooling_name)
{
	if (pooling_name == AVERAGE_POOLING) {
		return tensor_average_pooling(
			sealed_input, sealed_output, 
			field_y, field_x,
			padding_y, padding_x,
			stride_y, stride_x);
	} else if (pooling_name == BACKWARD_AVERAGE_POOLING) {
		return tensor_backward_average_pooling(
			sealed_input, sealed_output, 
			field_y, field_x,
			padding_y, padding_x,
			stride_y, stride_x);
	} else if (pooling_name == MAX_POOLING) {
		return tensor_max_pooling(
			sealed_input, sealed_output, sealed_auxiliary,
			field_y, field_x,
			padding_y, padding_x,
			stride_y, stride_x);
	} else {
		return ENCLAVE_ERROR_NOT_IMPLEMENTED;
	}
}

uint32_t tensor_flatten(
	sealed_tensor_t* sealed_input, 
	sealed_divided_matrix_t* sealed_output)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	uint32_t n_input_blocks = sealed_output->n_rows_each_block;

	float *plain_output;
	float **plain_input_blocks = (float**)malloc(n_input_blocks * sizeof(float*));
	FOR (i, n_input_blocks) {
		plain_input_blocks[i] = (float*)malloc(sealed_input->sz_each_plain_block);
	}

	sealed_block_matrix_t *block_input, *block_output;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input_blocks[0], &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	// The n_batches of kernal is regarded as the number of output channels of convolution
	if (sealed_output->n_rows != sealed_input->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != sealed_input->n_channels * sealed_input->n_rows * sealed_input->n_cols) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t n_rows_in_block, n_cols_in_block;
	uint32_t idx_sealing_block = 0;

	FOR(idx_row_block, sealed_output->n_rows_blocks) {
		int output_length = sealed_output->n_cols_each_block;
		int loaded_length = sealed_input->n_rows * sealed_input->n_cols;
		int idx_loaded_col = -loaded_length;
		FOR(idx_col_block, sealed_output->n_cols_blocks) {
			memset(plain_output, 0, sealed_output->sz_each_plain_block);
			if (idx_loaded_col + loaded_length > (int) idx_col_block * sealed_output->n_cols_each_block) {
				// Place leftover block
				FOR(idx_local_row, n_input_blocks) {
					int input_offset = idx_col_block * sealed_output->n_cols_each_block - idx_loaded_col;
					size_t sz_cpy = MIN(output_length, (loaded_length - input_offset)) * sizeof(float);
					int output_offset = idx_local_row * sealed_output->n_cols_each_block;
					memcpy(&plain_output[output_offset], &plain_input_blocks[idx_local_row][input_offset], sz_cpy);
				}
			}
			while (idx_loaded_col + loaded_length < (int)((idx_col_block + 1) * sealed_output->n_cols_each_block) && idx_loaded_col + loaded_length < sealed_output->n_cols) {
				idx_loaded_col += loaded_length;
				// Load new A
				FOR(idx_local_row, n_input_blocks) {
					uint32_t idx_overall_block = (idx_row_block * sealed_output->n_rows_each_block + idx_local_row) * sealed_input->n_channels + (idx_loaded_col / loaded_length);
					if (idx_overall_block >= sealed_input->n_batches * sealed_input->n_channels) {
						memset(plain_input_blocks[idx_local_row], 0, sealed_input->sz_each_plain_block);
					} else {
						enclave_ret = tensor_get_sealed_block_from_untrusted(sealed_input, block_input, plain_input_blocks[idx_local_row], idx_overall_block);
						if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
					}
				}
				// Place A to B
				FOR(idx_local_row, n_input_blocks) {
					int input_offset = 0;
					size_t sz_cpy = 
						MIN(output_length, 
						MIN(loaded_length, 
						((idx_col_block + 1) * sealed_output->n_cols_each_block - idx_loaded_col))) * 
						sizeof(float);
					int output_offset = idx_local_row * sealed_output->n_cols_each_block + (idx_loaded_col - idx_col_block * sealed_output->n_cols_each_block);
					memcpy(&plain_output[output_offset], &plain_input_blocks[idx_local_row][input_offset], sz_cpy);
				}

			} 

			enclave_ret = place_block_to_untrusted(sealed_output, block_output, plain_output, idx_row_block * sealed_output->n_cols_blocks + idx_col_block);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
		}
	}

	free_allocated_memory(block_output);
	free_allocated_memory(block_input);
	free_allocated_memory(plain_output);
	FOR (i, n_input_blocks) {
		free_allocated_memory(plain_input_blocks[i]);
	}
	free(plain_input_blocks);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_tensor_flatten(
	sealed_tensor_t* sealed_input, size_t sz_sealed_input,
	sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output) 
{
	return tensor_flatten(
		sealed_input,
		sealed_output);
}

uint32_t tensor_backward_flatten(
	sealed_divided_matrix_t* sealed_output, 
	sealed_tensor_t* sealed_input)
{
	sgx_status_t proc_ret = SGX_SUCCESS;
	uint32_t enclave_ret = ENCLAVE_SUCCESS;
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	uint32_t n_input_blocks = sealed_output->n_rows_each_block;

	float *plain_output;
	float **plain_input_blocks = (float**)malloc(n_input_blocks * sizeof(float*));
	FOR (i, n_input_blocks) {
		plain_input_blocks[i] = (float*)malloc(sealed_input->sz_each_plain_block);
	}

	sealed_block_matrix_t *block_input, *block_output;
	enclave_ret = enclave_sealed_divided_memory_allocation(sealed_output, &plain_output, &block_output);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;
	enclave_ret = sealed_tensor_memory_allocation(sealed_input, &plain_input_blocks[0], &block_input);
	if (enclave_ret != ENCLAVE_SUCCESS) return enclave_ret;

	if (sealed_output->n_rows != sealed_input->n_batches) return ENCLAVE_ERROR_PARAMETERS;
	if (sealed_output->n_cols != sealed_input->n_channels * sealed_input->n_rows * sealed_input->n_cols) return ENCLAVE_ERROR_PARAMETERS;

	uint32_t n_rows_in_block, n_cols_in_block;
	uint32_t idx_sealing_block = 0;

	FOR(idx_row_block, sealed_output->n_rows_blocks) {
		int output_length = sealed_output->n_cols_each_block;
		int loaded_length = sealed_input->n_rows * sealed_input->n_cols;
		int idx_loaded_col = -loaded_length;

		FOR(idx_col_block, sealed_output->n_cols_blocks) {

			enclave_ret = get_sealed_block_from_untrusted(sealed_output, block_output, plain_output, idx_row_block * sealed_output->n_cols_blocks + idx_col_block);
			if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_UNSEALING;

			if (idx_loaded_col + loaded_length > (int) idx_col_block * sealed_output->n_cols_each_block) {
				// Place leftover block
				FOR(idx_local_row, n_input_blocks) {
					uint32_t idx_overall_block = (idx_row_block * sealed_output->n_rows_each_block + idx_local_row) * sealed_input->n_channels + (idx_loaded_col / loaded_length);
					if (idx_overall_block >= sealed_input->n_batches * sealed_input->n_channels) break;

					int input_offset = idx_col_block * sealed_output->n_cols_each_block - idx_loaded_col;
					size_t sz_cpy = MIN(output_length, (loaded_length - input_offset)) * sizeof(float);
					int output_offset = idx_local_row * sealed_output->n_cols_each_block;

					memcpy(&plain_input_blocks[idx_local_row][input_offset], &plain_output[output_offset], sz_cpy);
					if (input_offset + sz_cpy / sizeof(float) == loaded_length) {
						enclave_ret = tensor_place_block_to_untrusted(sealed_input, block_input, plain_input_blocks[idx_local_row], idx_overall_block);
						if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
					}
				}
			}
			while (idx_loaded_col + loaded_length < (int)((idx_col_block + 1) * sealed_output->n_cols_each_block) && idx_loaded_col + loaded_length < sealed_output->n_cols) {
				idx_loaded_col += loaded_length;
				// Load new A
				FOR(idx_local_row, n_input_blocks) {
					uint32_t idx_overall_block = (idx_row_block * sealed_output->n_rows_each_block + idx_local_row) * sealed_input->n_channels + (idx_loaded_col / loaded_length);
					if (idx_overall_block >= sealed_input->n_batches * sealed_input->n_channels) break;
					memset(plain_input_blocks[idx_local_row], 0, sealed_input->sz_each_plain_block);
				}
				// Place A to B
				FOR(idx_local_row, n_input_blocks) {
					uint32_t idx_overall_block = (idx_row_block * sealed_output->n_rows_each_block + idx_local_row) * sealed_input->n_channels + (idx_loaded_col / loaded_length);
					if (idx_overall_block >= sealed_input->n_batches * sealed_input->n_channels) break;

					int input_offset = 0;
					size_t sz_cpy = 
						MIN(output_length, 
						MIN(loaded_length, 
						((idx_col_block + 1) * sealed_output->n_cols_each_block - idx_loaded_col))) * 
						sizeof(float);
					int output_offset = idx_local_row * sealed_output->n_cols_each_block + (idx_loaded_col - idx_col_block * sealed_output->n_cols_each_block);

					memcpy(&plain_input_blocks[idx_local_row][input_offset], &plain_output[output_offset], sz_cpy);
					if (input_offset + sz_cpy / sizeof(float) == loaded_length) {
						enclave_ret = tensor_place_block_to_untrusted(sealed_input, block_input, plain_input_blocks[idx_local_row], idx_overall_block);
						if (enclave_ret != SGX_SUCCESS) return ENCLAVE_ERROR_SEALING;
					}
				}

			} 
		}
	}

	free_allocated_memory(block_output);
	free_allocated_memory(block_input);
	free_allocated_memory(plain_output);
	FOR (i, n_input_blocks) {
		free_allocated_memory(plain_input_blocks[i]);
	}
	free(plain_input_blocks);

	return ENCLAVE_SUCCESS;
}

uint32_t enclave_tensor_backward_flatten(
	sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output,
	sealed_tensor_t* sealed_input, size_t sz_sealed_input) 
{
	return tensor_backward_flatten(
		sealed_output,
		sealed_input);
}