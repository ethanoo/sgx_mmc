#ifndef ENCLAVE_MMC_T_H__
#define ENCLAVE_MMC_T_H__

#include <stdint.h>
#include <wchar.h>
#include <stddef.h>
#include "sgx_edger8r.h" /* for sgx_ocall etc. */

#include "stdlib.h"
#include "datatypes.h"

#define SGX_CAST(type, item) ((type)(item))

#ifdef __cplusplus
extern "C" {
#endif


uint32_t enclave_calc_sealed_block_matrix_size(uint32_t n_rows, uint32_t n_cols);
uint32_t enclave_create_sealed_block_matrix(sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, uint32_t n_rows, uint32_t n_cols, float* plain_matrix, size_t sz_plain_matrix);
uint32_t enclave_unseal_block_matrix(sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, float* plain_matrix, size_t sz_plain_matrix);
uint32_t enclave_sealed_block_gemm(sealed_block_matrix_t* sealed_A, size_t sz_sealed_A, sealed_block_matrix_t* sealed_B, size_t sz_sealed_B, sealed_block_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta);
uint32_t enclave_sealed_divided_gemm_with_functions(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta, elem_funcname_t preA_function_name, elem_funcname_t preB_function_name, elem_funcname_t post_function_name);
uint32_t enclave_sealed_divided_gemm(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta);
uint32_t enclave_sealed_divided_elem_bopab(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
uint32_t enclave_sealed_divided_elem_copab_alpha_beta(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name);
uint32_t enclave_sealed_divided_geadd(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, float alpha, float beta, elem_funcname_t func_name);
uint32_t enclave_sealed_divided_elem_bopa(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
uint32_t enclave_sealed_divided_elem_aopa(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, elem_funcname_t func_name);
uint32_t enclave_sealed_divided_aggregate_rows(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_sealed_res_agg, elem_funcname_t agg_func_name);
uint32_t enclave_sealed_divided_aggregate_rows_exponetial_add(sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_res_agg, float* pre_add, size_t sz_pre_add);
uint32_t enclave_sealed_gemm(uint8_t* sealed_A, uint8_t* sealed_B, uint8_t* sealed_C, uint32_t is_A_trans, uint32_t is_B_trans, float alpha, float beta, size_t sz_sealed_A, size_t sz_sealed_B, size_t sz_sealed_C, size_t sz_A, size_t sz_B, size_t sz_C, int m, int n, int k);
uint32_t enclave_output_layer(sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, sealed_divided_matrix_t* sealed_diff_preY, size_t sz_sealed_diff_preY, float* loss);
uint32_t enclave_output_layer_acc(sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, float* acc);
uint32_t enclave_to_zeros(float* A, size_t sz_A, int n_elem);
uint32_t enclave_sealed_size(uint32_t add_mac_txt_size, uint32_t txt_encrypt_size);
uint32_t enclave_seal_matrix(float* A, size_t sz_A, uint8_t* sealed_data, uint32_t sz_sealed_data);
uint32_t enclave_unseal_matrix(float* A, size_t sz_A, uint8_t* sealed_data, size_t sz_sealed_data);
uint32_t enclave_unseal_block_pointer(sealed_block_matrix_t* sealed_block, sealed_block_matrix_t* retrieve_sealed_block, size_t sz_sealed_block);
uint32_t enclave_convolute_tensor_kernal(sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta, uint32_t is_rot180_input, uint32_t is_rot180_kernal);
uint32_t enclave_sealed_tensor_elem_copab_alpha_beta(sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, sealed_tensor_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name);
uint32_t enclave_sealed_tensor_elem_bopa(sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
uint32_t enclave_tensor_pooling(sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_auxiliary, size_t sz_sealed_auxiliary, uint32_t field_y, uint32_t field_x, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, elem_funcname_t pooling_name);
uint32_t enclave_tensor_flatten(sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output);
uint32_t enclave_tensor_backward_flatten(sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_input, size_t sz_sealed_input);
uint32_t enclave_backward_convolute_tensor_kernal(sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta);
uint32_t enclave_backward_convolute_tensor_input(sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta);

sgx_status_t SGX_CDECL get_memory_ocall(uint32_t* retval, uint8_t* memory_ptr, uint8_t* receive_memory, size_t sz_memory);
sgx_status_t SGX_CDECL place_memory_ocall(uint32_t* retval, uint8_t* memory_from_enclave, uint8_t* memory_in_untrusted, size_t sz_memory);
sgx_status_t SGX_CDECL sgx_oc_cpuidex(int cpuinfo[4], int leaf, int subleaf);
sgx_status_t SGX_CDECL sgx_thread_wait_untrusted_event_ocall(int* retval, const void* self);
sgx_status_t SGX_CDECL sgx_thread_set_untrusted_event_ocall(int* retval, const void* waiter);
sgx_status_t SGX_CDECL sgx_thread_setwait_untrusted_events_ocall(int* retval, const void* waiter, const void* self);
sgx_status_t SGX_CDECL sgx_thread_set_multiple_untrusted_events_ocall(int* retval, const void** waiters, size_t total);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
