#include "enclave_mmc_t.h"

#include "sgx_trts.h" /* for sgx_ocalloc, sgx_is_outside_enclave */

#include <errno.h>
#include <string.h> /* for memcpy etc */
#include <stdlib.h> /* for malloc/free etc */

#define CHECK_REF_POINTER(ptr, siz) do {	\
	if (!(ptr) || ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define CHECK_UNIQUE_POINTER(ptr, siz) do {	\
	if ((ptr) && ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)


typedef struct ms_enclave_calc_sealed_block_matrix_size_t {
	uint32_t ms_retval;
	uint32_t ms_n_rows;
	uint32_t ms_n_cols;
} ms_enclave_calc_sealed_block_matrix_size_t;

typedef struct ms_enclave_create_sealed_block_matrix_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_matrix;
	size_t ms_sz_sealed_matrix;
	uint32_t ms_n_rows;
	uint32_t ms_n_cols;
	float* ms_plain_matrix;
	size_t ms_sz_plain_matrix;
} ms_enclave_create_sealed_block_matrix_t;

typedef struct ms_enclave_unseal_block_matrix_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_matrix;
	size_t ms_sz_sealed_matrix;
	float* ms_plain_matrix;
	size_t ms_sz_plain_matrix;
} ms_enclave_unseal_block_matrix_t;

typedef struct ms_enclave_sealed_block_gemm_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_block_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_block_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
} ms_enclave_sealed_block_gemm_t;

typedef struct ms_enclave_sealed_divided_gemm_with_functions_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_preA_function_name;
	elem_funcname_t ms_preB_function_name;
	elem_funcname_t ms_post_function_name;
} ms_enclave_sealed_divided_gemm_with_functions_t;

typedef struct ms_enclave_sealed_divided_gemm_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
} ms_enclave_sealed_divided_gemm_t;

typedef struct ms_enclave_sealed_divided_elem_bopab_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_bopab_t;

typedef struct ms_enclave_sealed_divided_elem_copab_alpha_beta_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_copab_alpha_beta_t;

typedef struct ms_enclave_sealed_divided_geadd_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_geadd_t;

typedef struct ms_enclave_sealed_divided_elem_bopa_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_bopa_t;

typedef struct ms_enclave_sealed_divided_elem_aopa_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_aopa_t;

typedef struct ms_enclave_sealed_divided_aggregate_rows_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	float* ms_res_agg;
	size_t ms_sz_sealed_res_agg;
	elem_funcname_t ms_agg_func_name;
} ms_enclave_sealed_divided_aggregate_rows_t;

typedef struct ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	float* ms_res_agg;
	size_t ms_sz_res_agg;
	float* ms_pre_add;
	size_t ms_sz_pre_add;
} ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t;

typedef struct ms_enclave_sealed_gemm_t {
	uint32_t ms_retval;
	uint8_t* ms_sealed_A;
	uint8_t* ms_sealed_B;
	uint8_t* ms_sealed_C;
	uint32_t ms_is_A_trans;
	uint32_t ms_is_B_trans;
	float ms_alpha;
	float ms_beta;
	size_t ms_sz_sealed_A;
	size_t ms_sz_sealed_B;
	size_t ms_sz_sealed_C;
	size_t ms_sz_A;
	size_t ms_sz_B;
	size_t ms_sz_C;
	int ms_m;
	int ms_n;
	int ms_k;
} ms_enclave_sealed_gemm_t;

typedef struct ms_enclave_output_layer_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_trueY;
	size_t ms_sz_sealed_trueY;
	sealed_divided_matrix_t* ms_sealed_preY;
	size_t ms_sz_sealed_preY;
	sealed_divided_matrix_t* ms_sealed_diff_preY;
	size_t ms_sz_sealed_diff_preY;
	float* ms_loss;
} ms_enclave_output_layer_t;

typedef struct ms_enclave_output_layer_acc_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_trueY;
	size_t ms_sz_sealed_trueY;
	sealed_divided_matrix_t* ms_sealed_preY;
	size_t ms_sz_sealed_preY;
	float* ms_acc;
} ms_enclave_output_layer_acc_t;

typedef struct ms_enclave_to_zeros_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	int ms_n_elem;
} ms_enclave_to_zeros_t;

typedef struct ms_enclave_sealed_size_t {
	uint32_t ms_retval;
	uint32_t ms_add_mac_txt_size;
	uint32_t ms_txt_encrypt_size;
} ms_enclave_sealed_size_t;

typedef struct ms_enclave_seal_matrix_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	uint8_t* ms_sealed_data;
	uint32_t ms_sz_sealed_data;
} ms_enclave_seal_matrix_t;

typedef struct ms_enclave_unseal_matrix_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	uint8_t* ms_sealed_data;
	size_t ms_sz_sealed_data;
} ms_enclave_unseal_matrix_t;

typedef struct ms_enclave_unseal_block_pointer_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_block;
	sealed_block_matrix_t* ms_retrieve_sealed_block;
	size_t ms_sz_sealed_block;
} ms_enclave_unseal_block_pointer_t;

typedef struct ms_enclave_convolute_tensor_kernal_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
	uint32_t ms_is_rot180_input;
	uint32_t ms_is_rot180_kernal;
} ms_enclave_convolute_tensor_kernal_t;

typedef struct ms_enclave_sealed_tensor_elem_copab_alpha_beta_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_tensor_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_tensor_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_tensor_elem_copab_alpha_beta_t;

typedef struct ms_enclave_sealed_tensor_elem_bopa_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_tensor_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_tensor_elem_bopa_t;

typedef struct ms_enclave_tensor_pooling_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	sealed_tensor_t* ms_sealed_auxiliary;
	size_t ms_sz_sealed_auxiliary;
	uint32_t ms_field_y;
	uint32_t ms_field_x;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	elem_funcname_t ms_pooling_name;
} ms_enclave_tensor_pooling_t;

typedef struct ms_enclave_tensor_flatten_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_divided_matrix_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
} ms_enclave_tensor_flatten_t;

typedef struct ms_enclave_tensor_backward_flatten_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
} ms_enclave_tensor_backward_flatten_t;

typedef struct ms_enclave_backward_convolute_tensor_kernal_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
} ms_enclave_backward_convolute_tensor_kernal_t;

typedef struct ms_enclave_backward_convolute_tensor_input_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
} ms_enclave_backward_convolute_tensor_input_t;

typedef struct ms_get_memory_ocall_t {
	uint32_t ms_retval;
	uint8_t* ms_memory_ptr;
	uint8_t* ms_receive_memory;
	size_t ms_sz_memory;
} ms_get_memory_ocall_t;

typedef struct ms_place_memory_ocall_t {
	uint32_t ms_retval;
	uint8_t* ms_memory_from_enclave;
	uint8_t* ms_memory_in_untrusted;
	size_t ms_sz_memory;
} ms_place_memory_ocall_t;

typedef struct ms_sgx_oc_cpuidex_t {
	int* ms_cpuinfo;
	int ms_leaf;
	int ms_subleaf;
} ms_sgx_oc_cpuidex_t;

typedef struct ms_sgx_thread_wait_untrusted_event_ocall_t {
	int ms_retval;
	void* ms_self;
} ms_sgx_thread_wait_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_set_untrusted_event_ocall_t {
	int ms_retval;
	void* ms_waiter;
} ms_sgx_thread_set_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_setwait_untrusted_events_ocall_t {
	int ms_retval;
	void* ms_waiter;
	void* ms_self;
} ms_sgx_thread_setwait_untrusted_events_ocall_t;

typedef struct ms_sgx_thread_set_multiple_untrusted_events_ocall_t {
	int ms_retval;
	void** ms_waiters;
	size_t ms_total;
} ms_sgx_thread_set_multiple_untrusted_events_ocall_t;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4127)
#pragma warning(disable: 4200)
#endif

static sgx_status_t SGX_CDECL sgx_enclave_calc_sealed_block_matrix_size(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_calc_sealed_block_matrix_size_t));
	ms_enclave_calc_sealed_block_matrix_size_t* ms = SGX_CAST(ms_enclave_calc_sealed_block_matrix_size_t*, pms);
	sgx_status_t status = SGX_SUCCESS;


	ms->ms_retval = enclave_calc_sealed_block_matrix_size(ms->ms_n_rows, ms->ms_n_cols);


	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_create_sealed_block_matrix(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_create_sealed_block_matrix_t));
	ms_enclave_create_sealed_block_matrix_t* ms = SGX_CAST(ms_enclave_create_sealed_block_matrix_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_block_matrix_t* _tmp_sealed_matrix = ms->ms_sealed_matrix;
	size_t _tmp_sz_sealed_matrix = ms->ms_sz_sealed_matrix;
	size_t _len_sealed_matrix = _tmp_sz_sealed_matrix;
	sealed_block_matrix_t* _in_sealed_matrix = NULL;
	float* _tmp_plain_matrix = ms->ms_plain_matrix;
	size_t _tmp_sz_plain_matrix = ms->ms_sz_plain_matrix;
	size_t _len_plain_matrix = _tmp_sz_plain_matrix;
	float* _in_plain_matrix = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_matrix, _len_sealed_matrix);
	CHECK_UNIQUE_POINTER(_tmp_plain_matrix, _len_plain_matrix);

	if (_tmp_sealed_matrix != NULL && _len_sealed_matrix != 0) {
		if ((_in_sealed_matrix = (sealed_block_matrix_t*)malloc(_len_sealed_matrix)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_sealed_matrix, 0, _len_sealed_matrix);
	}
	if (_tmp_plain_matrix != NULL && _len_plain_matrix != 0) {
		_in_plain_matrix = (float*)malloc(_len_plain_matrix);
		if (_in_plain_matrix == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_plain_matrix, _tmp_plain_matrix, _len_plain_matrix);
	}
	ms->ms_retval = enclave_create_sealed_block_matrix(_in_sealed_matrix, _tmp_sz_sealed_matrix, ms->ms_n_rows, ms->ms_n_cols, _in_plain_matrix, _tmp_sz_plain_matrix);
err:
	if (_in_sealed_matrix) {
		memcpy(_tmp_sealed_matrix, _in_sealed_matrix, _len_sealed_matrix);
		free(_in_sealed_matrix);
	}
	if (_in_plain_matrix) free(_in_plain_matrix);

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_unseal_block_matrix(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_unseal_block_matrix_t));
	ms_enclave_unseal_block_matrix_t* ms = SGX_CAST(ms_enclave_unseal_block_matrix_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_block_matrix_t* _tmp_sealed_matrix = ms->ms_sealed_matrix;
	size_t _tmp_sz_sealed_matrix = ms->ms_sz_sealed_matrix;
	size_t _len_sealed_matrix = _tmp_sz_sealed_matrix;
	sealed_block_matrix_t* _in_sealed_matrix = NULL;
	float* _tmp_plain_matrix = ms->ms_plain_matrix;
	size_t _tmp_sz_plain_matrix = ms->ms_sz_plain_matrix;
	size_t _len_plain_matrix = _tmp_sz_plain_matrix;
	float* _in_plain_matrix = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_matrix, _len_sealed_matrix);
	CHECK_UNIQUE_POINTER(_tmp_plain_matrix, _len_plain_matrix);

	if (_tmp_sealed_matrix != NULL && _len_sealed_matrix != 0) {
		_in_sealed_matrix = (sealed_block_matrix_t*)malloc(_len_sealed_matrix);
		if (_in_sealed_matrix == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_matrix, _tmp_sealed_matrix, _len_sealed_matrix);
	}
	if (_tmp_plain_matrix != NULL && _len_plain_matrix != 0) {
		if ((_in_plain_matrix = (float*)malloc(_len_plain_matrix)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_plain_matrix, 0, _len_plain_matrix);
	}
	ms->ms_retval = enclave_unseal_block_matrix(_in_sealed_matrix, _tmp_sz_sealed_matrix, _in_plain_matrix, _tmp_sz_plain_matrix);
err:
	if (_in_sealed_matrix) free(_in_sealed_matrix);
	if (_in_plain_matrix) {
		memcpy(_tmp_plain_matrix, _in_plain_matrix, _len_plain_matrix);
		free(_in_plain_matrix);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_block_gemm(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_block_gemm_t));
	ms_enclave_sealed_block_gemm_t* ms = SGX_CAST(ms_enclave_sealed_block_gemm_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_block_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_block_matrix_t* _in_sealed_A = NULL;
	sealed_block_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_block_matrix_t* _in_sealed_B = NULL;
	sealed_block_matrix_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	sealed_block_matrix_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_block_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_block_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		_in_sealed_C = (sealed_block_matrix_t*)malloc(_len_sealed_C);
		if (_in_sealed_C == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_C, _tmp_sealed_C, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_block_gemm(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, _in_sealed_C, _tmp_sz_sealed_C, ms->ms_is_trans_A, ms->ms_is_trans_B, ms->ms_alpha, ms->ms_beta);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_gemm_with_functions(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_gemm_with_functions_t));
	ms_enclave_sealed_divided_gemm_with_functions_t* ms = SGX_CAST(ms_enclave_sealed_divided_gemm_with_functions_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;
	sealed_divided_matrix_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	sealed_divided_matrix_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		_in_sealed_C = (sealed_divided_matrix_t*)malloc(_len_sealed_C);
		if (_in_sealed_C == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_C, _tmp_sealed_C, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_divided_gemm_with_functions(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, _in_sealed_C, _tmp_sz_sealed_C, ms->ms_is_trans_A, ms->ms_is_trans_B, ms->ms_alpha, ms->ms_beta, ms->ms_preA_function_name, ms->ms_preB_function_name, ms->ms_post_function_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_gemm(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_gemm_t));
	ms_enclave_sealed_divided_gemm_t* ms = SGX_CAST(ms_enclave_sealed_divided_gemm_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;
	sealed_divided_matrix_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	sealed_divided_matrix_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		_in_sealed_C = (sealed_divided_matrix_t*)malloc(_len_sealed_C);
		if (_in_sealed_C == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_C, _tmp_sealed_C, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_divided_gemm(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, _in_sealed_C, _tmp_sz_sealed_C, ms->ms_is_trans_A, ms->ms_is_trans_B, ms->ms_alpha, ms->ms_beta);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_elem_bopab(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_elem_bopab_t));
	ms_enclave_sealed_divided_elem_bopab_t* ms = SGX_CAST(ms_enclave_sealed_divided_elem_bopab_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	ms->ms_retval = enclave_sealed_divided_elem_bopab(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) {
		memcpy(_tmp_sealed_B, _in_sealed_B, _len_sealed_B);
		free(_in_sealed_B);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_elem_copab_alpha_beta(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_elem_copab_alpha_beta_t));
	ms_enclave_sealed_divided_elem_copab_alpha_beta_t* ms = SGX_CAST(ms_enclave_sealed_divided_elem_copab_alpha_beta_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;
	sealed_divided_matrix_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	sealed_divided_matrix_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		_in_sealed_C = (sealed_divided_matrix_t*)malloc(_len_sealed_C);
		if (_in_sealed_C == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_C, _tmp_sealed_C, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_divided_elem_copab_alpha_beta(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, _in_sealed_C, _tmp_sz_sealed_C, ms->ms_alpha, ms->ms_beta, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_geadd(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_geadd_t));
	ms_enclave_sealed_divided_geadd_t* ms = SGX_CAST(ms_enclave_sealed_divided_geadd_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	ms->ms_retval = enclave_sealed_divided_geadd(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, ms->ms_alpha, ms->ms_beta, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) {
		memcpy(_tmp_sealed_B, _in_sealed_B, _len_sealed_B);
		free(_in_sealed_B);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_elem_bopa(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_elem_bopa_t));
	ms_enclave_sealed_divided_elem_bopa_t* ms = SGX_CAST(ms_enclave_sealed_divided_elem_bopa_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	sealed_divided_matrix_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_divided_matrix_t* _in_sealed_B = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_divided_matrix_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	ms->ms_retval = enclave_sealed_divided_elem_bopa(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) {
		memcpy(_tmp_sealed_B, _in_sealed_B, _len_sealed_B);
		free(_in_sealed_B);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_elem_aopa(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_elem_aopa_t));
	ms_enclave_sealed_divided_elem_aopa_t* ms = SGX_CAST(ms_enclave_sealed_divided_elem_aopa_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	ms->ms_retval = enclave_sealed_divided_elem_aopa(_in_sealed_A, _tmp_sz_sealed_A, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_aggregate_rows(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_aggregate_rows_t));
	ms_enclave_sealed_divided_aggregate_rows_t* ms = SGX_CAST(ms_enclave_sealed_divided_aggregate_rows_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	float* _tmp_res_agg = ms->ms_res_agg;
	size_t _tmp_sz_sealed_res_agg = ms->ms_sz_sealed_res_agg;
	size_t _len_res_agg = _tmp_sz_sealed_res_agg;
	float* _in_res_agg = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_res_agg, _len_res_agg);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_res_agg != NULL && _len_res_agg != 0) {
		if ((_in_res_agg = (float*)malloc(_len_res_agg)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_res_agg, 0, _len_res_agg);
	}
	ms->ms_retval = enclave_sealed_divided_aggregate_rows(_in_sealed_A, _tmp_sz_sealed_A, _in_res_agg, _tmp_sz_sealed_res_agg, ms->ms_agg_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_res_agg) {
		memcpy(_tmp_res_agg, _in_res_agg, _len_res_agg);
		free(_in_res_agg);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_divided_aggregate_rows_exponetial_add(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t));
	ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t* ms = SGX_CAST(ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_divided_matrix_t* _in_sealed_A = NULL;
	float* _tmp_res_agg = ms->ms_res_agg;
	size_t _tmp_sz_res_agg = ms->ms_sz_res_agg;
	size_t _len_res_agg = _tmp_sz_res_agg;
	float* _in_res_agg = NULL;
	float* _tmp_pre_add = ms->ms_pre_add;
	size_t _tmp_sz_pre_add = ms->ms_sz_pre_add;
	size_t _len_pre_add = _tmp_sz_pre_add;
	float* _in_pre_add = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_res_agg, _len_res_agg);
	CHECK_UNIQUE_POINTER(_tmp_pre_add, _len_pre_add);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_divided_matrix_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_res_agg != NULL && _len_res_agg != 0) {
		if ((_in_res_agg = (float*)malloc(_len_res_agg)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_res_agg, 0, _len_res_agg);
	}
	if (_tmp_pre_add != NULL && _len_pre_add != 0) {
		_in_pre_add = (float*)malloc(_len_pre_add);
		if (_in_pre_add == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_pre_add, _tmp_pre_add, _len_pre_add);
	}
	ms->ms_retval = enclave_sealed_divided_aggregate_rows_exponetial_add(_in_sealed_A, _tmp_sz_sealed_A, _in_res_agg, _tmp_sz_res_agg, _in_pre_add, _tmp_sz_pre_add);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_res_agg) {
		memcpy(_tmp_res_agg, _in_res_agg, _len_res_agg);
		free(_in_res_agg);
	}
	if (_in_pre_add) free(_in_pre_add);

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_gemm(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_gemm_t));
	ms_enclave_sealed_gemm_t* ms = SGX_CAST(ms_enclave_sealed_gemm_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	uint8_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	uint8_t* _in_sealed_A = NULL;
	uint8_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	uint8_t* _in_sealed_B = NULL;
	uint8_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	uint8_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (uint8_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (uint8_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		if ((_in_sealed_C = (uint8_t*)malloc(_len_sealed_C)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_sealed_C, 0, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_gemm(_in_sealed_A, _in_sealed_B, _in_sealed_C, ms->ms_is_A_trans, ms->ms_is_B_trans, ms->ms_alpha, ms->ms_beta, _tmp_sz_sealed_A, _tmp_sz_sealed_B, _tmp_sz_sealed_C, ms->ms_sz_A, ms->ms_sz_B, ms->ms_sz_C, ms->ms_m, ms->ms_n, ms->ms_k);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_output_layer(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_output_layer_t));
	ms_enclave_output_layer_t* ms = SGX_CAST(ms_enclave_output_layer_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_trueY = ms->ms_sealed_trueY;
	size_t _tmp_sz_sealed_trueY = ms->ms_sz_sealed_trueY;
	size_t _len_sealed_trueY = _tmp_sz_sealed_trueY;
	sealed_divided_matrix_t* _in_sealed_trueY = NULL;
	sealed_divided_matrix_t* _tmp_sealed_preY = ms->ms_sealed_preY;
	size_t _tmp_sz_sealed_preY = ms->ms_sz_sealed_preY;
	size_t _len_sealed_preY = _tmp_sz_sealed_preY;
	sealed_divided_matrix_t* _in_sealed_preY = NULL;
	sealed_divided_matrix_t* _tmp_sealed_diff_preY = ms->ms_sealed_diff_preY;
	size_t _tmp_sz_sealed_diff_preY = ms->ms_sz_sealed_diff_preY;
	size_t _len_sealed_diff_preY = _tmp_sz_sealed_diff_preY;
	sealed_divided_matrix_t* _in_sealed_diff_preY = NULL;
	float* _tmp_loss = ms->ms_loss;
	size_t _len_loss = 4;
	float* _in_loss = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_trueY, _len_sealed_trueY);
	CHECK_UNIQUE_POINTER(_tmp_sealed_preY, _len_sealed_preY);
	CHECK_UNIQUE_POINTER(_tmp_sealed_diff_preY, _len_sealed_diff_preY);
	CHECK_UNIQUE_POINTER(_tmp_loss, _len_loss);

	if (_tmp_sealed_trueY != NULL && _len_sealed_trueY != 0) {
		_in_sealed_trueY = (sealed_divided_matrix_t*)malloc(_len_sealed_trueY);
		if (_in_sealed_trueY == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_trueY, _tmp_sealed_trueY, _len_sealed_trueY);
	}
	if (_tmp_sealed_preY != NULL && _len_sealed_preY != 0) {
		_in_sealed_preY = (sealed_divided_matrix_t*)malloc(_len_sealed_preY);
		if (_in_sealed_preY == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_preY, _tmp_sealed_preY, _len_sealed_preY);
	}
	if (_tmp_sealed_diff_preY != NULL && _len_sealed_diff_preY != 0) {
		_in_sealed_diff_preY = (sealed_divided_matrix_t*)malloc(_len_sealed_diff_preY);
		if (_in_sealed_diff_preY == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_diff_preY, _tmp_sealed_diff_preY, _len_sealed_diff_preY);
	}
	if (_tmp_loss != NULL && _len_loss != 0) {
		if ((_in_loss = (float*)malloc(_len_loss)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_loss, 0, _len_loss);
	}
	ms->ms_retval = enclave_output_layer(_in_sealed_trueY, _tmp_sz_sealed_trueY, _in_sealed_preY, _tmp_sz_sealed_preY, _in_sealed_diff_preY, _tmp_sz_sealed_diff_preY, _in_loss);
err:
	if (_in_sealed_trueY) free(_in_sealed_trueY);
	if (_in_sealed_preY) free(_in_sealed_preY);
	if (_in_sealed_diff_preY) {
		memcpy(_tmp_sealed_diff_preY, _in_sealed_diff_preY, _len_sealed_diff_preY);
		free(_in_sealed_diff_preY);
	}
	if (_in_loss) {
		memcpy(_tmp_loss, _in_loss, _len_loss);
		free(_in_loss);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_output_layer_acc(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_output_layer_acc_t));
	ms_enclave_output_layer_acc_t* ms = SGX_CAST(ms_enclave_output_layer_acc_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_trueY = ms->ms_sealed_trueY;
	size_t _tmp_sz_sealed_trueY = ms->ms_sz_sealed_trueY;
	size_t _len_sealed_trueY = _tmp_sz_sealed_trueY;
	sealed_divided_matrix_t* _in_sealed_trueY = NULL;
	sealed_divided_matrix_t* _tmp_sealed_preY = ms->ms_sealed_preY;
	size_t _tmp_sz_sealed_preY = ms->ms_sz_sealed_preY;
	size_t _len_sealed_preY = _tmp_sz_sealed_preY;
	sealed_divided_matrix_t* _in_sealed_preY = NULL;
	float* _tmp_acc = ms->ms_acc;
	size_t _len_acc = 4;
	float* _in_acc = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_trueY, _len_sealed_trueY);
	CHECK_UNIQUE_POINTER(_tmp_sealed_preY, _len_sealed_preY);
	CHECK_UNIQUE_POINTER(_tmp_acc, _len_acc);

	if (_tmp_sealed_trueY != NULL && _len_sealed_trueY != 0) {
		_in_sealed_trueY = (sealed_divided_matrix_t*)malloc(_len_sealed_trueY);
		if (_in_sealed_trueY == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_trueY, _tmp_sealed_trueY, _len_sealed_trueY);
	}
	if (_tmp_sealed_preY != NULL && _len_sealed_preY != 0) {
		_in_sealed_preY = (sealed_divided_matrix_t*)malloc(_len_sealed_preY);
		if (_in_sealed_preY == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_preY, _tmp_sealed_preY, _len_sealed_preY);
	}
	if (_tmp_acc != NULL && _len_acc != 0) {
		if ((_in_acc = (float*)malloc(_len_acc)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_acc, 0, _len_acc);
	}
	ms->ms_retval = enclave_output_layer_acc(_in_sealed_trueY, _tmp_sz_sealed_trueY, _in_sealed_preY, _tmp_sz_sealed_preY, _in_acc);
err:
	if (_in_sealed_trueY) free(_in_sealed_trueY);
	if (_in_sealed_preY) free(_in_sealed_preY);
	if (_in_acc) {
		memcpy(_tmp_acc, _in_acc, _len_acc);
		free(_in_acc);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_to_zeros(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_to_zeros_t));
	ms_enclave_to_zeros_t* ms = SGX_CAST(ms_enclave_to_zeros_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	float* _tmp_A = ms->ms_A;
	size_t _tmp_sz_A = ms->ms_sz_A;
	size_t _len_A = _tmp_sz_A;
	float* _in_A = NULL;

	CHECK_UNIQUE_POINTER(_tmp_A, _len_A);

	if (_tmp_A != NULL && _len_A != 0) {
		if ((_in_A = (float*)malloc(_len_A)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_A, 0, _len_A);
	}
	ms->ms_retval = enclave_to_zeros(_in_A, _tmp_sz_A, ms->ms_n_elem);
err:
	if (_in_A) {
		memcpy(_tmp_A, _in_A, _len_A);
		free(_in_A);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_size(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_size_t));
	ms_enclave_sealed_size_t* ms = SGX_CAST(ms_enclave_sealed_size_t*, pms);
	sgx_status_t status = SGX_SUCCESS;


	ms->ms_retval = enclave_sealed_size(ms->ms_add_mac_txt_size, ms->ms_txt_encrypt_size);


	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_seal_matrix(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_seal_matrix_t));
	ms_enclave_seal_matrix_t* ms = SGX_CAST(ms_enclave_seal_matrix_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	float* _tmp_A = ms->ms_A;
	size_t _tmp_sz_A = ms->ms_sz_A;
	size_t _len_A = _tmp_sz_A;
	float* _in_A = NULL;
	uint8_t* _tmp_sealed_data = ms->ms_sealed_data;
	uint32_t _tmp_sz_sealed_data = ms->ms_sz_sealed_data;
	size_t _len_sealed_data = _tmp_sz_sealed_data;
	uint8_t* _in_sealed_data = NULL;

	CHECK_UNIQUE_POINTER(_tmp_A, _len_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_data, _len_sealed_data);

	if (_tmp_A != NULL && _len_A != 0) {
		_in_A = (float*)malloc(_len_A);
		if (_in_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_A, _tmp_A, _len_A);
	}
	if (_tmp_sealed_data != NULL && _len_sealed_data != 0) {
		if ((_in_sealed_data = (uint8_t*)malloc(_len_sealed_data)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_sealed_data, 0, _len_sealed_data);
	}
	ms->ms_retval = enclave_seal_matrix(_in_A, _tmp_sz_A, _in_sealed_data, _tmp_sz_sealed_data);
err:
	if (_in_A) free(_in_A);
	if (_in_sealed_data) {
		memcpy(_tmp_sealed_data, _in_sealed_data, _len_sealed_data);
		free(_in_sealed_data);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_unseal_matrix(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_unseal_matrix_t));
	ms_enclave_unseal_matrix_t* ms = SGX_CAST(ms_enclave_unseal_matrix_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	float* _tmp_A = ms->ms_A;
	size_t _tmp_sz_A = ms->ms_sz_A;
	size_t _len_A = _tmp_sz_A;
	float* _in_A = NULL;
	uint8_t* _tmp_sealed_data = ms->ms_sealed_data;
	size_t _tmp_sz_sealed_data = ms->ms_sz_sealed_data;
	size_t _len_sealed_data = _tmp_sz_sealed_data;
	uint8_t* _in_sealed_data = NULL;

	CHECK_UNIQUE_POINTER(_tmp_A, _len_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_data, _len_sealed_data);

	if (_tmp_A != NULL && _len_A != 0) {
		if ((_in_A = (float*)malloc(_len_A)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_A, 0, _len_A);
	}
	if (_tmp_sealed_data != NULL && _len_sealed_data != 0) {
		_in_sealed_data = (uint8_t*)malloc(_len_sealed_data);
		if (_in_sealed_data == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_data, _tmp_sealed_data, _len_sealed_data);
	}
	ms->ms_retval = enclave_unseal_matrix(_in_A, _tmp_sz_A, _in_sealed_data, _tmp_sz_sealed_data);
err:
	if (_in_A) {
		memcpy(_tmp_A, _in_A, _len_A);
		free(_in_A);
	}
	if (_in_sealed_data) free(_in_sealed_data);

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_unseal_block_pointer(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_unseal_block_pointer_t));
	ms_enclave_unseal_block_pointer_t* ms = SGX_CAST(ms_enclave_unseal_block_pointer_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_block_matrix_t* _tmp_sealed_block = ms->ms_sealed_block;
	sealed_block_matrix_t* _tmp_retrieve_sealed_block = ms->ms_retrieve_sealed_block;
	size_t _tmp_sz_sealed_block = ms->ms_sz_sealed_block;
	size_t _len_retrieve_sealed_block = _tmp_sz_sealed_block;
	sealed_block_matrix_t* _in_retrieve_sealed_block = NULL;

	CHECK_UNIQUE_POINTER(_tmp_retrieve_sealed_block, _len_retrieve_sealed_block);

	if (_tmp_retrieve_sealed_block != NULL && _len_retrieve_sealed_block != 0) {
		if ((_in_retrieve_sealed_block = (sealed_block_matrix_t*)malloc(_len_retrieve_sealed_block)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_retrieve_sealed_block, 0, _len_retrieve_sealed_block);
	}
	ms->ms_retval = enclave_unseal_block_pointer(_tmp_sealed_block, _in_retrieve_sealed_block, _tmp_sz_sealed_block);
err:
	if (_in_retrieve_sealed_block) {
		memcpy(_tmp_retrieve_sealed_block, _in_retrieve_sealed_block, _len_retrieve_sealed_block);
		free(_in_retrieve_sealed_block);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_convolute_tensor_kernal(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_convolute_tensor_kernal_t));
	ms_enclave_convolute_tensor_kernal_t* ms = SGX_CAST(ms_enclave_convolute_tensor_kernal_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;
	sealed_tensor_t* _tmp_sealed_kernal = ms->ms_sealed_kernal;
	size_t _tmp_sz_sealed_kernal = ms->ms_sz_sealed_kernal;
	size_t _len_sealed_kernal = _tmp_sz_sealed_kernal;
	sealed_tensor_t* _in_sealed_kernal = NULL;
	sealed_tensor_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_tensor_t* _in_sealed_output = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);
	CHECK_UNIQUE_POINTER(_tmp_sealed_kernal, _len_sealed_kernal);
	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);

	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	if (_tmp_sealed_kernal != NULL && _len_sealed_kernal != 0) {
		_in_sealed_kernal = (sealed_tensor_t*)malloc(_len_sealed_kernal);
		if (_in_sealed_kernal == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_kernal, _tmp_sealed_kernal, _len_sealed_kernal);
	}
	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_tensor_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	ms->ms_retval = enclave_convolute_tensor_kernal(_in_sealed_input, _tmp_sz_sealed_input, _in_sealed_kernal, _tmp_sz_sealed_kernal, _in_sealed_output, _tmp_sz_sealed_output, ms->ms_padding_y, ms->ms_padding_x, ms->ms_stride_y, ms->ms_stride_x, ms->ms_alpha, ms->ms_beta, ms->ms_is_rot180_input, ms->ms_is_rot180_kernal);
err:
	if (_in_sealed_input) free(_in_sealed_input);
	if (_in_sealed_kernal) free(_in_sealed_kernal);
	if (_in_sealed_output) {
		memcpy(_tmp_sealed_output, _in_sealed_output, _len_sealed_output);
		free(_in_sealed_output);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_tensor_elem_copab_alpha_beta(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_tensor_elem_copab_alpha_beta_t));
	ms_enclave_sealed_tensor_elem_copab_alpha_beta_t* ms = SGX_CAST(ms_enclave_sealed_tensor_elem_copab_alpha_beta_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_tensor_t* _in_sealed_A = NULL;
	sealed_tensor_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_tensor_t* _in_sealed_B = NULL;
	sealed_tensor_t* _tmp_sealed_C = ms->ms_sealed_C;
	size_t _tmp_sz_sealed_C = ms->ms_sz_sealed_C;
	size_t _len_sealed_C = _tmp_sz_sealed_C;
	sealed_tensor_t* _in_sealed_C = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);
	CHECK_UNIQUE_POINTER(_tmp_sealed_C, _len_sealed_C);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_tensor_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_tensor_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	if (_tmp_sealed_C != NULL && _len_sealed_C != 0) {
		_in_sealed_C = (sealed_tensor_t*)malloc(_len_sealed_C);
		if (_in_sealed_C == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_C, _tmp_sealed_C, _len_sealed_C);
	}
	ms->ms_retval = enclave_sealed_tensor_elem_copab_alpha_beta(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, _in_sealed_C, _tmp_sz_sealed_C, ms->ms_alpha, ms->ms_beta, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) free(_in_sealed_B);
	if (_in_sealed_C) {
		memcpy(_tmp_sealed_C, _in_sealed_C, _len_sealed_C);
		free(_in_sealed_C);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_sealed_tensor_elem_bopa(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_sealed_tensor_elem_bopa_t));
	ms_enclave_sealed_tensor_elem_bopa_t* ms = SGX_CAST(ms_enclave_sealed_tensor_elem_bopa_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_A = ms->ms_sealed_A;
	size_t _tmp_sz_sealed_A = ms->ms_sz_sealed_A;
	size_t _len_sealed_A = _tmp_sz_sealed_A;
	sealed_tensor_t* _in_sealed_A = NULL;
	sealed_tensor_t* _tmp_sealed_B = ms->ms_sealed_B;
	size_t _tmp_sz_sealed_B = ms->ms_sz_sealed_B;
	size_t _len_sealed_B = _tmp_sz_sealed_B;
	sealed_tensor_t* _in_sealed_B = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_A, _len_sealed_A);
	CHECK_UNIQUE_POINTER(_tmp_sealed_B, _len_sealed_B);

	if (_tmp_sealed_A != NULL && _len_sealed_A != 0) {
		_in_sealed_A = (sealed_tensor_t*)malloc(_len_sealed_A);
		if (_in_sealed_A == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_A, _tmp_sealed_A, _len_sealed_A);
	}
	if (_tmp_sealed_B != NULL && _len_sealed_B != 0) {
		_in_sealed_B = (sealed_tensor_t*)malloc(_len_sealed_B);
		if (_in_sealed_B == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_B, _tmp_sealed_B, _len_sealed_B);
	}
	ms->ms_retval = enclave_sealed_tensor_elem_bopa(_in_sealed_A, _tmp_sz_sealed_A, _in_sealed_B, _tmp_sz_sealed_B, ms->ms_func_name);
err:
	if (_in_sealed_A) free(_in_sealed_A);
	if (_in_sealed_B) {
		memcpy(_tmp_sealed_B, _in_sealed_B, _len_sealed_B);
		free(_in_sealed_B);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_tensor_pooling(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_tensor_pooling_t));
	ms_enclave_tensor_pooling_t* ms = SGX_CAST(ms_enclave_tensor_pooling_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;
	sealed_tensor_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_tensor_t* _in_sealed_output = NULL;
	sealed_tensor_t* _tmp_sealed_auxiliary = ms->ms_sealed_auxiliary;
	size_t _tmp_sz_sealed_auxiliary = ms->ms_sz_sealed_auxiliary;
	size_t _len_sealed_auxiliary = _tmp_sz_sealed_auxiliary;
	sealed_tensor_t* _in_sealed_auxiliary = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);
	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);
	CHECK_UNIQUE_POINTER(_tmp_sealed_auxiliary, _len_sealed_auxiliary);

	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_tensor_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	if (_tmp_sealed_auxiliary != NULL && _len_sealed_auxiliary != 0) {
		_in_sealed_auxiliary = (sealed_tensor_t*)malloc(_len_sealed_auxiliary);
		if (_in_sealed_auxiliary == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_auxiliary, _tmp_sealed_auxiliary, _len_sealed_auxiliary);
	}
	ms->ms_retval = enclave_tensor_pooling(_in_sealed_input, _tmp_sz_sealed_input, _in_sealed_output, _tmp_sz_sealed_output, _in_sealed_auxiliary, _tmp_sz_sealed_auxiliary, ms->ms_field_y, ms->ms_field_x, ms->ms_padding_y, ms->ms_padding_x, ms->ms_stride_y, ms->ms_stride_x, ms->ms_pooling_name);
err:
	if (_in_sealed_input) free(_in_sealed_input);
	if (_in_sealed_output) {
		memcpy(_tmp_sealed_output, _in_sealed_output, _len_sealed_output);
		free(_in_sealed_output);
	}
	if (_in_sealed_auxiliary) {
		memcpy(_tmp_sealed_auxiliary, _in_sealed_auxiliary, _len_sealed_auxiliary);
		free(_in_sealed_auxiliary);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_tensor_flatten(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_tensor_flatten_t));
	ms_enclave_tensor_flatten_t* ms = SGX_CAST(ms_enclave_tensor_flatten_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;
	sealed_divided_matrix_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_divided_matrix_t* _in_sealed_output = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);
	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);

	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_divided_matrix_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	ms->ms_retval = enclave_tensor_flatten(_in_sealed_input, _tmp_sz_sealed_input, _in_sealed_output, _tmp_sz_sealed_output);
err:
	if (_in_sealed_input) free(_in_sealed_input);
	if (_in_sealed_output) {
		memcpy(_tmp_sealed_output, _in_sealed_output, _len_sealed_output);
		free(_in_sealed_output);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_tensor_backward_flatten(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_tensor_backward_flatten_t));
	ms_enclave_tensor_backward_flatten_t* ms = SGX_CAST(ms_enclave_tensor_backward_flatten_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_divided_matrix_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_divided_matrix_t* _in_sealed_output = NULL;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);
	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);

	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_divided_matrix_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	ms->ms_retval = enclave_tensor_backward_flatten(_in_sealed_output, _tmp_sz_sealed_output, _in_sealed_input, _tmp_sz_sealed_input);
err:
	if (_in_sealed_output) free(_in_sealed_output);
	if (_in_sealed_input) {
		memcpy(_tmp_sealed_input, _in_sealed_input, _len_sealed_input);
		free(_in_sealed_input);
	}

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_backward_convolute_tensor_kernal(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_backward_convolute_tensor_kernal_t));
	ms_enclave_backward_convolute_tensor_kernal_t* ms = SGX_CAST(ms_enclave_backward_convolute_tensor_kernal_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;
	sealed_tensor_t* _tmp_sealed_kernal = ms->ms_sealed_kernal;
	size_t _tmp_sz_sealed_kernal = ms->ms_sz_sealed_kernal;
	size_t _len_sealed_kernal = _tmp_sz_sealed_kernal;
	sealed_tensor_t* _in_sealed_kernal = NULL;
	sealed_tensor_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_tensor_t* _in_sealed_output = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);
	CHECK_UNIQUE_POINTER(_tmp_sealed_kernal, _len_sealed_kernal);
	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);

	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	if (_tmp_sealed_kernal != NULL && _len_sealed_kernal != 0) {
		_in_sealed_kernal = (sealed_tensor_t*)malloc(_len_sealed_kernal);
		if (_in_sealed_kernal == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_kernal, _tmp_sealed_kernal, _len_sealed_kernal);
	}
	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_tensor_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	ms->ms_retval = enclave_backward_convolute_tensor_kernal(_in_sealed_input, _tmp_sz_sealed_input, _in_sealed_kernal, _tmp_sz_sealed_kernal, _in_sealed_output, _tmp_sz_sealed_output, ms->ms_padding_y, ms->ms_padding_x, ms->ms_stride_y, ms->ms_stride_x, ms->ms_alpha, ms->ms_beta);
err:
	if (_in_sealed_input) free(_in_sealed_input);
	if (_in_sealed_kernal) {
		memcpy(_tmp_sealed_kernal, _in_sealed_kernal, _len_sealed_kernal);
		free(_in_sealed_kernal);
	}
	if (_in_sealed_output) free(_in_sealed_output);

	return status;
}

static sgx_status_t SGX_CDECL sgx_enclave_backward_convolute_tensor_input(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_enclave_backward_convolute_tensor_input_t));
	ms_enclave_backward_convolute_tensor_input_t* ms = SGX_CAST(ms_enclave_backward_convolute_tensor_input_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	sealed_tensor_t* _tmp_sealed_input = ms->ms_sealed_input;
	size_t _tmp_sz_sealed_input = ms->ms_sz_sealed_input;
	size_t _len_sealed_input = _tmp_sz_sealed_input;
	sealed_tensor_t* _in_sealed_input = NULL;
	sealed_tensor_t* _tmp_sealed_kernal = ms->ms_sealed_kernal;
	size_t _tmp_sz_sealed_kernal = ms->ms_sz_sealed_kernal;
	size_t _len_sealed_kernal = _tmp_sz_sealed_kernal;
	sealed_tensor_t* _in_sealed_kernal = NULL;
	sealed_tensor_t* _tmp_sealed_output = ms->ms_sealed_output;
	size_t _tmp_sz_sealed_output = ms->ms_sz_sealed_output;
	size_t _len_sealed_output = _tmp_sz_sealed_output;
	sealed_tensor_t* _in_sealed_output = NULL;

	CHECK_UNIQUE_POINTER(_tmp_sealed_input, _len_sealed_input);
	CHECK_UNIQUE_POINTER(_tmp_sealed_kernal, _len_sealed_kernal);
	CHECK_UNIQUE_POINTER(_tmp_sealed_output, _len_sealed_output);

	if (_tmp_sealed_input != NULL && _len_sealed_input != 0) {
		_in_sealed_input = (sealed_tensor_t*)malloc(_len_sealed_input);
		if (_in_sealed_input == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_input, _tmp_sealed_input, _len_sealed_input);
	}
	if (_tmp_sealed_kernal != NULL && _len_sealed_kernal != 0) {
		_in_sealed_kernal = (sealed_tensor_t*)malloc(_len_sealed_kernal);
		if (_in_sealed_kernal == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_kernal, _tmp_sealed_kernal, _len_sealed_kernal);
	}
	if (_tmp_sealed_output != NULL && _len_sealed_output != 0) {
		_in_sealed_output = (sealed_tensor_t*)malloc(_len_sealed_output);
		if (_in_sealed_output == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memcpy(_in_sealed_output, _tmp_sealed_output, _len_sealed_output);
	}
	ms->ms_retval = enclave_backward_convolute_tensor_input(_in_sealed_input, _tmp_sz_sealed_input, _in_sealed_kernal, _tmp_sz_sealed_kernal, _in_sealed_output, _tmp_sz_sealed_output, ms->ms_padding_y, ms->ms_padding_x, ms->ms_stride_y, ms->ms_stride_x, ms->ms_alpha, ms->ms_beta);
err:
	if (_in_sealed_input) {
		memcpy(_tmp_sealed_input, _in_sealed_input, _len_sealed_input);
		free(_in_sealed_input);
	}
	if (_in_sealed_kernal) free(_in_sealed_kernal);
	if (_in_sealed_output) free(_in_sealed_output);

	return status;
}

SGX_EXTERNC const struct {
	size_t nr_ecall;
	struct {void* call_addr; uint8_t is_priv;} ecall_table[29];
} g_ecall_table = {
	29,
	{
		{(void*)(uintptr_t)sgx_enclave_calc_sealed_block_matrix_size, 0},
		{(void*)(uintptr_t)sgx_enclave_create_sealed_block_matrix, 0},
		{(void*)(uintptr_t)sgx_enclave_unseal_block_matrix, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_block_gemm, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_gemm_with_functions, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_gemm, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_elem_bopab, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_elem_copab_alpha_beta, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_geadd, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_elem_bopa, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_elem_aopa, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_aggregate_rows, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_divided_aggregate_rows_exponetial_add, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_gemm, 0},
		{(void*)(uintptr_t)sgx_enclave_output_layer, 0},
		{(void*)(uintptr_t)sgx_enclave_output_layer_acc, 0},
		{(void*)(uintptr_t)sgx_enclave_to_zeros, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_size, 0},
		{(void*)(uintptr_t)sgx_enclave_seal_matrix, 0},
		{(void*)(uintptr_t)sgx_enclave_unseal_matrix, 0},
		{(void*)(uintptr_t)sgx_enclave_unseal_block_pointer, 0},
		{(void*)(uintptr_t)sgx_enclave_convolute_tensor_kernal, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_tensor_elem_copab_alpha_beta, 0},
		{(void*)(uintptr_t)sgx_enclave_sealed_tensor_elem_bopa, 0},
		{(void*)(uintptr_t)sgx_enclave_tensor_pooling, 0},
		{(void*)(uintptr_t)sgx_enclave_tensor_flatten, 0},
		{(void*)(uintptr_t)sgx_enclave_tensor_backward_flatten, 0},
		{(void*)(uintptr_t)sgx_enclave_backward_convolute_tensor_kernal, 0},
		{(void*)(uintptr_t)sgx_enclave_backward_convolute_tensor_input, 0},
	}
};

SGX_EXTERNC const struct {
	size_t nr_ocall;
	uint8_t entry_table[7][29];
} g_dyn_entry_table = {
	7,
	{
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	}
};


sgx_status_t SGX_CDECL get_memory_ocall(uint32_t* retval, uint8_t* memory_ptr, uint8_t* receive_memory, size_t sz_memory)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_receive_memory = sz_memory;

	ms_get_memory_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_get_memory_ocall_t);
	void *__tmp = NULL;

	ocalloc_size += (receive_memory != NULL && sgx_is_within_enclave(receive_memory, _len_receive_memory)) ? _len_receive_memory : 0;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_get_memory_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_get_memory_ocall_t));

	ms->ms_memory_ptr = SGX_CAST(uint8_t*, memory_ptr);
	if (receive_memory != NULL && sgx_is_within_enclave(receive_memory, _len_receive_memory)) {
		ms->ms_receive_memory = (uint8_t*)__tmp;
		__tmp = (void *)((size_t)__tmp + _len_receive_memory);
		memset(ms->ms_receive_memory, 0, _len_receive_memory);
	} else if (receive_memory == NULL) {
		ms->ms_receive_memory = NULL;
	} else {
		sgx_ocfree();
		return SGX_ERROR_INVALID_PARAMETER;
	}
	
	ms->ms_sz_memory = sz_memory;
	status = sgx_ocall(0, ms);

	if (retval) *retval = ms->ms_retval;
	if (receive_memory) memcpy((void*)receive_memory, ms->ms_receive_memory, _len_receive_memory);

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL place_memory_ocall(uint32_t* retval, uint8_t* memory_from_enclave, uint8_t* memory_in_untrusted, size_t sz_memory)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_memory_from_enclave = sz_memory;

	ms_place_memory_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_place_memory_ocall_t);
	void *__tmp = NULL;

	ocalloc_size += (memory_from_enclave != NULL && sgx_is_within_enclave(memory_from_enclave, _len_memory_from_enclave)) ? _len_memory_from_enclave : 0;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_place_memory_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_place_memory_ocall_t));

	if (memory_from_enclave != NULL && sgx_is_within_enclave(memory_from_enclave, _len_memory_from_enclave)) {
		ms->ms_memory_from_enclave = (uint8_t*)__tmp;
		__tmp = (void *)((size_t)__tmp + _len_memory_from_enclave);
		memcpy(ms->ms_memory_from_enclave, memory_from_enclave, _len_memory_from_enclave);
	} else if (memory_from_enclave == NULL) {
		ms->ms_memory_from_enclave = NULL;
	} else {
		sgx_ocfree();
		return SGX_ERROR_INVALID_PARAMETER;
	}
	
	ms->ms_memory_in_untrusted = SGX_CAST(uint8_t*, memory_in_untrusted);
	ms->ms_sz_memory = sz_memory;
	status = sgx_ocall(1, ms);

	if (retval) *retval = ms->ms_retval;

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_oc_cpuidex(int cpuinfo[4], int leaf, int subleaf)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_cpuinfo = 4 * sizeof(*cpuinfo);

	ms_sgx_oc_cpuidex_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_oc_cpuidex_t);
	void *__tmp = NULL;

	ocalloc_size += (cpuinfo != NULL && sgx_is_within_enclave(cpuinfo, _len_cpuinfo)) ? _len_cpuinfo : 0;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_oc_cpuidex_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_oc_cpuidex_t));

	if (cpuinfo != NULL && sgx_is_within_enclave(cpuinfo, _len_cpuinfo)) {
		ms->ms_cpuinfo = (int*)__tmp;
		__tmp = (void *)((size_t)__tmp + _len_cpuinfo);
		memset(ms->ms_cpuinfo, 0, _len_cpuinfo);
	} else if (cpuinfo == NULL) {
		ms->ms_cpuinfo = NULL;
	} else {
		sgx_ocfree();
		return SGX_ERROR_INVALID_PARAMETER;
	}
	
	ms->ms_leaf = leaf;
	ms->ms_subleaf = subleaf;
	status = sgx_ocall(2, ms);

	if (cpuinfo) memcpy((void*)cpuinfo, ms->ms_cpuinfo, _len_cpuinfo);

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_wait_untrusted_event_ocall(int* retval, const void* self)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_wait_untrusted_event_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_wait_untrusted_event_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_wait_untrusted_event_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_wait_untrusted_event_ocall_t));

	ms->ms_self = SGX_CAST(void*, self);
	status = sgx_ocall(3, ms);

	if (retval) *retval = ms->ms_retval;

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_set_untrusted_event_ocall(int* retval, const void* waiter)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_set_untrusted_event_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_set_untrusted_event_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_set_untrusted_event_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_set_untrusted_event_ocall_t));

	ms->ms_waiter = SGX_CAST(void*, waiter);
	status = sgx_ocall(4, ms);

	if (retval) *retval = ms->ms_retval;

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_setwait_untrusted_events_ocall(int* retval, const void* waiter, const void* self)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_setwait_untrusted_events_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_setwait_untrusted_events_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_setwait_untrusted_events_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_setwait_untrusted_events_ocall_t));

	ms->ms_waiter = SGX_CAST(void*, waiter);
	ms->ms_self = SGX_CAST(void*, self);
	status = sgx_ocall(5, ms);

	if (retval) *retval = ms->ms_retval;

	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_set_multiple_untrusted_events_ocall(int* retval, const void** waiters, size_t total)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_waiters = total * sizeof(*waiters);

	ms_sgx_thread_set_multiple_untrusted_events_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_set_multiple_untrusted_events_ocall_t);
	void *__tmp = NULL;

	ocalloc_size += (waiters != NULL && sgx_is_within_enclave(waiters, _len_waiters)) ? _len_waiters : 0;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_set_multiple_untrusted_events_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_set_multiple_untrusted_events_ocall_t));

	if (waiters != NULL && sgx_is_within_enclave(waiters, _len_waiters)) {
		ms->ms_waiters = (void**)__tmp;
		__tmp = (void *)((size_t)__tmp + _len_waiters);
		memcpy((void*)ms->ms_waiters, waiters, _len_waiters);
	} else if (waiters == NULL) {
		ms->ms_waiters = NULL;
	} else {
		sgx_ocfree();
		return SGX_ERROR_INVALID_PARAMETER;
	}
	
	ms->ms_total = total;
	status = sgx_ocall(6, ms);

	if (retval) *retval = ms->ms_retval;

	sgx_ocfree();
	return status;
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif
