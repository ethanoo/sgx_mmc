#include <random>
#include <algorithm>
#include <cstdlib>

#include "stdafx.h"
#include <tchar.h>

#define ENCLAVE_FILE _T("enclave_mmc.signed.dll")

#include "sgx_urts.h"
#include "sgx_report.h"
#include "sgx_error.h"
#include "datatypes.h"
#include "enclave_mmc_u.h"
#include "mnist_image_parser.h"
#include "construction.h"

#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UntrustedUnittest {
	TEST_CLASS(SealBlockUnittests) {
	public:
		sgx_enclave_id_t   eid;
		sgx_status_t       ret = SGX_SUCCESS;
		sgx_launch_token_t token = { 0 };
		int updated = 0;

		uint32_t m = 5;
		uint32_t n = 4;
		uint32_t k = 9;

		float *A, *B, *C, *D;
		uint32_t sz_A, sz_B, sz_C, sz_D;

		void init() {
			// ret = sgx_create_enclave(ENCLAVE_FILE, SGX_DEBUG_FLAG, &token, &updated, &eid, NULL);
			ret = sgx_create_enclave(ENCLAVE_FILE, 1, &token, &updated, &eid, NULL);
			Assert::AreEqual((uint32_t) ret, (uint32_t) SGX_SUCCESS);

			sz_A = m * k * sizeof(float);
			sz_B = k * n * sizeof(float);
			sz_C = m * n * sizeof(float);
			sz_D = m * k * sizeof(float);
			A = (float*) malloc(sz_A);
			B = (float*) malloc(sz_B);
			C = (float*) malloc(sz_C);
			D = (float*) malloc(sz_D);

			Assert::AreNotEqual((uint32_t) A, (uint32_t) NULL);
			Assert::AreNotEqual((uint32_t) B, (uint32_t) NULL);
			Assert::AreNotEqual((uint32_t) C, (uint32_t) NULL);
			Assert::AreNotEqual((uint32_t) D, (uint32_t) NULL);
		}

		void end() {
			free(A);
			free(B);
			free(C);
			free(D);

			sgx_status_t destroy_ret = sgx_destroy_enclave(eid);
			Assert::AreEqual((uint32_t) SGX_SUCCESS, (uint32_t) destroy_ret);
		}


		TEST_METHOD(BlockMatrixSealAndUnseal) {
			init();

			FOR(i, m) FOR(j, k) A[i * k + j] = 12.0f;

			uint32_t sz_sealed_block_A;
			sealed_block_matrix_t* sealed_block_A;

			create_sealed_block(eid, &sealed_block_A, &sz_sealed_block_A, m, k);

			enclave_create_sealed_block_matrix(
				eid, NULL,
				sealed_block_A, sz_sealed_block_A,
				m, k,
				A, sz_A);

			enclave_unseal_block_matrix(
				eid, NULL,
				sealed_block_A, sz_sealed_block_A,
				A, sz_A);

			FOR(i, m) FOR(j, k) Assert::AreEqual(A[i * k + j], 12.0f, 1e-6f);

			free(sealed_block_A);

			end();
		}

		TEST_METHOD(BlockMatrixRetrieve) {
			init();

			FOR(i, m) FOR(j, k) A[i * k + j] = 12.0f;

			size_t sz_sealed_block_A;
			sealed_block_matrix_t *sealed_block_A, *retrieve_sealed_block;

			create_sealed_block(eid, &sealed_block_A, &sz_sealed_block_A, m, k);

			enclave_create_sealed_block_matrix(
				eid, NULL,
				sealed_block_A, sz_sealed_block_A,
				m, k,
				A, sz_A);

			retrieve_sealed_block = (sealed_block_matrix_t*)malloc(sz_sealed_block_A);
			enclave_unseal_block_pointer(eid, NULL, sealed_block_A, retrieve_sealed_block, sz_sealed_block_A);

			enclave_unseal_block_matrix(
				eid, NULL,
				retrieve_sealed_block, sz_sealed_block_A,
				A, sz_A);

			FOR(i, m) FOR(j, k) Assert::AreEqual(12.0f, A[i * k + j], 1e-6f);

			free(sealed_block_A);

			end();
		}

		TEST_METHOD(BlockMatrixSealGEMMUnseal)
		{
			init();

			int accum = 0;
			FOR(i, m) FOR(j, k) {
				A[accum++] = 3.0f;
			}
			accum = 0;
			FOR(i, k) FOR(j, n) {
				B[accum++] = 5.0f;
			}

			uint32_t sz_sealed_block_A, sz_sealed_block_B, sz_sealed_block_C;
			sealed_block_matrix_t *sealed_block_A, *sealed_block_B, *sealed_block_C;

			create_sealed_block(eid, &sealed_block_A, &sz_sealed_block_A, m, k);
			create_sealed_block(eid, &sealed_block_B, &sz_sealed_block_B, k, n);
			create_sealed_block(eid, &sealed_block_C, &sz_sealed_block_C, m, n);

			uint32_t proc_ret = ENCLAVE_SUCCESS;

			// Loading data into sealed block
			enclave_create_sealed_block_matrix(
				eid, &proc_ret,
				sealed_block_A, sz_sealed_block_A,
				m, k,
				A, sz_A);
			Assert::AreEqual((uint32_t) ENCLAVE_SUCCESS, (uint32_t) proc_ret);

			enclave_create_sealed_block_matrix(
				eid, NULL,
				sealed_block_B, sz_sealed_block_B,
				k, n,
				B, sz_B);
			Assert::AreEqual((uint32_t) ENCLAVE_SUCCESS, (uint32_t) proc_ret);

			enclave_create_sealed_block_matrix(
				eid, NULL,
				sealed_block_C, sz_sealed_block_C,
				m, n,
				C, sz_C);
			Assert::AreEqual((uint32_t) ENCLAVE_SUCCESS, (uint32_t) proc_ret);

			// Loading data into sealed block
			enclave_sealed_block_gemm(
				eid, NULL,
				sealed_block_A, sz_sealed_block_A,
				sealed_block_B, sz_sealed_block_B,
				sealed_block_C, sz_sealed_block_C,
				0, 0,
				1.0f, 0.0f);

			enclave_unseal_block_matrix(
				eid, NULL,
				sealed_block_C, sz_sealed_block_C,
				C, sz_C);

			FOR(i, m) FOR(j, n) Assert::AreEqual(k * 3.0f * 5.0f, C[i * n + j], 1e-6f);

			free(sealed_block_A);
			free(sealed_block_B);
			free(sealed_block_C);

			end();
		}

	};

	TEST_CLASS(SealedDividedUnittests) {
	public:
		sgx_enclave_id_t   eid;
		sgx_status_t       ret = SGX_SUCCESS;
		sgx_launch_token_t token = { 0 };
		int updated = 0;

		void init() {
			// ret = sgx_create_enclave(ENCLAVE_FILE, SGX_DEBUG_FLAG, &token, &updated, &eid, NULL);
			ret = sgx_create_enclave(ENCLAVE_FILE, 1, &token, &updated, &eid, NULL);
			Assert::AreEqual((uint32_t) ret, (uint32_t) SGX_SUCCESS);
		}

		void end() {
			sgx_status_t destroy_ret = sgx_destroy_enclave(eid);
			Assert::AreEqual((uint32_t) SGX_SUCCESS, (uint32_t) destroy_ret);
		}

		TEST_METHOD(DividedMatrixSealUnseal) {
			uint32_t M = 95;
			uint32_t N = 105;
			uint32_t m = 12;
			uint32_t n = 13;

			init();

			size_t sz_A = M * N * sizeof(float);
			float *A = (float*)malloc(sz_A);
			FOR(i, M) FOR(j, N) A[i * N + j] = 12.0f;

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, N, m, n);
			uint32_t ret_proc = ENCLAVE_SUCCESS;

			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			float p = 0;
			uint32_t idx = 0;
			bool condition = false;
			FOR(i, M) {
				FOR(j, N) {
					Assert::AreEqual(12.0f, A[i * N + j], 1e-6f);
				}
			}

			free(A);
			destory_sealed_divided_matrix(sealed_divided_A);

			end();
		}

		TEST_METHOD(DividedMatrixGEMM) {
#ifdef _DEBUG
			uint32_t M = 95;
			uint32_t N = 66;
			uint32_t K = 153;

			uint32_t m = 12;
			uint32_t n = 13;
			uint32_t k = 17;
#else
			uint32_t M = 1000;
			uint32_t N = 1000;
			uint32_t K = 1000;

			uint32_t m = 224;
			uint32_t n = 224;
			uint32_t k = 224;
#endif // _DEBUG


			float A_elem = 5.0f;
			float B_elem = 7.0f;

			init();

			size_t sz_A = M * K * sizeof(float);
			size_t sz_B = K * N * sizeof(float);
			size_t sz_C = M * N * sizeof(float);

			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			float *C = (float*)malloc(sz_C);

			FOR(i, M) FOR(j, K) A[i * K + j] = (i + 1.0f) * (j - 1.0f);
			FOR(i, K) FOR(j, N) B[i * N + j] = (i + 2.0f) * (j - 2.0f);

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, K, m, k);
			sealed_divided_matrix_t* sealed_divided_B = create_empty_sealed_divided_matrix(eid, K, N, k, n);
			sealed_divided_matrix_t* sealed_divided_C = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			uint32_t ret_proc = ENCLAVE_SUCCESS;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_divided_gemm(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				sealed_divided_C, sealed_divided_C->sz_itself,
				0, 0,
				1.0, 0.0);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			FOR(i, M) {
				FOR(j, N) {
					float expected = (i + 1.0f) * (j - 2.0f) * K * (K * K - 7.0f) / 3.0f;
					float actual = C[i * N + j];
					Assert::AreEqual(expected, actual, (expected + actual) * 1e-3f);
				}
			}

			free(A);
			free(B);
			free(C);
			destory_sealed_divided_matrix(sealed_divided_A);
			destory_sealed_divided_matrix(sealed_divided_B);
			destory_sealed_divided_matrix(sealed_divided_C);

			end();
		}

		TEST_METHOD(DividedMatrixGEMM_TT) {
#ifdef _DEBUG
			uint32_t M = 95;
			uint32_t N = 66;
			uint32_t K = 153;

			uint32_t m = 12;
			uint32_t n = 13;
			uint32_t k = 17;
#else
			uint32_t M = 1000;
			uint32_t N = 1000;
			uint32_t K = 1000;

			uint32_t m = 50;
			uint32_t n = 50;
			uint32_t k = 50;
#endif // _DEBUG


			float A_elem = 5.0f;
			float B_elem = 7.0f;

			init();

			size_t sz_A = M * K * sizeof(float);
			size_t sz_B = K * N * sizeof(float);
			size_t sz_C = M * N * sizeof(float);

			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			float *C = (float*)malloc(sz_C);

			FOR(i, K) FOR(j, M) A[i * M + j] = (i - 1.0f) * (j + 1.0f);
			FOR(i, N) FOR(j, K) B[i * K + j] = (i - 2.0f) * (j + 2.0f);

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, K, M, k, m);
			sealed_divided_matrix_t* sealed_divided_B = create_empty_sealed_divided_matrix(eid, N, K, n, k);
			sealed_divided_matrix_t* sealed_divided_C = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			uint32_t ret_proc = ENCLAVE_SUCCESS;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_divided_gemm(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				sealed_divided_C, sealed_divided_C->sz_itself,
				1, 1,
				1.0, 0.0);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			FOR(i, M) {
				FOR(j, N) {
					float expected = (i + 1.0f) * (j - 2.0f) * K * (K * K - 7.0f) / 3.0f;
					float actual = C[i * N + j];
					Assert::AreEqual(expected, actual, (expected + actual) * 1e-3f);
				}
			}

			free(A);
			free(B);
			free(C);
			destory_sealed_divided_matrix(sealed_divided_A);
			destory_sealed_divided_matrix(sealed_divided_B);
			destory_sealed_divided_matrix(sealed_divided_C);

			end();
		}

		TEST_METHOD(DividedMatrixElemBOPAB) {
			// Status flags
			sgx_status_t proc_ret = SGX_SUCCESS;
			uint32_t ocall_ret = OCALL_SUCCESS;
			uint32_t enclave_ret = ENCLAVE_SUCCESS;

			uint32_t M = 28;
			uint32_t N = 20;

			uint32_t m = 12;
			uint32_t n = 13;

			float A_elem = 8.0f;
			float B_elem = 31.0f;
			init();

			size_t sz_A = M * N * sizeof(float);
			size_t sz_B = M * N * sizeof(float);
			size_t sz_C = M * N * sizeof(float);

			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			float *C = (float*)malloc(sz_C);

			FOR(i, M) FOR(j, N) A[i * N + j] = A_elem;
			FOR(i, M) FOR(j, N) B[i * N + j] = B_elem;
			FOR(i, M) FOR(j, N) C[i * N + j] = B_elem;

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, N, m, n);
			sealed_divided_matrix_t* sealed_divided_B = create_empty_sealed_divided_matrix(eid, M, N, m, n);
			sealed_divided_matrix_t* sealed_divided_C = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			uint32_t ret_proc = ENCLAVE_SUCCESS;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			auto run_check = [&](float b) {
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				// Assert::IsTrue(temp_b != 0 || temp_a != 0);
				Assert::IsTrue(b != 0);

				ret_proc = unseal_divided_matrix(eid, sealed_divided_B, B, sz_B);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				FOR(i, M) FOR(j, N) Assert::AreEqual(b, B[i * N + j]);
			};

			auto test_func = [&](float b, elem_funcname_t func_name) {
				enclave_sealed_divided_elem_bopab(
					eid, &ret_proc,
					sealed_divided_A, sealed_divided_A->sz_itself,
					sealed_divided_B, sealed_divided_B->sz_itself,
					func_name);
				run_check(b);
			};

			float temp_a = A_elem;
			float temp_b = B_elem;
			float temp_c;

			// B = B + A
			temp_b = temp_a + temp_b;
			// test_func(temp_b, ADD);
			enclave_sealed_divided_elem_bopab(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				ADD);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			// Assert::IsTrue(temp_b != 0 || temp_a != 0);
			Assert::IsTrue(temp_b != 0);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) FOR(j, N) Assert::AreEqual(A_elem + B_elem, B[i * N + j]);

			// B = A - B
			temp_b = temp_a - temp_b;
			test_func(temp_b, SUBTRACT_AB);

			// B = B - A
			temp_b = temp_b - temp_a;
			test_func(temp_b, SUBTRACT_BA);

			// B = A * B
			temp_b = temp_a * temp_b;
			test_func(temp_b, PRODUCT);

			float alpha = 3.7;
			float beta = 0.2;

			// B = alpha * A + beta * B
			temp_b = alpha * temp_a + beta * temp_b;
			enclave_sealed_divided_geadd(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				alpha, beta,
				ADD);
			run_check(temp_b);

			// C = alpha * A + beta * B
			temp_c = alpha * temp_a + beta * temp_b;
			enclave_sealed_divided_elem_copab_alpha_beta(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				sealed_divided_C, sealed_divided_C->sz_itself,
				alpha, beta,
				ADD);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			Assert::IsTrue(temp_b != 0 || temp_a != 0);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) FOR(j, N) Assert::AreEqual(temp_c, C[i * N + j]);

			free(A);
			free(B);
			free(C);
			destory_sealed_divided_matrix(sealed_divided_A);
			destory_sealed_divided_matrix(sealed_divided_B);
			destory_sealed_divided_matrix(sealed_divided_C);

			end();
		}

		TEST_METHOD(DividedMatrixElemBOPA) {
			// Status flags
			uint32_t proc_ret = SGX_SUCCESS;
			uint32_t ocall_ret = OCALL_SUCCESS;
			uint32_t enclave_ret = ENCLAVE_SUCCESS;

			uint32_t M = 95;
			uint32_t N = 66;

			uint32_t m = 12;
			uint32_t n = 13;

			float A_elem = 8.0f;
			float temp_a = A_elem;
			float temp_b = temp_a;

			init();

			size_t sz_A = M * N * sizeof(float);
			size_t sz_B = M * N * sizeof(float);
			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			FOR(i, M) FOR(j, N) A[i * N + j] = A_elem;

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, N, m, n);
			sealed_divided_matrix_t* sealed_divided_B = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			enclave_ret = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(enclave_ret == ENCLAVE_SUCCESS);

			auto sigmoid = [](float x) { return 1.0f / (1.0f + exp(-(x))); };

			proc_ret = enclave_sealed_divided_elem_bopa( 
				eid, &enclave_ret, 
				sealed_divided_A, sealed_divided_A->sz_itself, 
				sealed_divided_B, sealed_divided_B->sz_itself, 
				SIGMOID);
			Assert::IsTrue(proc_ret == ENCLAVE_SUCCESS);
			temp_b = sigmoid(temp_a);

			proc_ret = unseal_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(proc_ret == ENCLAVE_SUCCESS);
			FOR(i, M) FOR(j, N) Assert::AreEqual(temp_b, B[i * N + j]);

			free(A);
			free(B);
			destory_sealed_divided_matrix(sealed_divided_A);
			destory_sealed_divided_matrix(sealed_divided_B);

			end();
		}

		TEST_METHOD(DividedMatrixElemAOPA) {
			// Status flags
			uint32_t proc_ret = SGX_SUCCESS;
			uint32_t ocall_ret = OCALL_SUCCESS;
			uint32_t enclave_ret = ENCLAVE_SUCCESS;

			uint32_t M = 95;
			uint32_t N = 66;

			uint32_t m = 12;
			uint32_t n = 13;

			float A_elem = 8.0f;
			float temp_a = A_elem;

			init();

			size_t sz_A = M * N * sizeof(float);
			float *A = (float*)malloc(sz_A);
			FOR(i, M) FOR(j, N) A[i * N + j] = A_elem;

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			enclave_ret = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(enclave_ret == ENCLAVE_SUCCESS);

			auto sigmoid = [](float x) { return 1.0f / (1.0f + exp(-(x))); };

			proc_ret = enclave_sealed_divided_elem_aopa( eid, &enclave_ret, sealed_divided_A, sealed_divided_A->sz_itself, SIGMOID);
			Assert::IsTrue(proc_ret == ENCLAVE_SUCCESS);
			temp_a = sigmoid(temp_a);

			proc_ret = unseal_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(proc_ret == ENCLAVE_SUCCESS);
			FOR(i, M) FOR(j, N) Assert::AreEqual(temp_a, A[i * N + j]);

			free(A);
			destory_sealed_divided_matrix(sealed_divided_A);

			end();
		}

		TEST_METHOD(DividedMatrixGEMM_with_functions) {
#ifdef _DEBUG
			uint32_t M = 95;
			uint32_t N = 66;
			uint32_t K = 153;

			uint32_t m = 12;
			uint32_t n = 13;
			uint32_t k = 17;
#else
			uint32_t M = 1000;
			uint32_t N = 1000;
			uint32_t K = 1000;

			uint32_t m = 30;
			uint32_t n = 30;
			uint32_t k = 30;
#endif // _DEBUG


			float A_elem = 5.0f;
			float B_elem = 7.0f;

			init();

			size_t sz_A = M * K * sizeof(float);
			size_t sz_B = K * N * sizeof(float);
			size_t sz_C = M * N * sizeof(float);

			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			float *C = (float*)malloc(sz_C);

			FOR(i, M) FOR(j, K) A[i * K + j] = A_elem;
			FOR(i, K) FOR(j, N) B[i * N + j] = B_elem;

			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, K, m, k);
			sealed_divided_matrix_t* sealed_divided_B = create_empty_sealed_divided_matrix(eid, K, N, k, n);
			sealed_divided_matrix_t* sealed_divided_C = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			uint32_t ret_proc = ENCLAVE_SUCCESS;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_B, B, sz_B);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_divided_gemm_with_functions(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				sealed_divided_B, sealed_divided_B->sz_itself,
				sealed_divided_C, sealed_divided_C->sz_itself,
				0, 0,
				1.0, 0.0,
				IDENTICAL, IDENTICAL,
				SIGMOID);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_divided_matrix(eid, sealed_divided_C, C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			auto sigmoid = [](float x) { return 1.0f / (1.0f + exp(-(x))); };
			FOR(i, M) {
				FOR(j, N) {
					Assert::AreEqual(sigmoid(K * A_elem * B_elem), C[i * N + j]);
				}
			}

			free(A);
			free(B);
			free(C);
			destory_sealed_divided_matrix(sealed_divided_A);
			destory_sealed_divided_matrix(sealed_divided_B);
			destory_sealed_divided_matrix(sealed_divided_C);

			end();
		}

		TEST_METHOD(DividedMatrixAggRows) {
			uint32_t M = 1000;
			uint32_t N = 1000;

			uint32_t m = 49;
			uint32_t n = 49;

			init();

			size_t sz_A = M * N * sizeof(float);
			size_t sz_B = M * sizeof(float);
			size_t sz_C = M * sizeof(float);

			float *A = (float*)malloc(sz_A);
			float *B = (float*)malloc(sz_B);
			float *C = (float*)malloc(sz_C);

			FOR(i, M) FOR(j, N) A[i * N + j] = i * j;
			sealed_divided_matrix_t* sealed_divided_A = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			uint32_t ret_proc = ENCLAVE_SUCCESS;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_divided_aggregate_rows(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				B, sz_B,
				ADD);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) Assert::AreEqual(i * N * (N - 1) / 2.0f, B[i], i * N * N * 1e-3f); 

			enclave_sealed_divided_aggregate_rows(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				B, sz_B,
				MAX);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) Assert::AreEqual((float) i * (N - 1), B[i]) ; 

			FOR(i, M) FOR(j, N) A[i * N + j] = -(i + 1) * (j + 1);
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			enclave_sealed_divided_aggregate_rows(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				B, sz_B,
				MAX);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) Assert::AreEqual((float) -(i + 1), B[i]); 

			FOR(i, M) FOR(j, N) A[i * N + j] = i * j;
			ret_proc = load_sealed_divided_matrix(eid, sealed_divided_A, A, sz_A);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			FOR(i, M) C[i] = 10 * i;
			enclave_sealed_divided_aggregate_rows_exponetial_add(
				eid, &ret_proc,
				sealed_divided_A, sealed_divided_A->sz_itself,
				B, sz_B,
				C, sz_C);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, M) {
				float expected_res = 0.0f;
				FOR(j, N) expected_res += exp(i * (j - 10));
				Assert::AreEqual(expected_res, B[i]); 
			}

			free(A);
			free(B);
			destory_sealed_divided_matrix(sealed_divided_A);

			end();
		}

		TEST_METHOD(OutputLayer) {
			uint32_t M = 3;
			uint32_t N = 3;

			uint32_t m = 2;
			uint32_t n = 2;

			init();

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			size_t sz_preY = M * N * sizeof(float);
			size_t sz_trueY = M * sizeof(float);
			size_t sz_diff_preY = M * N * sizeof(float);

			float *preY = (float*)malloc(sz_preY);
			float *trueY = (float*)malloc(sz_trueY);
			float *diff_preY = (float*)malloc(sz_diff_preY);

			preY[0 * N + 0] = 5;
			preY[0 * N + 1] = 9;
			preY[0 * N + 2] = 8;
			preY[1 * N + 0] = 2;
			preY[1 * N + 1] = 7;
			preY[1 * N + 2] = 1;
			preY[2 * N + 0] = -2;
			preY[2 * N + 1] = -3;
			preY[2 * N + 2] = -4;
			sealed_divided_matrix_t* sealed_preY = create_empty_sealed_divided_matrix(eid, M, N, m, n);
			ret_proc = load_sealed_divided_matrix(eid, sealed_preY, preY, sz_preY);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			sealed_divided_matrix_t* sealed_diff_preY = create_empty_sealed_divided_matrix(eid, M, N, m, n);

			trueY[0] = 1;
			trueY[1] = 0;
			trueY[2] = 2;
			// trueY is a column
			sealed_divided_matrix_t* sealed_trueY = create_empty_sealed_divided_matrix(eid, 1, M, 1, m);
			ret_proc = load_sealed_divided_matrix(eid, sealed_trueY, trueY, sz_trueY);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			float loss = 0.0f;
			enclave_output_layer(
				eid, &ret_proc,
				sealed_trueY, sealed_trueY->sz_itself,
				sealed_preY, sealed_preY->sz_itself,
				sealed_diff_preY, sealed_diff_preY->sz_itself,
				&loss);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			Assert::AreEqual(2.581114373f, loss, 1e-2f); 

			ret_proc = unseal_divided_matrix(eid, sealed_diff_preY, diff_preY, sz_diff_preY);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			Assert::AreEqual(0.013212886f / M, diff_preY[0 * N + 0], 1e-3f); 
			Assert::AreEqual(-0.2786008f / M, diff_preY[0 * N + 1], 1e-3f); 
			Assert::AreEqual(0.265387928f / M, diff_preY[0 * N + 2], 1e-3f); 
			Assert::AreEqual(-0.993323587f / M, diff_preY[1 * N + 0], 1e-3f); 
			Assert::AreEqual(0.990867472f / M, diff_preY[1 * N + 1], 1e-3f); 
			Assert::AreEqual(2.456114904e-3f / M, diff_preY[1 * N + 2], 1e-3f); 
			Assert::AreEqual(0.665240955f / M, diff_preY[2 * N + 0], 1e-3f); 
			Assert::AreEqual(0.244728f / M, diff_preY[2 * N + 1], 1e-3f); 
			Assert::AreEqual(-0.909969426f / M, diff_preY[2 * N + 2], 1e-3f); 

			float acc = 0.0f;
			enclave_output_layer_acc(
				eid, &ret_proc,
				sealed_trueY, sealed_trueY->sz_itself,
				sealed_preY, sealed_preY->sz_itself,
				&acc);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			Assert::AreEqual(1.0f / 3.0f, acc, 1e-3f); 

			free(trueY);
			free(preY);
			free(diff_preY);
			destory_sealed_divided_matrix(sealed_trueY);
			destory_sealed_divided_matrix(sealed_preY);
			destory_sealed_divided_matrix(sealed_diff_preY);

			end();
		}

		TEST_METHOD(MP_Xor_GradientCheck_Output) {
			init();

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 5;
			uint32_t n_hidden_units = 4;
			uint32_t n_output_classes = 3;

			size_t sz_input = n_batches * n_hidden_units * sizeof(float);
			size_t sz_fc2 = n_hidden_units * n_output_classes * sizeof(float);
			size_t sz_preY = n_batches * n_output_classes * sizeof(float);
			size_t sz_diff_preY = n_batches * n_output_classes * sizeof(float);
			size_t sz_trueY = n_batches * sizeof(float);

			float* input = (float*)malloc(sz_input);
			float* fc2 = (float*)malloc(sz_fc2);
			float* preY = (float*)malloc(sz_preY);
			float* diff_preY = (float*)malloc(sz_diff_preY);
			float* trueY = (float*)malloc(sz_trueY);

			sealed_divided_matrix_t* sealed_input = create_empty_sealed_divided_matrix(
				eid, n_batches, n_hidden_units, n_batches, n_hidden_units);
			sealed_divided_matrix_t* sealed_fc2 = create_empty_sealed_divided_matrix(
				eid, n_hidden_units, n_output_classes, n_hidden_units, n_output_classes);
			sealed_divided_matrix_t* sealed_preY = create_empty_sealed_divided_matrix(
				eid, n_batches, n_output_classes, n_batches, n_output_classes);
			sealed_divided_matrix_t* sealed_diff_preY = create_empty_sealed_divided_matrix(
				eid, n_batches, n_output_classes, n_batches, n_output_classes);
			sealed_divided_matrix_t* sealed_trueY = create_empty_sealed_divided_matrix(
				eid, 1, n_batches, 1, n_batches);


			FOR(i, n_batches) FOR(j, n_hidden_units) input[i * n_hidden_units + j] = 0.03 * i - 0.9 * j;
			FOR(i, n_batches) trueY[i] = i % n_output_classes;

			// Randomization
			// std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> e{};
			// std::normal_distribution<float> dist_fc2{ 0.0f, sqrtf(2.0f) / (float) sqrt(n_hidden_units + n_output_classes) };
			//std::default_random_engine generator;
			//std::normal_distribution<float> dist_fc2(5.0, 2.0);

			// FOR(i, n_hidden_units) FOR(j, n_output_classes) fc2[i * n_output_classes + j] = (float) dist_fc2(e);

			FOR(a, n_batches) FOR(b, n_output_classes) {

				FOR(i, n_batches) FOR(j, n_output_classes) preY[i * n_output_classes + j] = 0.6 * i - 0.9 * j;

				ret_proc = load_sealed_divided_matrix(eid, sealed_input, input, sz_input);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_fc2, fc2, sz_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_preY, preY, sz_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_diff_preY, diff_preY, sz_diff_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_trueY, trueY, sz_trueY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float loss = 0.0f;

				// Compute the analytic differentiation
				//sealed_gemm_with_functions(
				//	eid, &ret_proc,
				//	sealed_input,
				//	sealed_fc2,
				//	sealed_preY,
				//	0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				//Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				ret_proc = unseal_divided_matrix(eid, sealed_diff_preY, diff_preY, sz_diff_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float back_diff = diff_preY[a * n_output_classes + b];

				// Compute the finite differentiation
				float eps = 1e-4;
				float plus_loss, minus_loss;

				ret_proc = unseal_divided_matrix(eid, sealed_preY, preY, sz_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				preY[a * n_output_classes + b] += eps;
				// ret_proc = load_sealed_divided_matrix(eid, sealed_fc2, fc2, sz_fc2);
				ret_proc = load_sealed_divided_matrix(eid, sealed_preY, preY, sz_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				//sealed_gemm_with_functions(
				//	eid, &ret_proc,
				//	sealed_input,
				//	sealed_fc2,
				//	sealed_preY,
				//	0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				//Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&plus_loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// fc2[a * n_output_classes + b] -= 2.0f * eps;
				preY[a * n_output_classes + b] -= 2.0f * eps;
				ret_proc = load_sealed_divided_matrix(eid, sealed_preY, preY, sz_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				//sealed_gemm_with_functions(
				//	eid, &ret_proc,
				//	sealed_input,
				//	sealed_fc2,
				//	sealed_preY,
				//	0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				//Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&minus_loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float finite_diff = (plus_loss - minus_loss) / (2.0f * eps);
				Assert::AreEqual(finite_diff, back_diff, abs(finite_diff + back_diff) * 1e-2f);
			}

			free(input);
			free(preY);
			free(trueY);
			free(diff_preY);
			free(fc2);
			free(sealed_preY);
			free(sealed_trueY);
			free(sealed_diff_preY);
			free(sealed_fc2);

			end();
		}

		TEST_METHOD(MP_Xor_GradientCheck_FC2) {
			init();

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 5;
			uint32_t n_hidden_units = 6;
			uint32_t n_output_classes = 3;

			size_t sz_input = n_batches * n_hidden_units * sizeof(float);
			size_t sz_fc2 = n_hidden_units * n_output_classes * sizeof(float);
			size_t sz_diff_fc2 = n_hidden_units * n_output_classes * sizeof(float);
			size_t sz_preY = n_batches * n_output_classes * sizeof(float);
			size_t sz_diff_preY = n_batches * n_output_classes * sizeof(float);
			size_t sz_trueY = n_batches * sizeof(float);

			float* input = (float*)malloc(sz_input);
			float* fc2 = (float*)malloc(sz_fc2);
			float* diff_fc2 = (float*)malloc(sz_diff_fc2);
			float* preY = (float*)malloc(sz_preY);
			float* diff_preY = (float*)malloc(sz_diff_preY);
			float* trueY = (float*)malloc(sz_trueY);

			sealed_divided_matrix_t* sealed_input = create_empty_sealed_divided_matrix(
				eid, n_batches, n_hidden_units, n_batches, n_hidden_units);
			sealed_divided_matrix_t* sealed_fc2 = create_empty_sealed_divided_matrix(
				eid, n_hidden_units, n_output_classes, n_hidden_units, n_output_classes);
			sealed_divided_matrix_t* sealed_diff_fc2 = create_empty_sealed_divided_matrix(
				eid, n_hidden_units, n_output_classes, n_hidden_units, n_output_classes);
			sealed_divided_matrix_t* sealed_preY = create_empty_sealed_divided_matrix(
				eid, n_batches, n_output_classes, n_batches, n_output_classes);
			sealed_divided_matrix_t* sealed_diff_preY = create_empty_sealed_divided_matrix(
				eid, n_batches, n_output_classes, n_batches, n_output_classes);
			sealed_divided_matrix_t* sealed_trueY = create_empty_sealed_divided_matrix(
				eid, 1, n_batches, 1, n_batches);


			FOR(i, n_batches) FOR(j, n_hidden_units) input[i * n_hidden_units + j] = 0.03 * i - 0.9 * j;
			FOR(i, n_hidden_units) FOR(j, n_output_classes) fc2[i * n_output_classes + j] = 0.62 * i - 0.32 * j + 0.21;
			FOR(i, n_batches) trueY[i] = i % n_output_classes;

			// Randomization
			//std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> e{};
			//std::normal_distribution<float> dist_fc2{ 0.0f, sqrtf(2.0f) / (float) sqrt(n_hidden_units + n_output_classes) };
			//FOR(i, n_hidden_units) FOR(j, n_output_classes) fc2[i * n_output_classes + j] = (float) dist_fc2(e);

			FOR(a, n_hidden_units) FOR(b, n_output_classes) {

				FOR(i, n_batches) FOR(j, n_output_classes) preY[i * n_output_classes + j] = 0.6 * i - 0.9 * j;

				ret_proc = load_sealed_divided_matrix(eid, sealed_input, input, sz_input);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_fc2, fc2, sz_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_diff_fc2, diff_fc2, sz_diff_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_preY, preY, sz_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_diff_preY, diff_preY, sz_diff_preY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				ret_proc = load_sealed_divided_matrix(eid, sealed_trueY, trueY, sz_trueY);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float loss = 0.0f;

				// Compute the analytic differentiation
				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_input,
					sealed_fc2,
					sealed_preY,
					0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_input,
					sealed_diff_preY,
					sealed_diff_fc2,
					1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				ret_proc = unseal_divided_matrix(eid, sealed_diff_fc2, diff_fc2, sz_diff_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float back_diff = diff_fc2[a * n_output_classes + b];

				// Compute the finite differentiation
				float eps = 1e-2;
				float plus_loss, minus_loss;

				fc2[a * n_output_classes + b] += eps;
				ret_proc = load_sealed_divided_matrix(eid, sealed_fc2, fc2, sz_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_input,
					sealed_fc2,
					sealed_preY,
					0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&plus_loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				fc2[a * n_output_classes + b] -= 2.0f * eps;
				ret_proc = load_sealed_divided_matrix(eid, sealed_fc2, fc2, sz_fc2);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_input,
					sealed_fc2,
					sealed_preY,
					0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				enclave_output_layer(
					eid, &ret_proc,
					sealed_trueY, sealed_trueY->sz_itself,
					sealed_preY, sealed_preY->sz_itself,
					sealed_diff_preY, sealed_diff_preY->sz_itself,
					&minus_loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float finite_diff = (plus_loss - minus_loss) / (2.0f * eps);
				Assert::AreEqual(finite_diff, back_diff, abs(finite_diff + back_diff) * 1e-2f);
			}

			free(input);
			free(preY);
			free(trueY);
			free(diff_preY);
			free(fc2);
			free(diff_fc2);
			free(sealed_preY);
			free(sealed_trueY);
			free(sealed_diff_preY);
			free(sealed_fc2);
			free(sealed_diff_fc2);

			end();
		}

		TEST_METHOD(MP_Xor_GradientCheck_FC1) {
			init();

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 5;
			uint32_t n_features = 4;
			uint32_t n_hidden_units = 6;
			uint32_t n_output_classes = 3;

#define GEN_PARA_SEAL_PLAIN(x_, row_, col_) \
	size_t sz_##x_ = (row_) * (col_) * sizeof(float); \
	float* (x_) = (float*)malloc(sz_##x_); \
	sealed_divided_matrix_t* sealed_##x_ = create_empty_sealed_divided_matrix( eid, (row_), (col_), (row_), (col_));

			GEN_PARA_SEAL_PLAIN(input, n_batches, n_features);
			GEN_PARA_SEAL_PLAIN(fc1, n_features, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(diff_fc1, n_features, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(b1, n_batches, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(activation_input, n_batches, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(diff_activation_input, n_batches, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(middle_hidden, n_batches, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(diff_middle_hidden, n_batches, n_hidden_units);
			GEN_PARA_SEAL_PLAIN(fc2, n_hidden_units, n_output_classes);
			GEN_PARA_SEAL_PLAIN(b2, n_batches, n_output_classes);
			GEN_PARA_SEAL_PLAIN(diff_fc2, n_hidden_units, n_output_classes);
			GEN_PARA_SEAL_PLAIN(preY, n_batches, n_output_classes);
			GEN_PARA_SEAL_PLAIN(diff_preY, n_batches, n_output_classes);
			GEN_PARA_SEAL_PLAIN(trueY, 1, n_batches);

			FOR(i, n_batches) FOR(j, n_features) input[i * n_features + j] = 0.3 * i - 0.4 * j + 0.3;
			FOR(i, n_features) FOR(j, n_hidden_units) fc1[i * n_hidden_units + j] = - 0.523 * i + 0.22 * j + 0.23;
			FOR(i, n_batches) FOR(j, n_hidden_units) activation_input[i * n_hidden_units + j] = - 0.623 * i + 0.32 * j + 0.13;
			FOR(i, n_batches) FOR(j, n_hidden_units) middle_hidden[i * n_hidden_units + j] = -0.323 * i + 0.212 * j + 0.23;
			FOR(i, n_hidden_units) FOR(j, n_output_classes) fc2[i * n_output_classes + j] = 0.62 * i - 0.32 * j + 0.31;
			FOR(i, n_batches) FOR(j, n_output_classes) preY[i * n_output_classes + j] = 0.3 * i - 0.9 * j;
			FOR(i, n_batches) trueY[i] = i % n_output_classes;

#define LOAD_TO_SEAL(y_) \
	ret_proc = load_sealed_divided_matrix(eid, sealed_##y_, y_, sz_##y_); \
	Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			LOAD_TO_SEAL(input);
			LOAD_TO_SEAL(fc1);
			LOAD_TO_SEAL(activation_input);
			LOAD_TO_SEAL(middle_hidden);
			LOAD_TO_SEAL(fc2);
			LOAD_TO_SEAL(preY);
			LOAD_TO_SEAL(trueY);

			// Loopping to test all position
			FOR(a, n_features) FOR(b, n_hidden_units) {

				// Compute the analytic differentiation
				// forwarding
				auto forwarding = [&](float* loss) {
					sealed_gemm_with_functions(
						eid, &ret_proc,
						sealed_input,
						sealed_fc1,
						sealed_activation_input,
						0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
					Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
					enclave_sealed_divided_elem_bopa(
						eid, &ret_proc,
						sealed_activation_input, sealed_activation_input->sz_itself,
						sealed_middle_hidden, sealed_middle_hidden->sz_itself,
						SIGMOID);
					Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
					sealed_gemm_with_functions(
						eid, &ret_proc,
						sealed_middle_hidden,
						sealed_fc2,
						sealed_preY,
						0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
					Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
					enclave_output_layer(
						eid, &ret_proc,
						sealed_trueY, sealed_trueY->sz_itself,
						sealed_preY, sealed_preY->sz_itself,
						sealed_diff_preY, sealed_diff_preY->sz_itself,
						loss);
					Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				};

				float analysis_loss = 0.0f;
				forwarding(&analysis_loss);

				// backward
				// the diff of weighting of fc2
				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_middle_hidden,
					sealed_diff_preY,
					sealed_diff_fc2,
					1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				// the diff of the middle hidden
				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_diff_preY,
					sealed_fc2,
					sealed_diff_middle_hidden,
					0, 1, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				// the diff of the activation layer
				enclave_sealed_divided_elem_copab_alpha_beta(
					eid, &ret_proc,
					sealed_activation_input, sealed_activation_input->sz_itself,
					sealed_diff_middle_hidden, sealed_diff_middle_hidden->sz_itself,
					sealed_diff_activation_input, sealed_diff_activation_input->sz_itself,
					0.0f, 0.0f,
					SIGMOID_DIFFA_B);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
				// the diff of weighting of fc1
				sealed_gemm_with_functions(
					eid, &ret_proc,
					sealed_input,
					sealed_diff_activation_input,
					sealed_diff_fc1,
					1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				float eps = 1e-1f;
				float plus_loss, minus_loss;
				float back_diff;

#define FINITE_DIFF(x_, y_, col_) \
				ret_proc = unseal_divided_matrix(eid, sealed_##y_, y_, sz_##y_); \
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS); \
				back_diff = (y_)[a * (col_) + b]; \
				(x_)[a * (col_) + b] += eps; \
				LOAD_TO_SEAL(x_); \
				forwarding(&plus_loss); \
				(x_)[a * (col_) + b] -= 2.0f * eps; \
				LOAD_TO_SEAL(x_); \
				forwarding(&minus_loss);

				FINITE_DIFF(fc1, diff_fc1, n_hidden_units);
				// FINITE_DIFF(activation_input, diff_activation_input, n_hidden_units);
				float finite_diff = (plus_loss - minus_loss) / (2.0f * eps);
				float tor = abs(finite_diff + back_diff) * 1e-2f;
				Assert::AreEqual(finite_diff, back_diff, tor);
			}

			free(input);
			free(fc1);
			free(diff_fc1);
			free(b1);
			free(activation_input);
			free(diff_activation_input);
			free(middle_hidden);
			free(diff_middle_hidden);
			free(fc2);
			free(b2);
			free(diff_fc2);
			free(preY);
			free(diff_preY);
			free(trueY);

			end();
		}

		TEST_METHOD(MP_Xor_Train) {
			init();

#define GEN_PARA_SEAL_PLAIN(x_, row_, col_) \
	size_t sz_##x_ = (row_) * (col_) * sizeof(float); \
	float* (x_) = (float*)malloc(sz_##x_); \
	sealed_divided_matrix_t* sealed_##x_ = create_empty_sealed_divided_matrix( eid, (row_), (col_), (row_), (col_));

#define LOAD_TO_SEAL(y_) \
	ret_proc = load_sealed_divided_matrix(eid, sealed_##y_, y_, sz_##y_); \
	Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

#define GEN_SEAL_MAT(x_, row_, col_) \
	size_t sz_##x_ = (row_) * (col_) * sizeof(float); \
	float* (x_) = (float*)malloc(sz_##x_); \
	sealed_divided_matrix_t* sealed_##x_ = create_empty_sealed_divided_matrix( eid, (row_), (col_), 32, 32);

#define CHECK_SUCCESS(x_) \
			Assert::IsTrue(x_ == ENCLAVE_SUCCESS);

// Y_ += alpha_ * X_
#define UPDATE_ADD(X_, Y_, alpha_) \
	enclave_sealed_divided_geadd( \
		eid, &ret_proc, \
		X_, X_->sz_itself, \
		Y_, Y_->sz_itself, \
		alpha_, 1.0f, \
		ADD); \
		Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 2;
			uint32_t n_features = 2;
			uint32_t n_hidden_units = 4;
			uint32_t n_output_classes = 2;

			matrix_layer_t* input = create_matrix_layer( eid, n_batches, n_features);
			matrix_layer_t* fc1 = create_matrix_layer( eid, n_features, n_hidden_units);
			matrix_layer_t* b1 = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* activation_input = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* middle_hidden = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* fc2 = create_matrix_layer( eid, n_hidden_units, n_output_classes);
			matrix_layer_t* b2 = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* preY = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* trueY = create_matrix_layer( eid, 1, n_batches);

			// Randomize and set the matrices

			fc_layer_t* fc1_layer = create_fc_layer(input, fc1, b1, activation_input, middle_hidden);
			fc_layer_t* fc2_layer = create_fc_layer(middle_hidden, fc2, b2, preY, trueY);

			// Randomize and set the matrices
			matrix_randomize_seal(eid, fc1);
			matrix_randomize_seal(eid, b1);
			matrix_randomize_seal(eid, fc2);
			matrix_randomize_seal(eid, b2);

			float loss = 0.0f;
			float acc = 0.0f;
			float learning_rate = 1.0f;
			float update_rate = learning_rate;

			uint32_t n_iter = 2000;

			FOR(idx_iter, n_iter) {

				// 0 x 0 -> 0
				// 0 x 1 -> 1
				// 1 x 0 -> 1
				// 1 x 1 -> 0

				if (idx_iter % 2) {
					input->plain[0] = 0; input->plain[1] = 0;
					input->plain[2] = 0; input->plain[3] = 1;
					trueY->plain[0] = 0;
					trueY->plain[1] = 1;
				} else {
					input->plain[0] = 1; input->plain[1] = 0;
					input->plain[2] = 1; input->plain[3] = 1;
					trueY->plain[0] = 1;
					trueY->plain[1] = 0;
				}
				CHECK_SUCCESS(layer_load_sealed(eid, input));
				CHECK_SUCCESS(layer_load_sealed(eid, trueY));

				// forwarding
				CHECK_SUCCESS(fc_forward(eid, fc1_layer, true));
				CHECK_SUCCESS(fc_forward(eid, fc2_layer, false));

				enclave_output_layer(
					eid, &ret_proc,
					trueY->sealed, trueY->sealed->sz_itself,
					preY->sealed, preY->sealed->sz_itself,
					preY->sealed_diff, preY->sealed_diff->sz_itself,
					&loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// backward
				CHECK_SUCCESS(fc_backward(eid, fc2_layer, false, true));
				CHECK_SUCCESS(fc_backward(eid, fc1_layer, true, false));

				// Calculate the loss and accuracy
				enclave_output_layer_acc(
					eid, &ret_proc,
					trueY->sealed, trueY->sealed->sz_itself,
					preY->sealed, preY->sealed->sz_itself,
					&acc);
				printf("turn: %5d loss: %.3f acc: %.3f\n", idx_iter, loss, acc);
				// Update the matrices
				
				CHECK_SUCCESS(fc_update(eid, fc2_layer, update_rate));
				CHECK_SUCCESS(fc_update(eid, fc1_layer, update_rate));

			}
			Assert::AreEqual(1.0f, acc);

			destroy_matrix_layer(input);
			destroy_matrix_layer(fc1);
			destroy_matrix_layer(b1);
			destroy_matrix_layer(activation_input);
			destroy_matrix_layer(middle_hidden);
			destroy_matrix_layer(fc2);
			destroy_matrix_layer(b2);
			destroy_matrix_layer(preY);
			destroy_matrix_layer(trueY);

			end();
		}

		TEST_METHOD(MP_MNIST) {
			init();

			// Load all the training images and labels
			float* f_image_mnist = NULL;
			float* f_label_mnist = NULL;
			int n_images, n_rows, n_cols;
			read_mnist_image_label(
				"..\\data_mnist\\train-images-idx3-ubyte",
				"..\\data_mnist\\train-labels-idx1-ubyte",
				&f_image_mnist,
				&f_label_mnist,
				&n_images,
				&n_rows,
				&n_cols);

			FOR(i, n_images * n_rows * n_cols) f_image_mnist[i] = f_image_mnist[i] / 128.0f - 1.0f;

			// Parameter

			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 32;
			uint32_t n_features = n_rows * n_cols;
			uint32_t n_hidden_units = 128;
			uint32_t n_output_classes = 10;

			matrix_layer_t* input = create_matrix_layer( eid, n_batches, n_features);
			matrix_layer_t* fc1 = create_matrix_layer( eid, n_features, n_hidden_units);
			matrix_layer_t* b1 = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* activation_input = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* middle_hidden = create_matrix_layer( eid, n_batches, n_hidden_units);
			matrix_layer_t* fc2 = create_matrix_layer( eid, n_hidden_units, n_output_classes);
			matrix_layer_t* b2 = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* preY = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* trueY = create_matrix_layer( eid, 1, n_batches);
			// GEN_PARA_SEAL_PLAIN(trueY, 1, n_batches);

			fc_layer_t* fc1_layer = create_fc_layer(input, fc1, b1, activation_input, middle_hidden);
			fc_layer_t* fc2_layer = create_fc_layer(middle_hidden, fc2, b2, preY, trueY);

			// Randomize and set the matrices
			matrix_randomize_seal(eid, fc1);
			matrix_randomize_seal(eid, b1);
			matrix_randomize_seal(eid, fc2);
			matrix_randomize_seal(eid, b2);

			matrix_clean(eid, activation_input);
			matrix_clean(eid, middle_hidden);
			matrix_clean(eid, preY);

			float loss = 0.0f;
			float acc = 0.0f;
			float learning_rate = 5e-1f;
			float update_rate = learning_rate;
#ifdef _DEBUG
			uint32_t n_iter = 10;
#else
			uint32_t n_iter = 200;
#endif // _DEBUG

			FOR(idx_iter, n_iter) {

				uint32_t start_batch = (idx_iter * n_batches) % (n_images - n_batches);
				// uint32_t start_batch = 0;

				memcpy(input->plain, &f_image_mnist[start_batch * n_features], n_batches * n_features * sizeof(float));
				memcpy(trueY->plain, &f_label_mnist[start_batch], n_batches * sizeof(float));

				FOR(i, n_batches * n_features) 
					if (input->plain[i] < -1.0f || input->plain[i] > 1.0f) printf("%2f\n", input->plain[i]);

				CHECK_SUCCESS(layer_load_sealed(eid, input));
				CHECK_SUCCESS(layer_load_sealed(eid, trueY));

				// forwarding
				CHECK_SUCCESS(fc_forward(eid, fc1_layer, true));
				CHECK_SUCCESS(fc_forward(eid, fc2_layer, false));

				enclave_output_layer(
					eid, &ret_proc,
					trueY->sealed, trueY->sealed->sz_itself,
					preY->sealed, preY->sealed->sz_itself,
					preY->sealed_diff, preY->sealed_diff->sz_itself,
					&loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// backward
				CHECK_SUCCESS(fc_backward(eid, fc2_layer, false, true));
				CHECK_SUCCESS(fc_backward(eid, fc1_layer, true, false));
				
				// Calculate the loss and accuracy
				enclave_output_layer_acc(
					eid, &ret_proc,
					trueY->sealed, trueY->sealed->sz_itself,
					preY->sealed, preY->sealed->sz_itself,
					&acc);
				printf("turn: %5d loss: %.3f acc: %.3f\n", idx_iter, loss, acc);

				// Update the matrices

				CHECK_SUCCESS(fc_update(eid, fc2_layer, -update_rate));
				CHECK_SUCCESS(fc_update(eid, fc1_layer, -update_rate));
			}

			free(f_image_mnist);
			free(f_label_mnist);

			destroy_matrix_layer(input);
			destroy_matrix_layer(fc1);
			destroy_matrix_layer(b1);
			destroy_matrix_layer(activation_input);
			destroy_matrix_layer(middle_hidden);
			destroy_matrix_layer(fc2);
			destroy_matrix_layer(b2);
			destroy_matrix_layer(preY);
			destroy_matrix_layer(trueY);

			end();
		}

		TEST_METHOD(TensorConvolution) {
			uint32_t n_batches = 2;
			uint32_t n_input_channels = 3;
			uint32_t n_output_channels = 5;
			uint32_t n_input_rows = 28;
			uint32_t n_input_cols = 28;
			uint32_t n_kernal_rows = 5;
			uint32_t n_kernal_cols = 5;
			uint32_t n_output_rows;
			uint32_t n_output_cols;
			uint32_t padding_y = 1;
			uint32_t padding_x = 1;
			uint32_t stride_y = 2;
			uint32_t stride_x = 2;
			uint32_t pooling_field_y = 3;
			uint32_t pooling_field_x = 3;
			uint32_t pooling_stride_y = 1;
			uint32_t pooling_stride_x = 1;
			uint32_t pooling_padding_y = 1;
			uint32_t pooling_padding_x = 1;
			uint32_t pooling_output_y;
			uint32_t pooling_output_x;

			get_output_tensor_shape(
				n_input_channels, n_output_channels,
				n_input_rows, n_input_cols,
				n_kernal_rows, n_kernal_cols,
				stride_y, stride_x,
				padding_y, padding_x,
				&n_output_rows, &n_output_cols);

			get_output_tensor_shape(
				n_input_channels, n_output_channels,
				n_output_rows, n_output_cols,
				pooling_field_y, pooling_field_x,
				pooling_stride_y, pooling_stride_x,
				pooling_padding_y, pooling_padding_x,
				&pooling_output_y, &pooling_output_x);

			init();

			sealed_tensor_t* sealed_input = create_empty_sealed_tensor(
				eid, 
				n_batches, n_input_channels, 
				n_input_rows, n_input_cols);
			sealed_tensor_t* sealed_kernal = create_empty_sealed_tensor(
				eid, 
				n_output_channels, n_input_channels, 
				n_kernal_rows, n_kernal_cols);
			sealed_tensor_t* sealed_output = create_empty_sealed_tensor(
				eid, 
				n_batches, n_output_channels, 
				n_output_rows, n_output_cols);
			sealed_tensor_t* sealed_bias = create_empty_sealed_tensor(
				eid, 
				n_batches, n_output_channels, 
				n_output_rows, n_output_cols);
			sealed_tensor_t* sealed_pooled = create_empty_sealed_tensor(
				eid, 
				n_batches, n_output_channels, 
				pooling_output_y, pooling_output_x);
			sealed_tensor_t* sealed_aux = create_empty_sealed_tensor(
				eid, 
				n_batches, n_output_channels, 
				pooling_output_y, pooling_output_x);
			sealed_divided_matrix_t* sealed_flatten = create_empty_sealed_divided_matrix(
				eid, n_batches, n_output_channels * n_output_rows * n_output_cols, 13, 200);

#define ALLOCATE_PLAIN(name_) \
			size_t sz_plain_##name_ = get_tensor_n_elem(sealed_##name_) * sizeof(float); \
			float* plain_##name_ = (float*)malloc(sz_plain_##name_);

			ALLOCATE_PLAIN(input);
			ALLOCATE_PLAIN(kernal);
			ALLOCATE_PLAIN(output);
			ALLOCATE_PLAIN(bias);
			ALLOCATE_PLAIN(pooled);

			size_t sz_plain_flatten = n_batches * n_output_channels * n_output_rows * n_output_cols * sizeof(float); 
			float* (plain_flatten) = (float*)malloc(sz_plain_flatten);

			float elem_a = 0.002f;
			float elem_b = 0.003f;
			float elem_c = 0.03f;

			FOR(i, get_tensor_n_elem(sealed_input)) plain_input[i] = elem_a;
			FOR(i, get_tensor_n_elem(sealed_kernal)) plain_kernal[i] = elem_b;
			FOR(i, get_tensor_n_elem(sealed_bias)) plain_bias[i] = elem_c;

			uint32_t ret_proc = ENCLAVE_SUCCESS;

#define LOAD_TENSOR(name_) \
			ret_proc = load_sealed_tensor(eid, sealed_##name_, plain_##name_, sz_plain_##name_); \
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			LOAD_TENSOR(input);
			LOAD_TENSOR(kernal);
			LOAD_TENSOR(output);
			LOAD_TENSOR(bias);
			LOAD_TENSOR(pooled);

			ret_proc = load_sealed_divided_matrix(eid, sealed_flatten, plain_flatten, sz_plain_flatten);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_convolute_tensor_kernal(
				eid, &ret_proc,
				sealed_input, sealed_input->sz_itself,
				sealed_kernal, sealed_kernal->sz_itself,
				sealed_output, sealed_output->sz_itself,
				padding_y, padding_x,
				stride_y, stride_x,
				1.0f, 0.0f,
				1, 1);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_tensor_elem_copab_alpha_beta(
				eid, &ret_proc,
				sealed_output, sealed_output->sz_itself,
				sealed_bias, sealed_bias->sz_itself,
				sealed_output, sealed_output->sz_itself,
				1.0f, 1.0f,
				ADD);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_sealed_tensor_elem_bopa(
				eid, &ret_proc,
				sealed_output, sealed_output->sz_itself,
				sealed_output, sealed_output->sz_itself,
				SIGMOID);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_tensor(eid, sealed_input, plain_input, sz_plain_input);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = unseal_tensor(eid, sealed_kernal, plain_kernal, sz_plain_kernal);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			ret_proc = unseal_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			auto sigmoid = [](float x) { return 1.0f / (1.0f + expf(-x)); };

			FOR (i, n_batches) FOR (j, n_output_channels){
				Assert::AreEqual(sigmoid(16 * n_input_channels * elem_a * elem_b + elem_c), plain_output[get_tensor_index(sealed_output, i, j , 0, 0)], 1e-3f);
				for (uint32_t y = 1; y < n_output_rows; y++) Assert::AreEqual(sigmoid(20 * n_input_channels * elem_a * elem_b + elem_c), plain_output[get_tensor_index(sealed_output, i, j , y, 0)], 1e-3f);
				for (uint32_t x = 1; x < n_output_cols; x++) Assert::AreEqual(sigmoid(20 * n_input_channels * elem_a * elem_b + elem_c), plain_output[get_tensor_index(sealed_output, i, j , 0, x)], 1e-3f);
				for (uint32_t y = 1; y < n_output_rows; y++) for (uint32_t x = 1; x < n_output_cols; x++) {
					Assert::AreEqual(sigmoid(25 * n_input_channels * elem_a * elem_b + elem_c), plain_output[get_tensor_index(sealed_output, i, j , y, x)], 1e-3f);
				}
			}

			FOR(i, get_tensor_n_elem(sealed_output)) plain_output[i] = 9.0f;
			ret_proc = load_sealed_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_tensor_pooling(
				eid, &ret_proc,
				sealed_output, sealed_output->sz_itself,
				sealed_pooled, sealed_pooled->sz_itself,
				NULL, 0,
				pooling_field_y, pooling_field_x,
				pooling_padding_y, pooling_padding_x,
				pooling_stride_y, pooling_stride_x,
				AVERAGE_POOLING);

			ret_proc = unseal_tensor(eid, sealed_pooled, plain_pooled, sz_plain_pooled);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			FOR (i, n_batches) FOR (j, n_output_channels){
				Assert::AreEqual(4.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, 0, 0)]);
				Assert::AreEqual(4.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, pooling_output_y - 1, pooling_output_x - 1)]);
				Assert::AreEqual(4.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, pooling_output_y - 1, 0)]);
				Assert::AreEqual(4.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, 0, pooling_output_x - 1)]);
				for (uint32_t y = 1; y < n_output_rows - 1; y++) {
					Assert::AreEqual(6.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, y, 0)]);
					Assert::AreEqual(6.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, y, pooling_output_x - 1)]);
				}
				for (uint32_t x = 1; x < n_output_cols - 1; x++) {
					Assert::AreEqual(6.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, 0, x)]);
					Assert::AreEqual(6.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, pooling_output_y - 1, x)]);
				}
				for (uint32_t y = 1; y < n_output_rows - 1; y++) {
					for (uint32_t x = 1; x < n_output_cols - 1; x++) {
						Assert::AreEqual(9.0f, plain_pooled[get_tensor_index(sealed_pooled, i, j, y, x)]);
					}
				}
			}

			FOR(i, get_tensor_n_elem(sealed_output)) plain_output[i] = 0.0f;
			ret_proc = load_sealed_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_tensor_pooling(
				eid, &ret_proc,
				sealed_pooled, sealed_pooled->sz_itself,
				sealed_output, sealed_output->sz_itself,
				NULL, 0,
				pooling_field_y, pooling_field_x,
				pooling_padding_y, pooling_padding_x,
				pooling_stride_y, pooling_stride_x,
				BACKWARD_AVERAGE_POOLING);

			ret_proc = unseal_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, n_batches) FOR(j, n_output_channels) {
				for (uint32_t y = 2, x = 2; y < n_output_rows - 2; y++, x++) {
					Assert::AreEqual(9.0f, plain_output[get_tensor_index(sealed_pooled, i, j, y, x)]);
				}
			}

			// MAX POOLING
			FOR(i, get_tensor_n_elem(sealed_output)) plain_output[i] = (float) i;
			CHECK_SUCCESS(load_sealed_tensor(eid, sealed_output, plain_output, sz_plain_output));

			enclave_tensor_pooling(
				eid, &ret_proc,
				sealed_output, sealed_output->sz_itself,
				sealed_pooled, sealed_pooled->sz_itself,
				sealed_aux, sealed_aux->sz_itself,
				pooling_field_y, pooling_field_x,
				pooling_padding_y, pooling_padding_x,
				pooling_stride_y, pooling_stride_x,
				MAX_POOLING);

			CHECK_SUCCESS(unseal_tensor(eid, sealed_pooled, plain_pooled, sz_plain_pooled));

			FOR(i, get_tensor_n_elem(sealed_output)) plain_output[i] = (float) i;
			ret_proc = load_sealed_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_tensor_flatten(
				eid, &ret_proc,
				sealed_output, sealed_output->sz_itself ,
				sealed_flatten, sealed_flatten->sz_itself);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_divided_matrix(eid, sealed_flatten, plain_flatten, sz_plain_flatten);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, get_tensor_n_elem(sealed_output)) Assert::AreEqual((float) i, plain_flatten[i]);

			// Test For backward Flatten
			FOR(i, get_tensor_n_elem(sealed_output)) plain_output[i] = 0.0f;
			ret_proc = load_sealed_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_tensor_backward_flatten(
				eid, &ret_proc,
				sealed_flatten, sealed_flatten->sz_itself,
				sealed_output, sealed_output->sz_itself);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			ret_proc = unseal_tensor(eid, sealed_output, plain_output, sz_plain_output);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			FOR(i, get_tensor_n_elem(sealed_output)) Assert::AreEqual((float) i, plain_output[i]);

			enclave_backward_convolute_tensor_kernal(
				eid, &ret_proc,
				sealed_input, sealed_input->sz_itself,
				sealed_kernal, sealed_kernal->sz_itself,
				sealed_output, sealed_output->sz_itself,
				padding_y, padding_x,
				stride_y, stride_x,
				1.0f, 0.0f);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			enclave_backward_convolute_tensor_input(
				eid, &ret_proc,
				sealed_input, sealed_input->sz_itself,
				sealed_kernal, sealed_kernal->sz_itself,
				sealed_output, sealed_output->sz_itself,
				padding_y, padding_x,
				stride_y, stride_x,
				1.0f, 0.0f);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			destory_sealed_tensor(sealed_input);
			destory_sealed_tensor(sealed_kernal);
			destory_sealed_tensor(sealed_output);
			destory_sealed_tensor(sealed_bias);
			destory_sealed_tensor(sealed_pooled);
			destory_sealed_divided_matrix(sealed_flatten);
			free(plain_input);
			free(plain_kernal);
			free(plain_output);
			free(plain_bias);
			free(plain_flatten);
			free(plain_pooled);

			end();
		}

		TEST_METHOD(TensorCNN_GradientCheck) {
			uint32_t ret_proc = ENCLAVE_SUCCESS;

			uint32_t n_batches = 50;
			uint32_t n_hidden = 50;
			// uint32_t n_hidden = 20;
			uint32_t n_output_classes = 10;

			uint32_t n_channels1 = 1;
			uint32_t n_channels2 = 5;
			uint32_t n_channels3 = 5;

			uint32_t n_image1_rows = 28;
			uint32_t n_image1_cols = 28;
			uint32_t n_kernal1_rows = 5;
			uint32_t n_kernal1_cols = 5;
			uint32_t n_output1_rows;
			uint32_t n_output1_cols;
			uint32_t padding1_y = 1;
			uint32_t padding1_x = 1;
			uint32_t stride1_y = 2;
			uint32_t stride1_x = 2;
			uint32_t pooling1_field_y = 3;
			uint32_t pooling1_field_x = 3;
			uint32_t pooling1_stride_y = 1;
			uint32_t pooling1_stride_x = 1;
			uint32_t pooling1_padding_y = 1;
			uint32_t pooling1_padding_x = 1;
			uint32_t pooling1_output_y;
			uint32_t pooling1_output_x;

			get_output_tensor_shape(
				n_channels1, n_channels2,
				n_image1_rows, n_image1_cols,
				n_kernal1_rows, n_kernal1_cols,
				stride1_y, stride1_x,
				padding1_y, padding1_x,
				&n_output1_rows, &n_output1_cols);

			get_output_tensor_shape(
				n_channels1, n_channels2,
				n_output1_rows, n_output1_cols,
				pooling1_field_y, pooling1_field_x,
				pooling1_stride_y, pooling1_stride_x,
				pooling1_padding_y, pooling1_padding_x,
				&pooling1_output_y, &pooling1_output_x);

			uint32_t n_image2_rows = pooling1_output_y;
			uint32_t n_image2_cols = pooling1_output_x;
			uint32_t n_kernal2_rows = 5;
			uint32_t n_kernal2_cols = 5;
			uint32_t n_output2_rows;
			uint32_t n_output2_cols;
			uint32_t padding2_y = 0;
			uint32_t padding2_x = 0;
			uint32_t stride2_y = 2;
			uint32_t stride2_x = 2;
			uint32_t pooling2_field_y = 3;
			uint32_t pooling2_field_x = 3;
			uint32_t pooling2_stride_y = 1;
			uint32_t pooling2_stride_x = 1;
			uint32_t pooling2_padding_y = 1;
			uint32_t pooling2_padding_x = 1;
			uint32_t pooling2_output_y;
			uint32_t pooling2_output_x;

			get_output_tensor_shape(
				n_channels2, n_channels3,
				n_image2_rows, n_image2_cols,
				n_kernal2_rows, n_kernal2_cols,
				stride2_y, stride2_x,
				padding2_y, padding2_x,
				&n_output2_rows, &n_output2_cols);

			get_output_tensor_shape(
				n_channels2, n_channels3,
				n_output2_rows, n_output2_cols,
				pooling2_field_y, pooling2_field_x,
				pooling2_stride_y, pooling2_stride_x,
				pooling2_padding_y, pooling2_padding_x,
				&pooling2_output_y, &pooling2_output_x);

			uint32_t n_flatten_features = n_channels3 * pooling2_output_y * pooling2_output_x;

			init();

			// The 1st CNN
			tensor_layer_t* image1 = create_tensor_layer( eid, n_batches, n_channels1, n_image1_rows, n_image1_cols);
			// tensor_layer_t* conv1 = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
			tensor_layer_t* conv1_kernal = create_tensor_layer( eid, n_channels2, n_channels1, n_kernal1_rows, n_kernal1_cols);
			tensor_layer_t* conv1_bias = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
			tensor_layer_t* conv1_preact = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
			tensor_layer_t* conv1_acted = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
			tensor_layer_t* conv1_pooled = create_tensor_layer( eid, n_batches, n_channels2, pooling1_output_y, pooling1_output_x);

			// The 2nd CNN
			// tensor_layer_t* conv2 = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
			tensor_layer_t* conv2_kernal = create_tensor_layer( eid, n_channels3, n_channels2, n_kernal2_rows, n_kernal2_cols);
			tensor_layer_t* conv2_bias = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
			tensor_layer_t* conv2_preact = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
			tensor_layer_t* conv2_acted = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
			tensor_layer_t* conv2_pooled = create_tensor_layer( eid, n_batches, n_channels3, pooling2_output_y, pooling2_output_x);

			// For the fully connected layers
			matrix_layer_t* flatten = create_matrix_layer( eid, n_batches, n_flatten_features, 50, 50);
			// matrix_layer_t* fc1 = create_matrix_layer( eid, n_flatten_features, n_hidden, 50, 50);
			// matrix_layer_t* fc1_bias = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
			// matrix_layer_t* fc1_output = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
			// matrix_layer_t* fc1_preact = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
			// matrix_layer_t* fc2 = create_matrix_layer( eid, n_hidden, n_output_classes, 50, 10);
			matrix_layer_t* fc2 = create_matrix_layer( eid, n_flatten_features, n_output_classes, 50, 10);
			matrix_layer_t* fc2_bias = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* preY = create_matrix_layer( eid, n_batches, n_output_classes);
			matrix_layer_t* trueY = create_matrix_layer( eid, 1, n_batches);

			cnn_layer_t* conv2 = create_cnn_layer(
				conv1_pooled,
				conv2_kernal,
				conv2_bias,
				conv2_preact,
				conv2_acted,
				conv2_pooled,
				padding2_y,
				padding2_x,
				stride2_y,
				stride2_x,
				pooling2_field_y,
				pooling2_field_x,
				pooling2_padding_y,
				pooling2_padding_x,
				pooling2_stride_y,
				pooling2_stride_x);

			// fc_layer_t* fc1_layer = create_fc_layer(flatten, fc1, fc1_bias, fc1_preact, fc1_output);
			fc_layer_t* fc2_layer = create_fc_layer(flatten, fc2, fc2_bias, preY, trueY);

			// Start loading
			// ret_proc |= matrix_randomize_seal(eid, fc1);
			// ret_proc |= matrix_randomize_seal(eid, fc1_bias);
			ret_proc |= matrix_randomize_seal(eid, fc2);
			ret_proc |= matrix_randomize_seal(eid, fc2_bias);
			ret_proc |= matrix_randomize_seal(eid, preY);
			FOR(i, n_batches) trueY->plain[i] = i % n_output_classes;
			ret_proc |= layer_load_sealed(eid, trueY);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			CHECK_SUCCESS(layer_load_sealed(eid, trueY));
			CHECK_SUCCESS(tensor_randomize_seal(eid, conv1_pooled));
			CHECK_SUCCESS(tensor_randomize_seal(eid, conv1_kernal));
			CHECK_SUCCESS(tensor_randomize_seal(eid, conv1_bias));
			CHECK_SUCCESS(tensor_randomize_seal(eid, conv2_kernal));
			CHECK_SUCCESS(tensor_randomize_seal(eid, conv2_bias));

			float loss = 0.0f;
			float acc = 0.0f;
			float learning_rate = 5e-1f;
			float update_rate = learning_rate;

			// Gredient Check
			tensor_layer_t* target_layer = conv1_pooled;

			// CHECK_SUCCESS(tensor_randomize_seal(eid, conv2_preact));
			// CHECK_SUCCESS(tensor_clean(eid, conv2_preact));
			CHECK_SUCCESS(tensor_randomize_seal(eid, target_layer));
			float base_loss, plus_loss, minus_loss;
			
			// forwarding
			auto forward = [&](float& compute_loss) {
				// // Conv2: Conv -> Preact
				// enclave_convolute_tensor_kernal(
				// 	eid, &ret_proc,
				// 	conv1_pooled->sealed, conv1_pooled->sealed->sz_itself,
				// 	conv2_kernal->sealed, conv2_kernal->sealed->sz_itself,
				// 	conv2_preact->sealed, conv2_preact->sealed->sz_itself,
				// 	padding2_y, padding2_x,
				// 	stride2_y, stride2_x,
				// 	1.0f, 0.0f,
				// 	0, 0);
				// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// // // Should not be added into gradient check because it will affect preact in next round
				// // enclave_sealed_tensor_elem_copab_alpha_beta(
				// // 	eid, &ret_proc,
				// // 	conv2_preact->sealed, conv2_preact->sealed->sz_itself,
				// // 	conv2_bias->sealed, conv2_bias->sealed->sz_itself,
				// // 	conv2_preact->sealed, conv2_preact->sealed->sz_itself,
				// // 	1.0f, 1.0f,
				// // 	ADD);
				// // Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// // Conv2: Preact -> Acted
				// enclave_sealed_tensor_elem_bopa(
				// 	eid, &ret_proc,
				// 	conv2_preact->sealed, conv2_preact->sealed->sz_itself,
				// 	conv2_acted->sealed, conv2_acted->sealed->sz_itself,
				// 	SIGMOID);
				// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// enclave_tensor_pooling(
				// 	eid, &ret_proc,
				// 	conv2_acted->sealed, conv2_acted->sealed->sz_itself,
				// 	conv2_pooled->sealed, conv2_pooled->sealed->sz_itself,
				// 	NULL, 0,
				// 	pooling2_field_y, pooling2_field_x,
				// 	pooling2_padding_y, pooling2_padding_x,
				// 	pooling2_stride_y, pooling2_stride_x,
				// 	AVERAGE_POOLING);
				// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				CHECK_SUCCESS(cnn_forward(eid, conv2, false));

				enclave_tensor_flatten(
					eid, &ret_proc,
					conv2_pooled->sealed, conv2_pooled->sealed->sz_itself,
					flatten->sealed, flatten->sealed->sz_itself);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

				// CHECK_SUCCESS(fc_forward(eid, fc1_layer, true));
				CHECK_SUCCESS(fc_forward(eid, fc2_layer, false));

				enclave_output_layer(
					eid, &ret_proc,
					trueY->sealed, trueY->sealed->sz_itself,
					preY->sealed, preY->sealed->sz_itself,
					preY->sealed_diff, preY->sealed_diff->sz_itself,
					&compute_loss);
				Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);
			};

			forward(base_loss);

			// backward
			CHECK_SUCCESS(fc_backward(eid, fc2_layer, false, true));
			// CHECK_SUCCESS(fc_backward(eid, fc1_layer, true, true));

			// backward of flatten Layer 
			enclave_tensor_backward_flatten(
				eid, &ret_proc,
				flatten->sealed_diff, flatten->sealed_diff->sz_itself,
				conv2_pooled->sealed_diff, conv2_pooled->sealed_diff->sz_itself);
			Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			CHECK_SUCCESS(cnn_backward(eid, conv2, true));

			// enclave_tensor_pooling(
			// 	eid, &ret_proc,
			// 	conv2_pooled->sealed_diff, conv2_pooled->sealed_diff->sz_itself,
			// 	conv2_acted->sealed_diff, conv2_acted->sealed_diff->sz_itself,
			// 	NULL, 0,
			// 	pooling2_field_y, pooling2_field_x,
			// 	pooling2_padding_y, pooling2_padding_x,
			// 	pooling2_stride_y, pooling2_stride_x,
			// 	BACKWARD_AVERAGE_POOLING);
			// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			// enclave_sealed_tensor_elem_copab_alpha_beta(
			// 	eid, &ret_proc,
			// 	conv2_preact->sealed, conv2_preact->sealed->sz_itself,
			// 	conv2_acted->sealed_diff, conv2_acted->sealed_diff->sz_itself,
			// 	conv2_preact->sealed_diff, conv2_preact->sealed_diff->sz_itself,
			// 	0.0f, 0.0f,
			// 	SIGMOID_DIFFA_B);
			// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			// enclave_backward_convolute_tensor_kernal(
			// 	eid, &ret_proc,
			// 	conv1_pooled->sealed, conv1_pooled->sealed->sz_itself,
			// 	conv2_kernal->sealed_diff, conv2_kernal->sealed_diff->sz_itself,
			// 	conv2_preact->sealed_diff, conv2_preact->sealed_diff->sz_itself,
			// 	padding2_y, padding2_x,
			// 	stride2_y, stride2_x,
			// 	1.0f, 0.0f);
			// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			// enclave_backward_convolute_tensor_input(
			// 	eid, &ret_proc,
			// 	conv1_pooled->sealed_diff, conv1_pooled->sealed_diff->sz_itself,
			// 	conv2_kernal->sealed, conv2_kernal->sealed->sz_itself,
			// 	conv2_preact->sealed_diff, conv2_preact->sealed_diff->sz_itself,
			// 	padding2_y, padding2_x,
			// 	stride2_y, stride2_x,
			// 	1.0f, 0.0f);
			// Assert::IsTrue(ret_proc == ENCLAVE_SUCCESS);

			// End of backward

			float eps = 1e-2f;
			uint32_t test_times = 50;
			srand(52223);

			CHECK_SUCCESS(unseal_tensor_layer(eid, target_layer, false));
			CHECK_SUCCESS(unseal_tensor_layer(eid, conv2_pooled, false));
			CHECK_SUCCESS(unseal_matrix_layer(eid, flatten, false));

			FOR(i, test_times) {
				uint32_t pos = rand() % get_tensor_n_elem(target_layer);
				float back_diff = target_layer->plain_diff[pos];

				tensor_alter(eid, target_layer, pos, eps);
				forward(plus_loss);

				tensor_alter(eid, target_layer, pos, -2.0f * eps);
				forward(minus_loss);

				float finite_diff = (plus_loss - minus_loss) / (2.0f * eps);
				Assert::AreEqual(finite_diff, back_diff, abs(MAX(1e-3f, ABS(finite_diff)) + MAX(1e-3f, ABS(back_diff))) * 0.5f);
			}

			// TODO: Free Memory

			end();
		}
	};
}