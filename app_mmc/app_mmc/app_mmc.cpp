#include "stdafx.h"

#include <crtdbg.h>
#include <assert.h>
#include <random>
#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <direct.h>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>

#define ENCLAVE_FILE _T("enclave_mmc.signed.dll")

#include "sgx_urts.h"
#include "sgx_report.h"
#include "datatypes.h"
#include "enclave_mmc_u.h"
#include "construction.h"

#define CHECK_SGX(x_) if ((uint32_t) (x_) != (uint32_t) SGX_SUCCESS) exit(1);

class Timing {
public:
	Timing(std::string name_) : name(name_) {};

	void start() { 
		start_time = std::chrono::high_resolution_clock::now(); 
	}

	void end() {
		end_time = std::chrono::high_resolution_clock::now(); 
	}

	void show(std::chrono::time_point<std::chrono::steady_clock> show_time) {
		std::cout << name << " took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(show_time - start_time).count()
			<< " milliseconds\n";
	}

	void print_now() {
		show(std::chrono::high_resolution_clock::now());
	}

	void print() {
		show(end_time);
	}

	void end_print() {
		end();
		print();
	}

private:
	std::string name;
	std::chrono::time_point<std::chrono::steady_clock> start_time, end_time;
};

void MNIST_MLP() {
	sgx_enclave_id_t   eid;
	sgx_status_t       ret = SGX_SUCCESS;
	sgx_launch_token_t token = { 0 };
	int updated = 0;

	ret = sgx_create_enclave(ENCLAVE_FILE, 1, &token, &updated, &eid, NULL);
	assert((uint32_t) ret == (uint32_t) SGX_SUCCESS);

	// Load all the training images and labels
	Timing loading_time("loading_training_data");
	loading_time.start();

	float* f_image_mnist = NULL;
	float* f_label_mnist = NULL;
	int n_images, n_rows, n_cols;
	read_mnist_image_label(
		"..\\data_mnist\\train-images-idx3-ubyte",
		"..\\data_mnist\\train-labels-idx1-ubyte",
		&f_image_mnist,
		&f_label_mnist,
		&n_images,
		&n_rows,
		&n_cols);

	loading_time.end_print();

	FOR(i, n_images * n_rows * n_cols) f_image_mnist[i] = f_image_mnist[i] / 128.0f - 1.0f;

	// Load all the prediction images and labels
	float* f_image_pred = NULL;
	float* f_label_pred = NULL;
	int n_pred, n_rows_pred, n_cols_pred;
	read_mnist_image_label(
		"..\\data_mnist\\t10k-images-idx3-ubyte",
		"..\\data_mnist\\t10k-labels-idx1-ubyte",
		&f_image_pred,
		&f_label_pred,
		&n_pred,
		&n_rows_pred,
		&n_cols_pred);

	FOR(i, n_pred * n_rows * n_cols) f_image_pred[i] = f_image_pred[i] / 128.0f - 1.0f;

	// Parameter

	uint32_t ret_proc = ENCLAVE_SUCCESS;

	uint32_t n_batches = 32;
	uint32_t n_features = n_rows * n_cols;
	uint32_t n_hidden_units = 128;
	uint32_t n_output_classes = 10;

#define GEN_SEAL_MAT(x_, row_, col_) \
	size_t sz_##x_ = (row_) * (col_) * sizeof(float); \
	float* (x_) = (float*)malloc(sz_##x_); \
	sealed_divided_matrix_t* sealed_##x_ = create_empty_sealed_divided_matrix( eid, (row_), (col_), 32, 32);

#define GEN_PARA_SEAL_PLAIN(x_, row_, col_) \
	size_t sz_##x_ = (row_) * (col_) * sizeof(float); \
	float* (x_) = (float*)malloc(sz_##x_); \
	sealed_divided_matrix_t* sealed_##x_ = create_empty_sealed_divided_matrix( eid, (row_), (col_), (row_), (col_));

	GEN_SEAL_MAT(input, n_batches, n_features);
	GEN_SEAL_MAT(fc1, n_features, n_hidden_units);
	GEN_SEAL_MAT(diff_fc1, n_features, n_hidden_units);
	GEN_SEAL_MAT(b1, n_batches, n_hidden_units);
	GEN_SEAL_MAT(activation_input, n_batches, n_hidden_units);
	GEN_SEAL_MAT(diff_activation_input, n_batches, n_hidden_units);
	GEN_SEAL_MAT(middle_hidden, n_batches, n_hidden_units);
	GEN_SEAL_MAT(diff_middle_hidden, n_batches, n_hidden_units);
	GEN_SEAL_MAT(fc2, n_hidden_units, n_output_classes);
	GEN_SEAL_MAT(b2, n_batches, n_output_classes);
	GEN_SEAL_MAT(diff_fc2, n_hidden_units, n_output_classes);
	GEN_SEAL_MAT(preY, n_batches, n_output_classes);
	GEN_SEAL_MAT(diff_preY, n_batches, n_output_classes);
	GEN_PARA_SEAL_PLAIN(trueY, 1, n_batches);

	// Randomize and set the matrices

	FOR(i, n_features) FOR(j, n_hidden_units) fc1[i * n_hidden_units + j] = (- 0.523 * i + 0.22 * j + 0.23) / (n_features + n_hidden_units) / 100;
	FOR(i, n_batches) FOR(j, n_hidden_units) b1[i * n_hidden_units + j] = (- 0.623 * i + 0.32 * j + 0.13) / (n_batches + n_hidden_units) / 100;
	FOR(i, n_hidden_units) FOR(j, n_output_classes) fc2[i * n_output_classes + j] = (0.62 * i - 0.32 * j + 0.31) / (n_hidden_units + n_output_classes) / 100;
	FOR(i, n_batches) FOR(j, n_output_classes) b2[i * n_output_classes + j] = (0.3 * i - 0.9 * j) / (n_batches + n_output_classes) / 100;

#define LOAD_TO_SEAL(y_) \
	ret_proc = load_sealed_divided_matrix(eid, sealed_##y_, y_, sz_##y_); \
	assert(ret_proc == ENCLAVE_SUCCESS);

	LOAD_TO_SEAL(fc1);
	LOAD_TO_SEAL(b1);
	LOAD_TO_SEAL(fc2);
	LOAD_TO_SEAL(b2);

	memset(activation_input, 0, sz_activation_input);
	memset(middle_hidden, 0, sz_middle_hidden);
	memset(preY, 0, sz_preY);
	LOAD_TO_SEAL(activation_input);
	LOAD_TO_SEAL(middle_hidden);
	LOAD_TO_SEAL(preY);


	float loss = 0.0f;
	float acc = 0.0f;
	float learning_rate = 0.5f;
	float momentum = 1.0f;
	float update_rate = learning_rate;

	uint32_t total_n_train = 15 * 50000;
	uint32_t n_iter = total_n_train / n_batches + 1;

	Timing training_time("training_MNIST");
	training_time.start();

	auto forwarding = [&]() {
		// act = input * fc1 + b1
		// Timing FC1_time("FC1");
		// FC1_time.start();
		sealed_gemm_with_functions(
			eid, &ret_proc,
			sealed_input,
			sealed_fc1,
			sealed_activation_input,
			0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		// FC1_time.end_print();
		assert(ret_proc == ENCLAVE_SUCCESS);
		enclave_sealed_divided_geadd(
			eid, &ret_proc,
			sealed_b1, sealed_b1->sz_itself,
			sealed_activation_input, sealed_activation_input->sz_itself,
			1.0f, 1.0f,
			ADD);
		assert(ret_proc == ENCLAVE_SUCCESS);

		// h = sigmoid(act)
		enclave_sealed_divided_elem_bopa(
			eid, &ret_proc,
			sealed_activation_input, sealed_activation_input->sz_itself,
			sealed_middle_hidden, sealed_middle_hidden->sz_itself,
			SIGMOID);
		assert(ret_proc == ENCLAVE_SUCCESS);

		// preY = h * fc2 + b2
		sealed_gemm_with_functions(
			eid, &ret_proc,
			sealed_middle_hidden,
			sealed_fc2,
			sealed_preY,
			0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		assert(ret_proc == ENCLAVE_SUCCESS);
		enclave_sealed_divided_geadd(
			eid, &ret_proc,
			sealed_b2, sealed_b2->sz_itself,
			sealed_preY, sealed_preY->sz_itself,
			1.0f, 1.0f,
			ADD);
		assert(ret_proc == ENCLAVE_SUCCESS);
	};

	auto testing_all = [&]() {
		float sum_testing_acc = 0;
		float testing_acc = 0;
		uint32_t total_testing_round = n_pred / n_batches;
		FOR(idx_test, total_testing_round) {
			uint32_t test_batch = (idx_test * n_batches) % (n_pred - n_batches);
			memcpy(input, &f_image_pred[test_batch * n_features], n_batches * n_features * sizeof(float));
			memcpy(trueY, &f_label_pred[test_batch], n_batches * sizeof(float));
			LOAD_TO_SEAL(input);
			LOAD_TO_SEAL(trueY);
			forwarding();
			enclave_output_layer_acc(
				eid, &ret_proc,
				sealed_trueY, sealed_trueY->sz_itself,
				sealed_preY, sealed_preY->sz_itself,
				&testing_acc);
			sum_testing_acc += testing_acc;
		}
		float avg_testing_acc = sum_testing_acc / total_testing_round;
		printf("testing, acc: %.3f \n", avg_testing_acc);
	};

	FOR(idx_iter, n_iter) {

		uint32_t start_batch = (idx_iter * n_batches) % (n_images - n_batches);
		// uint32_t start_batch = 0;

		memcpy(input, &f_image_mnist[start_batch * n_features], n_batches * n_features * sizeof(float));
		memcpy(trueY, &f_label_mnist[start_batch], n_batches * sizeof(float));

		// FOR(i, n_batches * n_features) { if (input[i] < -1.0f || input[i] > 1.0f) printf("%2f\n", input[i]); }
		// FOR(i, n_rows) { FOR(j, n_cols) { printf("%.3f ", input[i * n_cols + j]); } printf("\n"); }
		// FOR(i, n_batches) printf("%2g ", trueY[i]); printf("\n");

		LOAD_TO_SEAL(input);
		LOAD_TO_SEAL(trueY);

		// forwarding
		forwarding();

		enclave_output_layer(
			eid, &ret_proc,
			sealed_trueY, sealed_trueY->sz_itself,
			sealed_preY, sealed_preY->sz_itself,
			sealed_diff_preY, sealed_diff_preY->sz_itself,
			&loss);
		assert(ret_proc == ENCLAVE_SUCCESS);

		// backward
		// the diff of weighting of fc2
		sealed_gemm_with_functions(
			eid, &ret_proc,
			sealed_middle_hidden,
			sealed_diff_preY,
			sealed_diff_fc2,
			1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		assert(ret_proc == ENCLAVE_SUCCESS);
		// the diff of the middle hidden
		sealed_gemm_with_functions(
			eid, &ret_proc,
			sealed_diff_preY,
			sealed_fc2,
			sealed_diff_middle_hidden,
			0, 1, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		assert(ret_proc == ENCLAVE_SUCCESS);
		// the diff of the activation layer
		enclave_sealed_divided_elem_copab_alpha_beta(
			eid, &ret_proc,
			sealed_activation_input, sealed_activation_input->sz_itself,
			sealed_diff_middle_hidden, sealed_diff_middle_hidden->sz_itself,
			sealed_diff_activation_input, sealed_diff_activation_input->sz_itself,
			0.0f, 0.0f,
			SIGMOID_DIFFA_B);
		assert(ret_proc == ENCLAVE_SUCCESS);
		// the diff of weighting of fc1
		sealed_gemm_with_functions(
			eid, &ret_proc,
			sealed_input,
			sealed_diff_activation_input,
			sealed_diff_fc1,
			1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		assert(ret_proc == ENCLAVE_SUCCESS);
		
		// Calculate the loss and accuracy

		if (idx_iter % 100 == 0) {
			enclave_output_layer_acc(
				eid, &ret_proc,
				sealed_trueY, sealed_trueY->sz_itself,
				sealed_preY, sealed_preY->sz_itself,
				&acc);
			printf("turn: %5d | testing, loss: %.3f acc: %.3f \n", idx_iter, loss, acc);
		}
		if (idx_iter % 2000 == 0) testing_all();


		// Update the matrices
#define UPDATE_ADD(X_, Y_, alpha_, beta_) \
	enclave_sealed_divided_geadd( \
		eid, &ret_proc, \
		X_, X_->sz_itself, \
		Y_, Y_->sz_itself, \
		alpha_, beta_, \
		ADD); \
		assert(ret_proc == ENCLAVE_SUCCESS);

		UPDATE_ADD(sealed_diff_fc1, sealed_fc1, -update_rate, momentum);
		UPDATE_ADD(sealed_diff_activation_input, sealed_b1, -update_rate, momentum);
		UPDATE_ADD(sealed_diff_fc2, sealed_fc2, -update_rate, momentum);
		UPDATE_ADD(sealed_diff_preY, sealed_b2, -update_rate, momentum);

	}

	training_time.end_print();

	Timing prediction_all_time("prediction_all_MNIST");
	prediction_all_time.start();

	testing_all();

	prediction_all_time.end_print();

	free(f_image_mnist);
	free(f_label_mnist);

	free(input);
	free(fc1);
	free(diff_fc1);
	free(b1);
	free(activation_input);
	free(diff_activation_input);
	free(middle_hidden);
	free(diff_middle_hidden);
	free(fc2);
	free(b2);
	free(diff_fc2);
	free(preY);
	free(diff_preY);
	free(trueY);

	sgx_status_t destroy_ret = sgx_destroy_enclave(eid);
	assert((uint32_t) SGX_SUCCESS == (uint32_t) destroy_ret);
}

void MNIST_CryptoNet() {

	sgx_enclave_id_t   eid;
	sgx_status_t       ret = SGX_SUCCESS;
	sgx_launch_token_t token = { 0 };
	int updated = 0;

	ret = sgx_create_enclave(ENCLAVE_FILE, 1, &token, &updated, &eid, NULL);
	assert((uint32_t) ret == (uint32_t) SGX_SUCCESS);

	// Load all the training images and labels
	Timing loading_time("loading_training_data");
	loading_time.start();

	float* f_image_mnist = NULL;
	float* f_label_mnist = NULL;
	int n_images, n_rows, n_cols;
	read_mnist_image_label(
		"..\\data_mnist\\train-images-idx3-ubyte",
		"..\\data_mnist\\train-labels-idx1-ubyte",
		&f_image_mnist,
		&f_label_mnist,
		&n_images,
		&n_rows,
		&n_cols);

	loading_time.end_print();

	FOR(i, n_images * n_rows * n_cols) f_image_mnist[i] = f_image_mnist[i] / 128.0f - 1.0f;

	// Load all the prediction images and labels
	float* f_image_pred = NULL;
	float* f_label_pred = NULL;
	int n_pred, n_rows_pred, n_cols_pred;
	read_mnist_image_label(
		"..\\data_mnist\\t10k-images-idx3-ubyte",
		"..\\data_mnist\\t10k-labels-idx1-ubyte",
		&f_image_pred,
		&f_label_pred,
		&n_pred,
		&n_rows_pred,
		&n_cols_pred);

	FOR(i, n_pred * n_rows * n_cols) f_image_pred[i] = f_image_pred[i] / 128.0f - 1.0f;

	// Parameter

	uint32_t ret_proc = ENCLAVE_SUCCESS;

	uint32_t n_batches = 50;
	uint32_t n_hidden = 100;
	uint32_t n_features = n_rows * n_cols;
	uint32_t n_output_classes = 10;

	uint32_t n_channels1 = 1;
	uint32_t n_channels2 = 5;
	uint32_t n_channels3 = 10;

	uint32_t n_image1_rows = 28;
	uint32_t n_image1_cols = 28;
	uint32_t n_kernal1_rows = 5;
	uint32_t n_kernal1_cols = 5;
	uint32_t n_output1_rows;
	uint32_t n_output1_cols;
	uint32_t padding1_y = 1;
	uint32_t padding1_x = 1;
	uint32_t stride1_y = 2;
	uint32_t stride1_x = 2;
	uint32_t pooling1_field_y = 3;
	uint32_t pooling1_field_x = 3;
	uint32_t pooling1_stride_y = 1;
	uint32_t pooling1_stride_x = 1;
	uint32_t pooling1_padding_y = 1;
	uint32_t pooling1_padding_x = 1;
	uint32_t pooling1_output_y;
	uint32_t pooling1_output_x;

	get_output_tensor_shape(
		n_channels1, n_channels2,
		n_image1_rows, n_image1_cols,
		n_kernal1_rows, n_kernal1_cols,
		stride1_y, stride1_x,
		padding1_y, padding1_x,
		&n_output1_rows, &n_output1_cols);

	get_output_tensor_shape(
		n_channels1, n_channels2,
		n_output1_rows, n_output1_cols,
		pooling1_field_y, pooling1_field_x,
		pooling1_stride_y, pooling1_stride_x,
		pooling1_padding_y, pooling1_padding_x,
		&pooling1_output_y, &pooling1_output_x);

	uint32_t n_image2_rows = pooling1_output_y;
	uint32_t n_image2_cols = pooling1_output_x;
	uint32_t n_kernal2_rows = 5;
	uint32_t n_kernal2_cols = 5;
	uint32_t n_output2_rows;
	uint32_t n_output2_cols;
	uint32_t padding2_y = 0;
	uint32_t padding2_x = 0;
	uint32_t stride2_y = 2;
	uint32_t stride2_x = 2;
	uint32_t pooling2_field_y = 3;
	uint32_t pooling2_field_x = 3;
	uint32_t pooling2_stride_y = 1;
	uint32_t pooling2_stride_x = 1;
	uint32_t pooling2_padding_y = 1;
	uint32_t pooling2_padding_x = 1;
	uint32_t pooling2_output_y;
	uint32_t pooling2_output_x;

	get_output_tensor_shape(
		n_channels2, n_channels3,
		n_image2_rows, n_image2_cols,
		n_kernal2_rows, n_kernal2_cols,
		stride2_y, stride2_x,
		padding2_y, padding2_x,
		&n_output2_rows, &n_output2_cols);

	get_output_tensor_shape(
		n_channels2, n_channels3,
		n_output2_rows, n_output2_cols,
		pooling2_field_y, pooling2_field_x,
		pooling2_stride_y, pooling2_stride_x,
		pooling2_padding_y, pooling2_padding_x,
		&pooling2_output_y, &pooling2_output_x);

	uint32_t n_flatten_features = n_channels3 * pooling2_output_y * pooling2_output_x;

	// The 1st CNN
	tensor_layer_t* image1 = create_tensor_layer( eid, n_batches, n_channels1, n_image1_rows, n_image1_cols);
	tensor_layer_t* conv1_kernal = create_tensor_layer( eid, n_channels2, n_channels1, n_kernal1_rows, n_kernal1_cols);
	tensor_layer_t* conv1_bias = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
	tensor_layer_t* conv1_preact = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
	tensor_layer_t* conv1_acted = create_tensor_layer( eid, n_batches, n_channels2, n_output1_rows, n_output1_cols);
	tensor_layer_t* conv1_pooled = create_tensor_layer( eid, n_batches, n_channels2, pooling1_output_y, pooling1_output_x);

	// The 2nd CNN
	tensor_layer_t* conv2_kernal = create_tensor_layer( eid, n_channels3, n_channels2, n_kernal2_rows, n_kernal2_cols);
	tensor_layer_t* conv2_bias = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
	tensor_layer_t* conv2_preact = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
	tensor_layer_t* conv2_acted = create_tensor_layer( eid, n_batches, n_channels3, n_output2_rows, n_output2_cols);
	tensor_layer_t* conv2_pooled = create_tensor_layer( eid, n_batches, n_channels3, pooling2_output_y, pooling2_output_x);

	// For the fully connected layers
	matrix_layer_t* flatten = create_matrix_layer( eid, n_batches, n_flatten_features, 50, 50);
	matrix_layer_t* fc1 = create_matrix_layer( eid, n_flatten_features, n_hidden, 50, 50);
	matrix_layer_t* fc1_bias = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
	matrix_layer_t* fc1_output = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
	matrix_layer_t* fc1_preact = create_matrix_layer( eid, n_batches, n_hidden, 50, 50);
	matrix_layer_t* fc2 = create_matrix_layer( eid, n_hidden, n_output_classes, 50, 10);
	matrix_layer_t* fc2_bias = create_matrix_layer( eid, n_batches, n_output_classes);
	matrix_layer_t* preY = create_matrix_layer( eid, n_batches, n_output_classes);
	matrix_layer_t* trueY = create_matrix_layer( eid, 1, n_batches);

	cnn_layer_t* conv1 = create_cnn_layer(
		image1,
		conv1_kernal,
		conv1_bias,
		conv1_preact,
		conv1_acted,
		conv1_pooled,
		padding1_y,
		padding1_x,
		stride1_y,
		stride1_x,
		pooling1_field_y,
		pooling1_field_x,
		pooling1_padding_y,
		pooling1_padding_x,
		pooling1_stride_y,
		pooling1_stride_x);

	cnn_layer_t* conv2 = create_cnn_layer(
		conv1_pooled,
		conv2_kernal,
		conv2_bias,
		conv2_preact,
		conv2_acted,
		conv2_pooled,
		padding2_y,
		padding2_x,
		stride2_y,
		stride2_x,
		pooling2_field_y,
		pooling2_field_x,
		pooling2_padding_y,
		pooling2_padding_x,
		pooling2_stride_y,
		pooling2_stride_x);

	fc_layer_t* fc1_layer = create_fc_layer(flatten, fc1, fc1_bias, fc1_preact, fc1_output);
	fc_layer_t* fc2_layer = create_fc_layer(fc1_output, fc2, fc2_bias, preY, trueY);

	// Start loading
	ret_proc |= matrix_randomize_seal(eid, fc1);
	ret_proc |= matrix_randomize_seal(eid, fc1_bias);
	ret_proc |= matrix_randomize_seal(eid, fc2);
	ret_proc |= matrix_randomize_seal(eid, fc2_bias);
	CHECK_SGX(ret_proc);

	CHECK_SGX(tensor_randomize_seal(eid, conv1_kernal));
	CHECK_SGX(tensor_randomize_seal(eid, conv1_bias));
	CHECK_SGX(tensor_randomize_seal(eid, conv2_kernal));
	CHECK_SGX(tensor_randomize_seal(eid, conv2_bias));

	float loss = 0.0f;
	float acc = 0.0f;
	float learning_rate = 1.0f;
	float update_rate = learning_rate;
	
	// forwarding
	auto forward = [&](float& compute_loss) {
		CHECK_SGX(cnn_forward(eid, conv1, true));
		CHECK_SGX(cnn_forward(eid, conv2, true));

		enclave_tensor_flatten(
			eid, &ret_proc,
			conv2_pooled->sealed, conv2_pooled->sealed->sz_itself,
			flatten->sealed, flatten->sealed->sz_itself);
		CHECK_SGX(ret_proc);

		CHECK_SGX(fc_forward(eid, fc1_layer, true));
		CHECK_SGX(fc_forward(eid, fc2_layer, false));

		enclave_output_layer(
			eid, &ret_proc,
			trueY->sealed, trueY->sealed->sz_itself,
			preY->sealed, preY->sealed->sz_itself,
			preY->sealed_diff, preY->sealed_diff->sz_itself,
			&compute_loss);
		CHECK_SGX(ret_proc);
	};
	// End of forward

	// backward
	auto backward = [&]() {
		CHECK_SGX(fc_backward(eid, fc2_layer, false, true));
		CHECK_SGX(fc_backward(eid, fc1_layer, true, true));

		// backward of flatten Layer 
		enclave_tensor_backward_flatten(
			eid, &ret_proc,
			flatten->sealed_diff, flatten->sealed_diff->sz_itself,
			conv2_pooled->sealed_diff, conv2_pooled->sealed_diff->sz_itself);
		CHECK_SGX(ret_proc);

		CHECK_SGX(cnn_backward(eid, conv2, true));
		CHECK_SGX(cnn_backward(eid, conv1, false));
	};
	// End of backward

	auto update = [&]() {
		CHECK_SGX(cnn_update(eid, conv1, learning_rate));
		CHECK_SGX(cnn_update(eid, conv2, learning_rate));
		CHECK_SGX(fc_update(eid, fc1_layer, learning_rate));
		CHECK_SGX(fc_update(eid, fc2_layer, learning_rate));
	};

	// Training Related
	uint32_t total_n_train = 15 * 50000;
	uint32_t n_iter = total_n_train / n_batches + 1;
	n_iter = 100;

	Timing training_time("training_MNIST");
	training_time.start();

	FOR(idx_iter, n_iter) {
		uint32_t start_batch = (idx_iter * n_batches) % (n_images - n_batches);

		memcpy(image1->plain, &f_image_mnist[start_batch * n_features], n_batches * n_features * sizeof(float));
		memcpy(trueY->plain, &f_label_mnist[start_batch], n_batches * sizeof(float));

		CHECK_SGX(layer_load_sealed( eid, image1));
		CHECK_SGX(layer_load_sealed( eid, trueY));

		forward(loss);
		backward();
		update();

		printf("turn: %5d | training, loss: %.3f | ", idx_iter, loss);
		training_time.print_now();

		// if (idx_iter >= 1000 && idx_iter % 1000 == 0) {
		// 	learning_rate *= 0.8f;
		// }

		if (idx_iter % 100 == 0) {
			// learning_rate *= 0.97;
			enclave_output_layer_acc(
				eid, &ret_proc,
				trueY->sealed, trueY->sealed->sz_itself,
				preY->sealed, preY->sealed->sz_itself,
				&acc);
			printf("turn: %5d | testing, loss: %.3f acc: %.3f \n", idx_iter, loss, acc);
		}
		// if (idx_iter % 2000 == 0) testing_all();
	}
	// End of Training Related

	auto testing_all = [&]() {
		float sum_testing_acc = 0;
		float testing_acc = 0;
		uint32_t total_testing_round = n_pred / n_batches;
		FOR(idx_test, total_testing_round) {
			uint32_t start_batch = (idx_test * n_batches) % (n_pred - n_batches);

			memcpy(image1->plain, &f_image_pred[start_batch * n_features], n_batches * n_features * sizeof(float));
			memcpy(trueY->plain, &f_label_pred[start_batch], n_batches * sizeof(float));

			CHECK_SGX(layer_load_sealed( eid, image1));
			CHECK_SGX(layer_load_sealed( eid, trueY));
			forward(loss);
			enclave_output_layer_acc(
				eid, &ret_proc,
				trueY->sealed, trueY->sealed->sz_itself,
				preY->sealed, preY->sealed->sz_itself,
				&testing_acc);
			sum_testing_acc += testing_acc;
		}
		float avg_testing_acc = sum_testing_acc / total_testing_round;
		printf("testing, acc: %.3f \n", avg_testing_acc);
	};

	Timing testingall_time("testing_MNIST");
	testingall_time.start();

	testing_all();

	testingall_time.end_print();
	loading_time.end_print();

	// TODO: Free Memory

	sgx_status_t destroy_ret = sgx_destroy_enclave(eid);
	assert((uint32_t) SGX_SUCCESS == (uint32_t) destroy_ret);
}

int main() {
	// MNIST_MLP();

	MNIST_CryptoNet();

	getchar();
	return 0;
}