#pragma once

void read_mnist_image_label(
	char* path_images,
	char* path_labels,
	float** img_arr_p,
	float** lab_arr_p,
	int* _n_images_p,
	int* _n_rows_p,
	int* _n_cols_p);

void read_mnist_image(char* path_input_file,
	float**** res_arr_p,
	int* _n_images_p,
	int* _n_rows_p,
	int* _n_cols_p);

void read_mnist_label(char* path_input_file,
	int** res_arr_p,
	int* _n_labels_p);