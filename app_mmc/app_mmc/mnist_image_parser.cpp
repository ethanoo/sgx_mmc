/*
============================================================================
Name        : mnist_image_parser.c
Author      : Afroz Mohiuddin
Version     : 1
Description : Parser for mnist image db file.
============================================================================
*/

#include "stdafx.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <map>
#include <cstring>
#include <algorithm>
#include <cassert>

#include "hex_lib.h"
#include "const_macros.h"
#include "mnist_image_parser.h"

void endianSwap(unsigned int &x) {
	x = (x >> 24) | ((x << 8) & 0x00FF0000) | ((x >> 8) & 0x0000FF00) | (x << 24);
}

// Refer to: https://gist.github.com/spaghetti-source/5620288
void read_mnist_image_label(
	char* path_images,
	char* path_labels,
	float** img_arr_p,
	float** lab_arr_p,
	int* _n_images_p,
	int* _n_rows_p,
	int* _n_cols_p) 
{
	FILE *fimage = fopen(path_images, "rb");
	FILE *flabel = fopen(path_labels, "rb");
	assert(fimage);
	assert(flabel);

	unsigned int magic, num, row, col;
	fread(&magic, 4, 1, fimage);
	assert(magic == 0x03080000);
	fread(&magic, 4, 1, flabel);
	assert(magic == 0x01080000);

	fread(&num, 4, 1, flabel); // dust
	fread(&num, 4, 1, fimage); endianSwap(num);
	fread(&row, 4, 1, fimage); endianSwap(row);
	fread(&col, 4, 1, fimage); endianSwap(col);

	printf("num %d\n", num);
	printf("col %d\n", col);
	printf("row %d\n", row);
	*_n_images_p = num;
	*_n_rows_p = row;
	*_n_cols_p = col;
	*img_arr_p = (float*)malloc(num * row * col * sizeof(float));
	*lab_arr_p = (float*)malloc(num * sizeof(float));

	int32_t num_pixles_in_image = row * col;
	int32_t image_pixel_value;

	char* images_pixels_bytes = new char[num_pixles_in_image];
	char label_byte;

	FOR(k, num) {
		fread(images_pixels_bytes, sizeof(char), num_pixles_in_image, fimage);
		for (uint32_t idx = 0; idx < num_pixles_in_image; ++idx) {
			image_pixel_value = hex_char_to_int(images_pixels_bytes[idx]);
			(*img_arr_p)[k * num_pixles_in_image + (idx / col) * col + (idx % col)] = image_pixel_value;
		}
	}
	FOR(k, num) {
		fread(&label_byte, sizeof(char), 1, flabel);
		(*lab_arr_p)[k] = hex_char_to_int(label_byte);
	}
	delete[] images_pixels_bytes;
}


void read_mnist_file(FILE* input_file_pointer,
	int32_t* number_of_images_p,
	int32_t magic_check) {

	CHECK_NOTNULL(input_file_pointer);
	char magic_number_bytes[4];
	CHECK(fread(magic_number_bytes, sizeof(char), 4, input_file_pointer));

	// If MSB is first then magic_number_bytes will be 0x00000803
	if (magic_number_bytes[2] == 0x08 && magic_number_bytes[3] == magic_check) {
		LOG_INFO("Little Endian : MSB first");
	} else if (magic_number_bytes[0] == 0x01 && magic_number_bytes[1] == 0x08) {
		// I haven't taken into account big indian-ness, yet.
		LOG_FATAL("Big Endian : MSB last");
	} else {
		LOG_FATAL("This doesn't correspond to a MNIST Label file.");
	}
	LOG_INFO("magic number: %d", hex_array_to_int(magic_number_bytes, 4));

	char number_of_images_bytes[4];
	CHECK(fread(number_of_images_bytes, sizeof(char), 4, input_file_pointer));
	int32_t number_of_images = hex_array_to_int(number_of_images_bytes, 4);
	LOG_INFO("number of images: %d", number_of_images);

	*number_of_images_p = number_of_images;

	return;
}

void read_mnist_image(char* path_input_file,
	float**** res_arr_p,
	int* _n_images_p,
	int* _n_rows_p,
	int* _n_cols_p) {

	FILE* input_file_pointer = fopen(path_input_file, "r");
	// std::ifstream file(path_input_file, std::ios::in | std::ios::binary | std::ios::ate);
	int32_t number_of_images;
	read_mnist_file(input_file_pointer, &number_of_images, 0x03);

	int32_t num_rows = 0, num_cols = 0;
	char num_row_cols[4];
	size_t res;
	char* tmp[1024];
	res = fread(tmp, sizeof(char), 1024, input_file_pointer);
	res = fread(num_row_cols, sizeof(char), 4, input_file_pointer);
	num_rows = hex_array_to_int(num_row_cols, 4);
	res = fread(num_row_cols, sizeof(char), 4, input_file_pointer);
	num_cols = hex_array_to_int(num_row_cols, 4);
	LOG_INFO("pixel rows: %d and pixel columns: %d", num_rows, num_cols);

	uint32_t num_pixles_in_image = num_cols * num_rows;
	// char* images_pixels_bytes = (char*)malloc(num_pixles_in_image * sizeof(char));
	char* images_pixels_bytes = new char[num_pixles_in_image];
	int images_done = 0;

	*_n_images_p = number_of_images;
	*_n_rows_p = num_rows;
	*_n_cols_p = num_cols;
	// ALLOCATE_3D_ARRAY(float, *res_arr_p, number_of_images, num_rows, num_cols);

	res = fread(images_pixels_bytes, sizeof(char), num_pixles_in_image, input_file_pointer);
	res = fread(images_pixels_bytes, sizeof(char), num_pixles_in_image, input_file_pointer);
	CHECK(fread(images_pixels_bytes, sizeof(char), num_pixles_in_image, input_file_pointer));
	while (images_done++ < number_of_images) {
		CHECK(fread(images_pixels_bytes, sizeof(char), num_pixles_in_image, input_file_pointer));
		for (uint32_t idx = 0; idx < num_pixles_in_image; ++idx) {
			int32_t image_pixel_value = hex_char_to_int(images_pixels_bytes[idx]);
			(*res_arr_p)[images_done - 1][idx / num_cols][idx % num_cols] =
				image_pixel_value;
		}
	}

	fclose(input_file_pointer);
	delete[] images_pixels_bytes;
	return;
}

void read_mnist_label(char* path_input_file,
	int** res_arr_p,
	int* _n_labels_p) {

	FILE* input_file_pointer = fopen(path_input_file, "r");
	int32_t number_of_labels;
	read_mnist_file(input_file_pointer, &number_of_labels, 0x01);

	*_n_labels_p = number_of_labels;
	ALLOCATE_1D_ARRAY(int, *res_arr_p, number_of_labels);

	char label_byte;
	int32_t idx_images = 0;
	while (fread(&label_byte, sizeof(char), 1, input_file_pointer)) {
		(*res_arr_p)[idx_images++] = hex_char_to_int(label_byte);
	}

	fclose(input_file_pointer);
	return;
}