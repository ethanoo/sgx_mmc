#pragma once

#include "datatypes.h"
#include "enclave_mmc_u.h"
#include "const_macros.h"
#include "mnist_image_parser.h"

void create_sealed_block(
	sgx_enclave_id_t eid,
	sealed_block_matrix_t** sealed_block, size_t* sz_sealed_block,
	int n_rows, int n_cols);

sealed_divided_matrix_t* create_empty_sealed_divided_matrix(
	sgx_enclave_id_t eid,
	uint32_t n_rows, uint32_t n_cols,
	uint32_t n_rows_each_block, uint32_t n_cols_each_block);

uint32_t destory_sealed_divided_matrix(sealed_divided_matrix_t* sealed_divided_matrix);

uint32_t load_sealed_divided_matrix(
	sgx_enclave_id_t eid,
	sealed_divided_matrix_t* sealed_divided_matrix,
	float* plain_matrix,
	size_t sz_plain_matrix);


uint32_t unseal_divided_matrix(
	sgx_enclave_id_t eid,
	sealed_divided_matrix_t* sealed_divided_matrix,
	float* plain_matrix, // Assume it was allocated
	size_t sz_plain_matrix);

void sealed_gemm_with_functions(
	sgx_enclave_id_t eid, uint32_t* ret_proc,
	sealed_divided_matrix_t* sealed_A,
	sealed_divided_matrix_t* sealed_B,
	sealed_divided_matrix_t* sealed_C,
	uint32_t is_trans_A, uint32_t is_trans_B,
	float alpha, float beta,
	elem_funcname_t preA_function_name, elem_funcname_t preB_function_name,
	elem_funcname_t post_function_name);

// TODO: Implement the destructor of sealed_divided

uint32_t get_memory_ocall(uint8_t* memory_ptr, uint8_t* receive_memory, size_t sz_memory);

uint32_t place_memory_ocall(uint8_t* memory_from_enclave, uint8_t* memory_in_untrusted, size_t sz_memory);

void PrintFullPath(char * partialPath);

sealed_tensor_t* create_empty_sealed_tensor(
	sgx_enclave_id_t eid,
	uint32_t n_batches, uint32_t n_channels,
	uint32_t n_rows, uint32_t n_cols);

uint32_t get_tensor_index(
	sealed_tensor_t* sealed_divided_tensor,
	uint32_t idx_batch, uint32_t idx_channel,
	uint32_t idx_row, uint32_t idx_col);

uint32_t get_tensor_n_elem(sealed_tensor_t* sealed_tensor);

uint32_t destory_sealed_tensor(sealed_tensor_t* sealed_divided_tensor);

uint32_t load_sealed_tensor(
	sgx_enclave_id_t eid,
	sealed_tensor_t* sealed_divided_tensor,
	float* plain_tensor,
	size_t sz_plain_tensor);

uint32_t unseal_tensor(
	sgx_enclave_id_t eid,
	sealed_tensor_t * sealed_divided_tensor,
	float* plain_tensor, // Assume it was allocated
	size_t sz_plain_tensor);

uint32_t get_output_tensor_shape(
	uint32_t n_input_channels, uint32_t n_output_channels,
	uint32_t input_y, uint32_t input_x,
	uint32_t kernal_y, uint32_t kernal_x,
	uint32_t stride_y, uint32_t stride_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t* output_y, uint32_t* output_x);

sealed_tensor_t* get_reversed_tensor(sealed_tensor_t* tensor_input);

typedef struct _tensor_layer_t {
	uint32_t n_batches;
	uint32_t n_channels;
	uint32_t n_rows;
	uint32_t n_cols;
	size_t sz_plain;
	sealed_tensor_t* sealed;
	sealed_tensor_t* sealed_reversed;
	sealed_tensor_t* sealed_diff;
	sealed_tensor_t* sealed_diff_reversed;
	float* plain;
	float* plain_diff;
} tensor_layer_t;

tensor_layer_t* create_tensor_layer(
	sgx_enclave_id_t eid,
	uint32_t n_batches,
	uint32_t n_channels,
	uint32_t n_rows,
	uint32_t n_cols);

uint32_t destroy_tensor_layer(tensor_layer_t* layer);

uint32_t tensor_randomize_seal(sgx_enclave_id_t eid, tensor_layer_t* layer);

uint32_t get_tensor_n_elem(tensor_layer_t* layer);

uint32_t tensor_alter(sgx_enclave_id_t eid, tensor_layer_t* layer, uint32_t pos, float delta);

uint32_t unseal_tensor_layer(sgx_enclave_id_t eid, tensor_layer_t* layer, bool is_normal);

uint32_t tensor_clean(sgx_enclave_id_t eid, tensor_layer_t* layer);

typedef struct _matrix_layer_t {
	uint32_t n_rows;
	uint32_t n_cols;
	uint32_t n_rows_each_block;
	uint32_t n_cols_each_block;
	sealed_divided_matrix_t* sealed;
	sealed_divided_matrix_t* sealed_diff;
	size_t sz_plain;
	float* plain;
	float* plain_diff;
} matrix_layer_t;

matrix_layer_t* create_matrix_layer(
	sgx_enclave_id_t eid,
	uint32_t n_rows,
	uint32_t n_cols,
	uint32_t n_rows_each_block,
	uint32_t n_cols_each_block);

matrix_layer_t* create_matrix_layer(
	sgx_enclave_id_t eid,
	uint32_t n_rows,
	uint32_t n_cols);

uint32_t destroy_matrix_layer(matrix_layer_t* layer);

uint32_t layer_load_sealed(sgx_enclave_id_t eid, matrix_layer_t* layer);

uint32_t layer_load_sealed(sgx_enclave_id_t eid, tensor_layer_t* layer);

uint32_t matrix_randomize_seal(sgx_enclave_id_t eid, matrix_layer_t* layer);

uint32_t matrix_clean(sgx_enclave_id_t eid, matrix_layer_t* layer);

// is_normal : not diff tensor
uint32_t unseal_matrix_layer(sgx_enclave_id_t eid, matrix_layer_t* layer, bool is_normal);

typedef struct _fc_layer_t {
	matrix_layer_t* input_layer;
	matrix_layer_t* weight_layer;
	matrix_layer_t* bias_layer;
	matrix_layer_t* preact_layer;
	matrix_layer_t* output_layer;
} fc_layer_t;

fc_layer_t* create_fc_layer(
	matrix_layer_t* input_layer,
	matrix_layer_t* weight_layer,
	matrix_layer_t* bias_layer,
	matrix_layer_t* preact_layer,
	matrix_layer_t* output_layer);

uint32_t fc_forward(sgx_enclave_id_t eid, fc_layer_t* working_layer, bool need_activation);

uint32_t fc_backward(sgx_enclave_id_t eid, fc_layer_t* working_layer, bool need_preact_diff, bool need_input_diff);

uint32_t fc_update(sgx_enclave_id_t eid, fc_layer_t* working_layer, float learning_rate, float momentum);

uint32_t fc_update(sgx_enclave_id_t eid, fc_layer_t* working_layer, float learning_rate);

typedef struct _cnn_layer_t {
	tensor_layer_t* input_layer;
	tensor_layer_t* kernal_layer;
	tensor_layer_t* bias_layer;
	tensor_layer_t* preact_layer;
	tensor_layer_t* acted_layer;
	tensor_layer_t* pooled_layer;
	uint32_t padding_y;
	uint32_t padding_x;
	uint32_t stride_y;
	uint32_t stride_x;
	uint32_t pooling_field_y;
	uint32_t pooling_field_x;
	uint32_t pooling_padding_y;
	uint32_t pooling_padding_x;
	uint32_t pooling_stride_y;
	uint32_t pooling_stride_x;
} cnn_layer_t;

cnn_layer_t* create_cnn_layer(
	tensor_layer_t* input_layer,
	tensor_layer_t* kernal_layer,
	tensor_layer_t* bias_layer,
	tensor_layer_t* preact_layer,
	tensor_layer_t* acted_layer,
	tensor_layer_t* pooled_layer,
	uint32_t padding_y,
	uint32_t padding_x,
	uint32_t stride_y,
	uint32_t stride_x,
	uint32_t pooling_field_y,
	uint32_t pooling_field_x,
	uint32_t pooling_padding_y,
	uint32_t pooling_padding_x,
	uint32_t pooling_stride_y,
	uint32_t pooling_stride_x);

uint32_t cnn_forward(sgx_enclave_id_t eid, cnn_layer_t* working_layer, bool need_bias);

uint32_t cnn_backward(sgx_enclave_id_t eid, cnn_layer_t* working_layer, bool need_input_diff);

uint32_t cnn_update(sgx_enclave_id_t eid, cnn_layer_t* working_layer, float learning_rate, float momentum);

uint32_t cnn_update(sgx_enclave_id_t eid, cnn_layer_t* working_layer, float learning_rate);
