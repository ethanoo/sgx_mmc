#include "stdafx.h"

#include <random>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <direct.h>

#include "construction.h"

uint32_t get_memory_ocall(uint8_t* memory_ptr, uint8_t* receive_memory, size_t sz_memory) {
	memcpy(receive_memory, memory_ptr, sz_memory);
	return OCALL_SUCCESS;
}

uint32_t place_memory_ocall(uint8_t* memory_from_enclave, uint8_t* memory_in_untrusted, size_t sz_memory) {
	memcpy(memory_in_untrusted, memory_from_enclave, sz_memory);
	return OCALL_SUCCESS;
}

void PrintFullPath(char * partialPath)
{
	char full[_MAX_PATH];
	if (_fullpath(full, partialPath, _MAX_PATH) != NULL)
		printf("Full path is: %s\n", full);
	else
		printf("Invalid path\n");
}

void create_sealed_block(
	sgx_enclave_id_t eid,
	sealed_block_matrix_t** sealed_block, size_t* sz_sealed_block,
	int n_rows, int n_cols)
{
	enclave_calc_sealed_block_matrix_size(eid, sz_sealed_block, n_rows, n_cols);
	*sealed_block = (sealed_block_matrix_t*)malloc(*sz_sealed_block);
}

sealed_divided_matrix_t* create_empty_sealed_divided_matrix(
	sgx_enclave_id_t eid,
	uint32_t n_rows, uint32_t n_cols,
	uint32_t n_rows_each_block, uint32_t n_cols_each_block)
{
	// if (n_rows < n_rows_each_block) return NULL;
	// if (n_cols < n_cols_each_block) return NULL;

	uint32_t n_rows_blocks = ((n_rows - 1) / n_rows_each_block) + 1;
	uint32_t n_cols_blocks = ((n_cols - 1) / n_cols_each_block) + 1;

	size_t sz_sealed_divided_matrix = sizeof(sealed_divided_matrix_t);
	sz_sealed_divided_matrix += n_rows_blocks * n_cols_blocks * sizeof(sealed_block_matrix_t*);

	size_t sz_each_sealed_block;
	enclave_calc_sealed_block_matrix_size(eid, &sz_each_sealed_block, n_rows_each_block, n_cols_each_block);

	size_t sz_each_plain_block = n_rows_each_block * n_cols_each_block * sizeof(float);

	sealed_divided_matrix_t* ret_sealed_divided_matrix = (sealed_divided_matrix_t*)malloc(sz_sealed_divided_matrix);
	ret_sealed_divided_matrix->n_rows = n_rows;
	ret_sealed_divided_matrix->n_cols = n_cols;
	ret_sealed_divided_matrix->n_rows_blocks = n_rows_blocks;
	ret_sealed_divided_matrix->n_cols_blocks = n_cols_blocks;
	ret_sealed_divided_matrix->n_rows_each_block = n_rows_each_block;
	ret_sealed_divided_matrix->n_cols_each_block = n_cols_each_block;
	ret_sealed_divided_matrix->sz_each_plain_block = sz_each_plain_block;
	ret_sealed_divided_matrix->sz_each_sealed_block = sz_each_sealed_block;
	ret_sealed_divided_matrix->sz_itself = sz_sealed_divided_matrix;

	for (uint32_t i = 0; i < (n_rows_blocks * n_cols_blocks); i++) {
		ret_sealed_divided_matrix->sealed_blocks[i] = (sealed_block_matrix_t*)malloc(sz_each_sealed_block);
	}

	return ret_sealed_divided_matrix;
}

uint32_t load_sealed_divided_matrix(
	sgx_enclave_id_t eid,
	sealed_divided_matrix_t* sealed_divided_matrix,
	float* plain_matrix,
	size_t sz_plain_matrix)
{
	int n_elems_in_matrix = sealed_divided_matrix->n_rows * sealed_divided_matrix->n_cols;
	if (n_elems_in_matrix * sizeof(float) != sz_plain_matrix) {
		return ENCLAVE_ERROR_PARAMETERS; // It is not from enclave but let's use it temporarily.
	}

	int n_elems_each_blocks = sealed_divided_matrix->n_rows_each_block * sealed_divided_matrix->n_cols_each_block;
	float* temp_plain_block = (float*)malloc(sealed_divided_matrix->sz_each_plain_block);

	uint32_t ret_sealing = ENCLAVE_SUCCESS;
	size_t n_rows_in_block, n_cols_in_block;
	size_t idx_sealing_block = 0;

	FOR(idx_row_block, sealed_divided_matrix->n_rows_blocks) FOR(idx_col_block, sealed_divided_matrix->n_cols_blocks) {
		// Take care of the corner cases

		// The bottommost blocks
		int bottommost_board = (idx_row_block + 1) * sealed_divided_matrix->n_rows_each_block;
		int exceeded_rows = bottommost_board - sealed_divided_matrix->n_rows;
		n_rows_in_block = sealed_divided_matrix->n_rows_each_block;
		if (exceeded_rows > 0) {
			n_rows_in_block = sealed_divided_matrix->n_rows_each_block - exceeded_rows;
		}

		// The rightmost blocks
		int rightmost_board = (idx_col_block + 1) * sealed_divided_matrix->n_cols_each_block;
		int exceeded_cols = rightmost_board - sealed_divided_matrix->n_cols;
		n_cols_in_block = sealed_divided_matrix->n_cols_each_block;
		if (exceeded_cols > 0) {
			n_cols_in_block = sealed_divided_matrix->n_cols_each_block - exceeded_cols;
		}
		int sz_cols_in_block = n_cols_in_block * sizeof(float);

		// Zeros-padding
		if ((exceeded_rows > 0) || (exceeded_cols > 0)) memset(temp_plain_block, 0, sealed_divided_matrix->sz_each_plain_block);

		// Fill in the temp block
		FOR(idx_row_in_block, n_rows_in_block) {
			int temp_offset = idx_row_in_block * sealed_divided_matrix->n_cols_each_block;
			int idx_row_in_matrix = ((idx_row_block * sealed_divided_matrix->n_rows_each_block) + idx_row_in_block);
			int idx_col_in_matrix = (idx_col_block * sealed_divided_matrix->n_cols_each_block);
			int plain_offset = idx_row_in_matrix * sealed_divided_matrix->n_cols + idx_col_in_matrix;
			memcpy(&temp_plain_block[temp_offset], &plain_matrix[plain_offset], sz_cols_in_block);
		}

		if (idx_sealing_block >= sealed_divided_matrix->n_rows_blocks * sealed_divided_matrix->n_cols_blocks) return ENCLAVE_ERROR_INDEX;

		enclave_create_sealed_block_matrix(
			eid, &ret_sealing,
			sealed_divided_matrix->sealed_blocks[idx_sealing_block++], sealed_divided_matrix->sz_each_sealed_block,
			sealed_divided_matrix->n_rows_each_block, sealed_divided_matrix->n_cols_each_block,
			temp_plain_block, sealed_divided_matrix->sz_each_plain_block);

		if (ret_sealing != ENCLAVE_SUCCESS) return ret_sealing;
	}

	free(temp_plain_block);

	return ENCLAVE_SUCCESS; // It is not from encalve
}


uint32_t destory_sealed_divided_matrix(sealed_divided_matrix_t* sealed_divided_matrix) {
	uint32_t M = sealed_divided_matrix->n_rows_blocks;
	uint32_t N = sealed_divided_matrix->n_cols_blocks;
	FOR(i, M*N) free(sealed_divided_matrix->sealed_blocks[i]);
	free(sealed_divided_matrix);
	return OCALL_SUCCESS;
}


uint32_t unseal_divided_matrix(
	sgx_enclave_id_t eid,
	sealed_divided_matrix_t* sealed_divided_matrix,
	float* plain_matrix, // Assume it was allocated
	size_t sz_plain_matrix)
{
	int n_elems_in_matrix = sealed_divided_matrix->n_rows * sealed_divided_matrix->n_cols;
	if (n_elems_in_matrix * sizeof(float) != sz_plain_matrix) {
		return ENCLAVE_ERROR_PARAMETERS; // It is not from enclave but let's use it temporarily.
	}

	int n_elems_each_blocks = sealed_divided_matrix->n_rows_each_block * sealed_divided_matrix->n_cols_each_block;
	float* temp_plain_block = (float*)malloc(sealed_divided_matrix->sz_each_plain_block);

	uint32_t ret_sealing = ENCLAVE_SUCCESS;
	size_t n_rows_in_block, n_cols_in_block;
	size_t idx_sealing_block = 0;

	FOR(idx_row_block, sealed_divided_matrix->n_rows_blocks) FOR(idx_col_block, sealed_divided_matrix->n_cols_blocks) {
		// Fill in the temp block
		if (idx_sealing_block >= sealed_divided_matrix->n_rows_blocks * sealed_divided_matrix->n_cols_blocks) return ENCLAVE_ERROR_INDEX;

		enclave_unseal_block_matrix(
			eid, &ret_sealing,
			sealed_divided_matrix->sealed_blocks[idx_sealing_block++], sealed_divided_matrix->sz_each_sealed_block,
			temp_plain_block, sealed_divided_matrix->sz_each_plain_block);

		if (ret_sealing != ENCLAVE_SUCCESS) return ret_sealing;

		// The bottommost blocks
		int bottommost_board = (idx_row_block + 1) * sealed_divided_matrix->n_rows_each_block;
		int exceeded_rows = bottommost_board - sealed_divided_matrix->n_rows;
		n_rows_in_block = sealed_divided_matrix->n_rows_each_block;
		if (exceeded_rows > 0) {
			n_rows_in_block = sealed_divided_matrix->n_rows_each_block - exceeded_rows;
		}

		// The rightmost blocks
		int rightmost_board = (idx_col_block + 1) * sealed_divided_matrix->n_cols_each_block;
		int exceeded_cols = rightmost_board - sealed_divided_matrix->n_cols;
		n_cols_in_block = sealed_divided_matrix->n_cols_each_block;
		if (exceeded_cols > 0) {
			n_cols_in_block = sealed_divided_matrix->n_cols_each_block - exceeded_cols;
		}
		int sz_cols_in_block = n_cols_in_block * sizeof(float);

		// Put back to plain matrix
		FOR(idx_row_in_block, n_rows_in_block) {
			int temp_offset = idx_row_in_block * sealed_divided_matrix->n_cols_each_block;
			int idx_row_in_matrix = ((idx_row_block * sealed_divided_matrix->n_rows_each_block) + idx_row_in_block);
			int idx_col_in_matrix = (idx_col_block * sealed_divided_matrix->n_cols_each_block);
			int plain_offset = idx_row_in_matrix * sealed_divided_matrix->n_cols + idx_col_in_matrix;
			memcpy(&plain_matrix[plain_offset], &temp_plain_block[temp_offset], sz_cols_in_block);
		}
	}

	free(temp_plain_block);

	return ENCLAVE_SUCCESS; // It is not from encalve
}

void sealed_gemm_with_functions(
	sgx_enclave_id_t eid, uint32_t* ret_proc,
	sealed_divided_matrix_t* sealed_A,
	sealed_divided_matrix_t* sealed_B,
	sealed_divided_matrix_t* sealed_C,
	uint32_t is_trans_A, uint32_t is_trans_B,
	float alpha, float beta,
	elem_funcname_t preA_function_name, elem_funcname_t preB_function_name,
	elem_funcname_t post_function_name)
{
	enclave_sealed_divided_gemm_with_functions(
		eid, ret_proc,
		sealed_A, sealed_A->sz_itself,
		sealed_B, sealed_B->sz_itself,
		sealed_C, sealed_C->sz_itself,
		is_trans_A, is_trans_B, alpha, beta,
		preA_function_name, preB_function_name,
		post_function_name);
}


// TODO: Implement the destructor of sealed_divided

sealed_tensor_t* create_empty_sealed_tensor(
	sgx_enclave_id_t eid,
	uint32_t n_batches, uint32_t n_channels,
	uint32_t n_rows, uint32_t n_cols)
{
	size_t sz_sealed_divided_tensor = sizeof(sealed_tensor_t);
	sz_sealed_divided_tensor += n_batches * n_channels * sizeof(sealed_block_matrix_t*);

	size_t sz_each_sealed_block;
	enclave_calc_sealed_block_matrix_size(eid, &sz_each_sealed_block, n_rows, n_cols);

	size_t sz_each_plain_block = n_rows * n_cols * sizeof(float);

	sealed_tensor_t* ret_sealed_divided_tensor = (sealed_tensor_t*)malloc(sz_sealed_divided_tensor);
	ret_sealed_divided_tensor->n_batches = n_batches;
	ret_sealed_divided_tensor->n_channels = n_channels;
	ret_sealed_divided_tensor->n_rows = n_rows;
	ret_sealed_divided_tensor->n_cols = n_cols;
	ret_sealed_divided_tensor->sz_each_plain_block = sz_each_plain_block;
	ret_sealed_divided_tensor->sz_each_sealed_block = sz_each_sealed_block;
	ret_sealed_divided_tensor->sz_itself = sz_sealed_divided_tensor;

	for (uint32_t i = 0; i < (n_batches * n_channels); i++) {
		ret_sealed_divided_tensor->sealed_blocks[i] = (sealed_block_matrix_t*)malloc(sz_each_sealed_block);
	}

	return ret_sealed_divided_tensor;
}

uint32_t get_tensor_index(
	sealed_tensor_t* sealed_tensor,
	uint32_t idx_batch, uint32_t idx_channel,
	uint32_t idx_row, uint32_t idx_col)
{
	uint32_t space_4 = sealed_tensor->n_cols;
	uint32_t space_3 = sealed_tensor->n_rows * space_4;
	uint32_t space_2 = sealed_tensor->n_channels * space_3;
	return idx_batch * space_2 + idx_channel * space_3 + idx_row * space_4 + idx_col;
}

uint32_t get_tensor_n_elem(sealed_tensor_t* sealed_tensor) {
	return sealed_tensor->n_batches * sealed_tensor->n_channels * sealed_tensor->n_rows * sealed_tensor->n_cols;
}

uint32_t destory_sealed_tensor(sealed_tensor_t* sealed_divided_tensor) {
	FOR(i, sealed_divided_tensor->n_batches * sealed_divided_tensor->n_channels) free(sealed_divided_tensor->sealed_blocks[i]);
	free(sealed_divided_tensor);
	return OCALL_SUCCESS;
}

uint32_t load_sealed_tensor(
	sgx_enclave_id_t eid,
	sealed_tensor_t* sealed_tensor,
	float* plain_tensor,
	size_t sz_plain_tensor)
{
	int n_elems_in_tensor =
		sealed_tensor->n_batches * sealed_tensor->n_channels *
		sealed_tensor->n_rows * sealed_tensor->n_cols;
	if (n_elems_in_tensor * sizeof(float) != sz_plain_tensor) {
		return ENCLAVE_ERROR_PARAMETERS; // It is not from enclave but let's use it temporarily.
	}

	int n_elems_each_blocks = sealed_tensor->n_rows * sealed_tensor->n_cols;
	// float* temp_plain_block = (float*)malloc(sealed_tensor->sz_each_plain_block);

	uint32_t ret_sealing = ENCLAVE_SUCCESS;
	size_t idx_sealing_block = 0;

	float* temp_plain_matrix = (float*)malloc(sealed_tensor->sz_each_plain_block);

	FOR(idx_batch, sealed_tensor->n_batches) FOR(idx_channel, sealed_tensor->n_channels) {

		if (idx_sealing_block >= sealed_tensor->n_batches * sealed_tensor->n_channels) return ENCLAVE_ERROR_INDEX;

		enclave_create_sealed_block_matrix(
			eid, &ret_sealing,
			sealed_tensor->sealed_blocks[idx_sealing_block], sealed_tensor->sz_each_sealed_block,
			sealed_tensor->n_rows, sealed_tensor->n_cols,
			&plain_tensor[idx_sealing_block * n_elems_each_blocks], sealed_tensor->sz_each_plain_block);

		idx_sealing_block++;

		if (ret_sealing != ENCLAVE_SUCCESS) return ret_sealing;
	}

	// free(temp_plain_block);

	return ENCLAVE_SUCCESS; // It is not from encalve
}

uint32_t unseal_tensor(
	sgx_enclave_id_t eid,
	sealed_tensor_t * sealed_tensor,
	float* plain_tensor, // Assume it was allocated
	size_t sz_plain_tensor)
{
	int n_elems_in_tensor =
		sealed_tensor->n_batches * sealed_tensor->n_channels *
		sealed_tensor->n_rows * sealed_tensor->n_cols;
	if (n_elems_in_tensor * sizeof(float) != sz_plain_tensor) {
		return ENCLAVE_ERROR_PARAMETERS; // It is not from enclave but let's use it temporarily.
	}

	int n_elems_each_blocks = sealed_tensor->n_rows * sealed_tensor->n_cols;
	// float* temp_plain_block = (float*)malloc(sealed_tensor->sz_each_plain_block);

	uint32_t ret_sealing = ENCLAVE_SUCCESS;
	size_t idx_sealing_block = 0;

	// float* temp_plain_block = (float*)malloc(sealed_tensor->sz_each_plain_block);

	FOR(idx_batch, sealed_tensor->n_batches) FOR(idx_channel, sealed_tensor->n_channels) {
		// Fill in the temp block
		if (idx_sealing_block >= sealed_tensor->n_batches * sealed_tensor->n_channels) return ENCLAVE_ERROR_INDEX;

		enclave_unseal_block_matrix(
			eid, &ret_sealing,
			sealed_tensor->sealed_blocks[idx_sealing_block], sealed_tensor->sz_each_sealed_block,
			&plain_tensor[idx_sealing_block * n_elems_each_blocks], sealed_tensor->sz_each_plain_block);
		// temp_plain_block, sealed_tensor->sz_each_plain_block);

		if (ret_sealing != ENCLAVE_SUCCESS) return ret_sealing;

		idx_sealing_block++;
		// memcpy(&plain_tensor[idx_sealing_block * n_elems_each_blocks], temp_plain_block, sealed_tensor->sz_each_plain_block);

	}

	// free(temp_plain_block);

	return ENCLAVE_SUCCESS; // It is not from encalve
}

uint32_t get_output_tensor_shape(
	uint32_t n_input_channels, uint32_t n_output_channels,
	uint32_t input_y, uint32_t input_x,
	uint32_t kernal_y, uint32_t kernal_x,
	uint32_t stride_y, uint32_t stride_x,
	uint32_t padding_y, uint32_t padding_x,
	uint32_t* output_y, uint32_t* output_x)
{
	*output_y = ((int)input_y - kernal_y + 2 * padding_y) / stride_y + 1;
	*output_x = ((int)input_x - kernal_x + 2 * padding_x) / stride_x + 1;
	return 0;
}

sealed_tensor_t* get_reversed_tensor(sealed_tensor_t* tensor_input) {
	sealed_tensor_t* res_tensor = (sealed_tensor_t*)malloc(tensor_input->sz_itself);
	memcpy(res_tensor, tensor_input, tensor_input->sz_itself);
	res_tensor->n_batches = tensor_input->n_channels;
	res_tensor->n_channels = tensor_input->n_batches;
	FOR(i, res_tensor->n_batches) FOR(j, res_tensor->n_channels) {
		res_tensor->sealed_blocks[i * res_tensor->n_channels + j] = tensor_input->sealed_blocks[j * tensor_input->n_channels + i];
	}
	return res_tensor;
}

tensor_layer_t* create_tensor_layer(
	sgx_enclave_id_t eid,
	uint32_t n_batches,
	uint32_t n_channels,
	uint32_t n_rows,
	uint32_t n_cols)
{
	tensor_layer_t* res_layer = (tensor_layer_t*)malloc(sizeof(tensor_layer_t));
	res_layer->n_batches = n_batches;
	res_layer->n_channels = n_channels;
	res_layer->n_rows = n_rows;
	res_layer->n_cols = n_cols;

	res_layer->sealed = create_empty_sealed_tensor(eid, n_batches, n_channels, n_rows, n_cols);
	res_layer->sealed_diff = create_empty_sealed_tensor(eid, n_batches, n_channels, n_rows, n_cols);
	res_layer->sealed_reversed = get_reversed_tensor(res_layer->sealed);
	res_layer->sealed_diff_reversed = get_reversed_tensor(res_layer->sealed_diff);

	res_layer->sz_plain = get_tensor_n_elem(res_layer->sealed) * sizeof(float);
	res_layer->plain = (float*)malloc(res_layer->sz_plain);
	res_layer->plain_diff = (float*)malloc(res_layer->sz_plain);

	return res_layer;
}

uint32_t destroy_tensor_layer(tensor_layer_t* layer) {
	destory_sealed_tensor(layer->sealed);
	destory_sealed_tensor(layer->sealed_diff);
	free(layer->sealed_reversed);
	free(layer->sealed_diff_reversed);
	free(layer->plain);
	free(layer->plain_diff);
	free(layer);

	return OCALL_SUCCESS;
}

uint32_t layer_load_sealed(sgx_enclave_id_t eid, tensor_layer_t* layer) {
	return load_sealed_tensor(eid, layer->sealed, layer->plain, layer->sz_plain);
}

uint32_t tensor_randomize_seal(sgx_enclave_id_t eid, tensor_layer_t* layer) {
	static std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> e{};
	std::normal_distribution<float> dist_fc2{ 0.0f, sqrtf(2.0f) / (float) sqrt(layer->n_rows + layer->n_cols) };

	uint32_t n_elem = layer->n_cols * layer->n_rows * layer->n_batches * layer->n_channels;
	FOR(i, n_elem) layer->plain[i] = dist_fc2(e);
	return layer_load_sealed(eid, layer);
}

uint32_t get_tensor_n_elem(tensor_layer_t* layer) {
	return layer->n_cols * layer->n_rows * layer->n_batches * layer->n_channels;
}

uint32_t tensor_alter(sgx_enclave_id_t eid, tensor_layer_t* layer, uint32_t pos, float delta) {
	unseal_tensor_layer(eid, layer, true);
	layer->plain[pos] += delta;
	return layer_load_sealed(eid, layer);
}

// is_normal : not diff tensor
uint32_t unseal_tensor_layer(sgx_enclave_id_t eid, tensor_layer_t* layer, bool is_normal) {
	if (is_normal) {
		return unseal_tensor(eid, layer->sealed, layer->plain, layer->sz_plain);
	} else {
		return unseal_tensor(eid, layer->sealed_diff, layer->plain_diff, layer->sz_plain);
	}
}

uint32_t unseal_matrix_layer(sgx_enclave_id_t eid, matrix_layer_t* layer, bool is_normal) {
	if (is_normal) {
		return unseal_divided_matrix(eid, layer->sealed, layer->plain, layer->sz_plain);
	} else {
		return unseal_divided_matrix(eid, layer->sealed_diff, layer->plain_diff, layer->sz_plain);
	}
}

matrix_layer_t* create_matrix_layer(
	sgx_enclave_id_t eid,
	uint32_t n_rows,
	uint32_t n_cols,
	uint32_t n_rows_each_block,
	uint32_t n_cols_each_block)
{
	matrix_layer_t* res_layer = (matrix_layer_t*)malloc(sizeof(matrix_layer_t));
	res_layer->n_rows = n_rows;
	res_layer->n_cols = n_cols;
	res_layer->n_rows_each_block = n_rows_each_block;
	res_layer->n_cols_each_block = n_cols_each_block;

	res_layer->sealed = create_empty_sealed_divided_matrix(eid, n_rows, n_cols, n_rows_each_block, n_cols_each_block);
	res_layer->sealed_diff = create_empty_sealed_divided_matrix(eid, n_rows, n_cols, n_rows_each_block, n_cols_each_block);

	res_layer->sz_plain = n_rows * n_cols * sizeof(float);
	res_layer->plain = (float*)malloc(res_layer->sz_plain);
	res_layer->plain_diff = (float*)malloc(res_layer->sz_plain);

	return res_layer;
}

matrix_layer_t* create_matrix_layer(
	sgx_enclave_id_t eid,
	uint32_t n_rows,
	uint32_t n_cols)
{
	return create_matrix_layer(
		eid,
		n_rows,
		n_cols,
		n_rows,
		n_cols);
}

uint32_t destroy_matrix_layer(matrix_layer_t* layer) {
	destory_sealed_divided_matrix(layer->sealed);
	destory_sealed_divided_matrix(layer->sealed_diff);
	free(layer->plain);
	free(layer->plain_diff);
	free(layer);

	return OCALL_SUCCESS;
}

uint32_t layer_load_sealed(sgx_enclave_id_t eid, matrix_layer_t* layer) {
	return load_sealed_divided_matrix(eid, layer->sealed, layer->plain, layer->sz_plain);
}

uint32_t matrix_randomize_seal(sgx_enclave_id_t eid, matrix_layer_t* layer) {
	static std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> e{1};
	std::normal_distribution<float> dist_fc2{ 0.0f, sqrtf(2.0f) / (float) sqrt(layer->n_rows + layer->n_cols) };

	// FOR(i, layer->n_rows) FOR(j, layer->n_cols) layer->plain[i * layer->n_cols + j] = a * i - b * j + bias;
	FOR(i, layer->n_rows) FOR(j, layer->n_cols) layer->plain[i * layer->n_cols + j] = dist_fc2(e);
	return layer_load_sealed(eid, layer);
}

uint32_t matrix_clean(sgx_enclave_id_t eid, matrix_layer_t* layer) {
	memset(layer->plain, 0, layer->sz_plain);
	return layer_load_sealed(eid, layer);
}

uint32_t tensor_clean(sgx_enclave_id_t eid, tensor_layer_t* layer) {
	memset(layer->plain, 0, layer->sz_plain);
	return layer_load_sealed(eid, layer);
}

fc_layer_t* create_fc_layer(
	matrix_layer_t* input_layer,
	matrix_layer_t* weight_layer,
	matrix_layer_t* bias_layer,
	matrix_layer_t* preact_layer,
	matrix_layer_t* output_layer) 
{
	fc_layer_t* res_layer = (fc_layer_t*)malloc(sizeof(fc_layer_t));
	res_layer->input_layer = input_layer;
	res_layer->weight_layer = weight_layer;
	res_layer->bias_layer = bias_layer;
	res_layer->preact_layer = preact_layer;
	res_layer->output_layer = output_layer;

	return res_layer;
}

uint32_t fc_forward(sgx_enclave_id_t eid, fc_layer_t* working_layer, bool need_activation) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;
	// Weighting
	sealed_gemm_with_functions(
		eid, &ret_proc,
		working_layer->input_layer->sealed,
		working_layer->weight_layer->sealed,
		working_layer->preact_layer->sealed,
		0, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	// Adding Bias
	enclave_sealed_divided_geadd(
		eid, &ret_proc,
		working_layer->bias_layer->sealed, working_layer->bias_layer->sealed->sz_itself,
		working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
		1.0f, 1.0f,
		ADD);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	// sigmoid(preact)
	if (need_activation) {
		enclave_sealed_divided_elem_bopa(
			eid, &ret_proc,
			working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
			working_layer->output_layer->sealed, working_layer->output_layer->sealed->sz_itself,
			SIGMOID);
		if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	}

	return ret_proc;
}

uint32_t fc_backward(sgx_enclave_id_t eid, fc_layer_t* working_layer, bool need_preact_diff, bool need_input_diff) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;
	// The diff of pre activation layer
	if (need_preact_diff) {
		enclave_sealed_divided_elem_copab_alpha_beta(
			eid, &ret_proc,
			working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
			working_layer->output_layer->sealed_diff, working_layer->output_layer->sealed_diff->sz_itself,
			working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
			0.0f, 0.0f,
			SIGMOID_DIFFA_B);
		if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	}

	// The diff of weighting
	sealed_gemm_with_functions(
		eid, &ret_proc,
		working_layer->input_layer->sealed,
		working_layer->preact_layer->sealed_diff,
		working_layer->weight_layer->sealed_diff,
		1, 0, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	// Compute the diff of input if needed
	if (need_input_diff) {
		sealed_gemm_with_functions(
			eid, &ret_proc,
			working_layer->preact_layer->sealed_diff,
			working_layer->weight_layer->sealed,
			working_layer->input_layer->sealed_diff,
			0, 1, 1.0, 0.0, IDENTICAL, IDENTICAL, IDENTICAL);
		if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	}

	return ret_proc;
}

uint32_t fc_update(sgx_enclave_id_t eid, fc_layer_t* working_layer, float learning_rate, float momentum) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;

	// update weight
	enclave_sealed_divided_geadd(
		eid, &ret_proc,
		working_layer->weight_layer->sealed_diff, working_layer->weight_layer->sealed_diff->sz_itself,
		working_layer->weight_layer->sealed, working_layer->weight_layer->sealed->sz_itself,
		-learning_rate, momentum,
		ADD);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	// update bias
	enclave_sealed_divided_geadd(
		eid, &ret_proc,
		working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
		working_layer->bias_layer->sealed, working_layer->bias_layer->sealed->sz_itself,
		-learning_rate, momentum,
		ADD);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	return ret_proc;
}

uint32_t fc_update(sgx_enclave_id_t eid, fc_layer_t* working_layer, float learning_rate) {
	return fc_update( eid, working_layer, learning_rate, 1.0f);
}

cnn_layer_t* create_cnn_layer(
	tensor_layer_t* input_layer,
	tensor_layer_t* kernal_layer,
	tensor_layer_t* bias_layer,
	tensor_layer_t* preact_layer,
	tensor_layer_t* acted_layer,
	tensor_layer_t* pooled_layer,
	uint32_t padding_y,
	uint32_t padding_x,
	uint32_t stride_y,
	uint32_t stride_x,
	uint32_t pooling_field_y,
	uint32_t pooling_field_x,
	uint32_t pooling_padding_y,
	uint32_t pooling_padding_x,
	uint32_t pooling_stride_y,
	uint32_t pooling_stride_x)
{
	cnn_layer_t* res_layer = (cnn_layer_t*)malloc(sizeof(cnn_layer_t));
	res_layer->input_layer = input_layer;
	res_layer->kernal_layer = kernal_layer;
	res_layer->bias_layer = bias_layer;
	res_layer->preact_layer = preact_layer;
	res_layer->acted_layer = acted_layer;
	res_layer->pooled_layer = pooled_layer;
	res_layer->padding_y = padding_y;
	res_layer->padding_x = padding_x;
	res_layer->stride_y = stride_y;
	res_layer->stride_x = stride_x;
	res_layer->pooling_field_y = pooling_field_y;
	res_layer->pooling_field_x = pooling_field_x;
	res_layer->pooling_padding_y = pooling_padding_y;
	res_layer->pooling_padding_x = pooling_padding_x;
	res_layer->pooling_stride_y = pooling_stride_y;
	res_layer->pooling_stride_x = pooling_stride_x;

	return res_layer;
}

uint32_t cnn_forward(sgx_enclave_id_t eid, cnn_layer_t* working_layer, bool need_bias) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;

	// Conv2: Conv -> Preact
	enclave_convolute_tensor_kernal(
		eid, &ret_proc,
		working_layer->input_layer->sealed, working_layer->input_layer->sealed->sz_itself,
		working_layer->kernal_layer->sealed, working_layer->kernal_layer->sealed->sz_itself,
		working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
		working_layer->padding_y, working_layer->padding_x,
		working_layer->stride_y, working_layer->stride_x,
		1.0f, 0.0f,
		0, 0);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	if (need_bias) {
		enclave_sealed_tensor_elem_copab_alpha_beta(
			eid, &ret_proc,
			working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
			working_layer->bias_layer->sealed, working_layer->bias_layer->sealed->sz_itself,
			working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
			1.0f, 1.0f,
			ADD);
		if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	}

	// Conv2: Preact -> Acted
	enclave_sealed_tensor_elem_bopa(
		eid, &ret_proc,
		working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
		working_layer->acted_layer->sealed, working_layer->acted_layer->sealed->sz_itself,
		SIGMOID);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	// Conv2: Acted -> Pooled
	enclave_tensor_pooling(
		eid, &ret_proc,
		working_layer->acted_layer->sealed, working_layer->acted_layer->sealed->sz_itself,
		working_layer->pooled_layer->sealed, working_layer->pooled_layer->sealed->sz_itself,
		NULL, 0,
		working_layer->pooling_field_y, working_layer->pooling_field_x,
		working_layer->pooling_padding_y, working_layer->pooling_padding_x,
		working_layer->pooling_stride_y, working_layer->pooling_stride_x,
		AVERAGE_POOLING);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	return ret_proc;
}

uint32_t cnn_backward(sgx_enclave_id_t eid, cnn_layer_t* working_layer, bool need_input_diff) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;

	enclave_tensor_pooling(
		eid, &ret_proc,
		working_layer->pooled_layer->sealed_diff, working_layer->pooled_layer->sealed_diff->sz_itself,
		working_layer->acted_layer->sealed_diff, working_layer->acted_layer->sealed_diff->sz_itself,
		NULL, 0,
		working_layer->pooling_field_y, working_layer->pooling_field_x,
		working_layer->pooling_padding_y, working_layer->pooling_padding_x,
		working_layer->pooling_stride_y, working_layer->pooling_stride_y,
		BACKWARD_AVERAGE_POOLING);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	enclave_sealed_tensor_elem_copab_alpha_beta(
		eid, &ret_proc,
		working_layer->preact_layer->sealed, working_layer->preact_layer->sealed->sz_itself,
		working_layer->acted_layer->sealed_diff, working_layer->acted_layer->sealed_diff->sz_itself,
		working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
		0.0f, 0.0f,
		SIGMOID_DIFFA_B);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	enclave_backward_convolute_tensor_kernal(
		eid, &ret_proc,
		working_layer->input_layer->sealed, working_layer->input_layer->sealed->sz_itself,
		working_layer->kernal_layer->sealed_diff, working_layer->kernal_layer->sealed_diff->sz_itself,
		working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
		working_layer->padding_y, working_layer->padding_x,
		working_layer->stride_y, working_layer->stride_x,
		1.0f, 0.0f);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	// Compute the diff of input if needed
	if (need_input_diff) {
		enclave_backward_convolute_tensor_input(
			eid, &ret_proc,
			working_layer->input_layer->sealed_diff, working_layer->input_layer->sealed_diff->sz_itself,
			working_layer->kernal_layer->sealed, working_layer->kernal_layer->sealed->sz_itself,
			working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
			working_layer->padding_y, working_layer->padding_x,
			working_layer->stride_y, working_layer->stride_x,
			1.0f, 0.0f);
		if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;
	}

	return ret_proc;
}

uint32_t cnn_update(sgx_enclave_id_t eid, cnn_layer_t* working_layer, float learning_rate, float momentum) {
	uint32_t ret_proc = ENCLAVE_SUCCESS;

	// update kernal
	enclave_sealed_tensor_elem_copab_alpha_beta(
		eid, &ret_proc,
		working_layer->kernal_layer->sealed, working_layer->kernal_layer->sealed->sz_itself,
		working_layer->kernal_layer->sealed_diff, working_layer->kernal_layer->sealed_diff->sz_itself,
		working_layer->kernal_layer->sealed, working_layer->kernal_layer->sealed->sz_itself,
		-learning_rate, momentum,
		ADD);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	// update bias
	enclave_sealed_tensor_elem_copab_alpha_beta(
		eid, &ret_proc,
		working_layer->bias_layer->sealed, working_layer->bias_layer->sealed->sz_itself,
		working_layer->preact_layer->sealed_diff, working_layer->preact_layer->sealed_diff->sz_itself,
		working_layer->bias_layer->sealed, working_layer->bias_layer->sealed->sz_itself,
		-learning_rate, momentum,
		ADD);
	if (ret_proc != ENCLAVE_SUCCESS) return ret_proc;

	return ret_proc;
}

uint32_t cnn_update(sgx_enclave_id_t eid, cnn_layer_t* working_layer, float learning_rate) {
	return cnn_update( eid, working_layer, learning_rate, 1.0f);
}
