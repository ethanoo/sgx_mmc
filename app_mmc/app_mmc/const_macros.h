#pragma once
#ifndef __MACROS_H__
#define __MACROS_H__

#include <stdio.h>
#include <stdlib.h>

typedef __int32 int32_t;

#define MAX(X_, Y_) (((X_) > (Y_)) ? (X_) : (Y_))
#define ABS(X_) (((X_) > 0) ? (X_) : -(X_))
#define CLIP_BY_VALUE(X_, A_, B_) ((X_) > (B_)) ? B_ : ((X_) < (A_) ? (A_)
#define FOR(i_, n_) for (int (i_)=0; (i_)<(n_); (i_)++)

#define LOG(fp, ...) \
do { \
  fprintf(fp, "%s - %s [%s:%d] ", __DATE__, __TIME__, __FILE__,  __LINE__); \
  fprintf(fp, __VA_ARGS__); \
  fprintf(fp, "\n"); \
} \
while (0)

#define INSTANTIATE_CLASS(classname) \
          char gInstantiationGuard##classname; \
          template class classname<float>; \
          template class classname<double>

#define LOG_ERROR(...) LOG(stderr, __VA_ARGS__)
#define LOG_INFO(...) LOG(stdout, __VA_ARGS__)

#define LOG_FATAL(...) LOG_ERROR(__VA_ARGS__); exit(EXIT_FAILURE);

#define CHECK(x) \
do { \
  if (!x) { \
    LOG_FATAL(#x " is false!"); \
  } \
} \
while (0)

#define CHECK_NOTNULL(x) CHECK(!(x == NULL))

#endif  /* __MACROS_H__ */

#define ALLOCATE_1D_ARRAY(type, var, x)                \
    do {                                               \
        (var)      = (type*) calloc(x, sizeof(type));  \
        if ((var) == NULL) {                           \
            perror("Error allocating memory.\n");      \
        }                                              \
    } while(0)

#define ALLOCATE_2D_ARRAY(type, var, x, y)                    \
    do {                                                      \
        (var)       = (type**) malloc(sizeof(type *) * (x));  \
        if ((var)  == NULL) {                                 \
            perror("Error allocating memory.\n");             \
        }                                                     \
        for (int i  = 0; i < x; i++) {                        \
            ALLOCATE_1D_ARRAY(type, (var)[i], y);             \
        }                                                     \
    } while(0)

#define ALLOCATE_3D_ARRAY(type, var, x, y, z)                   \
    do {                                                        \
        (var)       = (type***) malloc(sizeof(type **) * (x));  \
        if ((var)  == NULL) {                                   \
            perror("Error allocating memory.\n");               \
        }                                                       \
        for (int j  = 0; j < x; j++) {                          \
            ALLOCATE_2D_ARRAY(type, (var)[j], y, z);            \
        }                                                       \
    } while(0)

#define FREE_2D_ARRAY(var, x)          \
    do {                               \
        for (int i = 0; i < x; i++) {  \
            free((var)[i]);            \
        }                              \
        free(var);                     \
    } while(0)


#define FREE_3D_ARRAY(var, x, y)         \
    do {                                 \
        for (int j = 0; j < x; j++) {    \
            FREE_2D_ARRAY((var)[j], y);  \
        }                                \
        free(var);                       \
    } while(0)

#define DISPLAY_1D_ARRAY(indicator, var, x)  \
    do {                                     \
        int i;                               \
        for (i = 0; i < x; i++) {            \
            printf(indicator"\t", (var)[i]); \
        }                                    \
        printf("\n");                        \
    } while(0)

#define DISPLAY_2D_ARRAY(indicator, var, x, y)         \
    do {                                               \
        int _j;                                        \
        for (_j = 0; _j < x; _j++) {                   \
            DISPLAY_1D_ARRAY(indicator, (var)[_j], y); \
        }                                              \
    } while(0)

#define DISPLAY_2D_rawARRAY(indicator, var, n_rows_, n_cols_, p_n_rows_)    \
    do {                                                                    \
        int i_, j_;                                                         \
        for (i_     = 0; i_ < p_n_rows_; i_++) {                            \
            for (j_ = 0; j_ < n_cols_; j_++) {                              \
                printf(indicator"\t", *((var) + (j_) * (n_rows_) + (i_)));  \
            }                                                               \
            printf("\n");                                                   \
        }                                                                   \
    } while(0)

#define DEBUG_DISPLAY(indicator_, var_) printf(#var_/**/": "/**/indicator_"\n", (var_))

/***************************************************************************//**
																			 * Macros to handle error checking.
																			 */

#define TESTING_CHECK( err )                                                 \
    do {                                                                     \
        magma_int_t err_ = (err);                                            \
        if ( err_ != 0 ) {                                                   \
            fprintf( stderr, "Error: %s\nfailed at %s:%d: error %lld: %s\n", \
                     #err, __FILE__, __LINE__,                               \
                     (long long) err_, magma_strerror(err_) );               \
            exit(1);                                                         \
        }                                                                    \
    } while( 0 )
