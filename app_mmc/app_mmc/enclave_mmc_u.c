#include "enclave_mmc_u.h"
#include <errno.h>

typedef struct ms_enclave_calc_sealed_block_matrix_size_t {
	uint32_t ms_retval;
	uint32_t ms_n_rows;
	uint32_t ms_n_cols;
} ms_enclave_calc_sealed_block_matrix_size_t;

typedef struct ms_enclave_create_sealed_block_matrix_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_matrix;
	size_t ms_sz_sealed_matrix;
	uint32_t ms_n_rows;
	uint32_t ms_n_cols;
	float* ms_plain_matrix;
	size_t ms_sz_plain_matrix;
} ms_enclave_create_sealed_block_matrix_t;

typedef struct ms_enclave_unseal_block_matrix_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_matrix;
	size_t ms_sz_sealed_matrix;
	float* ms_plain_matrix;
	size_t ms_sz_plain_matrix;
} ms_enclave_unseal_block_matrix_t;

typedef struct ms_enclave_sealed_block_gemm_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_block_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_block_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
} ms_enclave_sealed_block_gemm_t;

typedef struct ms_enclave_sealed_divided_gemm_with_functions_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_preA_function_name;
	elem_funcname_t ms_preB_function_name;
	elem_funcname_t ms_post_function_name;
} ms_enclave_sealed_divided_gemm_with_functions_t;

typedef struct ms_enclave_sealed_divided_gemm_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	uint32_t ms_is_trans_A;
	uint32_t ms_is_trans_B;
	float ms_alpha;
	float ms_beta;
} ms_enclave_sealed_divided_gemm_t;

typedef struct ms_enclave_sealed_divided_elem_bopab_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_bopab_t;

typedef struct ms_enclave_sealed_divided_elem_copab_alpha_beta_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_divided_matrix_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_copab_alpha_beta_t;

typedef struct ms_enclave_sealed_divided_geadd_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_geadd_t;

typedef struct ms_enclave_sealed_divided_elem_bopa_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_divided_matrix_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_bopa_t;

typedef struct ms_enclave_sealed_divided_elem_aopa_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_divided_elem_aopa_t;

typedef struct ms_enclave_sealed_divided_aggregate_rows_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	float* ms_res_agg;
	size_t ms_sz_sealed_res_agg;
	elem_funcname_t ms_agg_func_name;
} ms_enclave_sealed_divided_aggregate_rows_t;

typedef struct ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	float* ms_res_agg;
	size_t ms_sz_res_agg;
	float* ms_pre_add;
	size_t ms_sz_pre_add;
} ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t;

typedef struct ms_enclave_sealed_gemm_t {
	uint32_t ms_retval;
	uint8_t* ms_sealed_A;
	uint8_t* ms_sealed_B;
	uint8_t* ms_sealed_C;
	uint32_t ms_is_A_trans;
	uint32_t ms_is_B_trans;
	float ms_alpha;
	float ms_beta;
	size_t ms_sz_sealed_A;
	size_t ms_sz_sealed_B;
	size_t ms_sz_sealed_C;
	size_t ms_sz_A;
	size_t ms_sz_B;
	size_t ms_sz_C;
	int ms_m;
	int ms_n;
	int ms_k;
} ms_enclave_sealed_gemm_t;

typedef struct ms_enclave_output_layer_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_trueY;
	size_t ms_sz_sealed_trueY;
	sealed_divided_matrix_t* ms_sealed_preY;
	size_t ms_sz_sealed_preY;
	sealed_divided_matrix_t* ms_sealed_diff_preY;
	size_t ms_sz_sealed_diff_preY;
	float* ms_loss;
} ms_enclave_output_layer_t;

typedef struct ms_enclave_output_layer_acc_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_trueY;
	size_t ms_sz_sealed_trueY;
	sealed_divided_matrix_t* ms_sealed_preY;
	size_t ms_sz_sealed_preY;
	float* ms_acc;
} ms_enclave_output_layer_acc_t;

typedef struct ms_enclave_to_zeros_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	int ms_n_elem;
} ms_enclave_to_zeros_t;

typedef struct ms_enclave_sealed_size_t {
	uint32_t ms_retval;
	uint32_t ms_add_mac_txt_size;
	uint32_t ms_txt_encrypt_size;
} ms_enclave_sealed_size_t;

typedef struct ms_enclave_seal_matrix_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	uint8_t* ms_sealed_data;
	uint32_t ms_sz_sealed_data;
} ms_enclave_seal_matrix_t;

typedef struct ms_enclave_unseal_matrix_t {
	uint32_t ms_retval;
	float* ms_A;
	size_t ms_sz_A;
	uint8_t* ms_sealed_data;
	size_t ms_sz_sealed_data;
} ms_enclave_unseal_matrix_t;

typedef struct ms_enclave_unseal_block_pointer_t {
	uint32_t ms_retval;
	sealed_block_matrix_t* ms_sealed_block;
	sealed_block_matrix_t* ms_retrieve_sealed_block;
	size_t ms_sz_sealed_block;
} ms_enclave_unseal_block_pointer_t;

typedef struct ms_enclave_convolute_tensor_kernal_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
	uint32_t ms_is_rot180_input;
	uint32_t ms_is_rot180_kernal;
} ms_enclave_convolute_tensor_kernal_t;

typedef struct ms_enclave_sealed_tensor_elem_copab_alpha_beta_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_tensor_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	sealed_tensor_t* ms_sealed_C;
	size_t ms_sz_sealed_C;
	float ms_alpha;
	float ms_beta;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_tensor_elem_copab_alpha_beta_t;

typedef struct ms_enclave_sealed_tensor_elem_bopa_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_A;
	size_t ms_sz_sealed_A;
	sealed_tensor_t* ms_sealed_B;
	size_t ms_sz_sealed_B;
	elem_funcname_t ms_func_name;
} ms_enclave_sealed_tensor_elem_bopa_t;

typedef struct ms_enclave_tensor_pooling_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	sealed_tensor_t* ms_sealed_auxiliary;
	size_t ms_sz_sealed_auxiliary;
	uint32_t ms_field_y;
	uint32_t ms_field_x;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	elem_funcname_t ms_pooling_name;
} ms_enclave_tensor_pooling_t;

typedef struct ms_enclave_tensor_flatten_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_divided_matrix_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
} ms_enclave_tensor_flatten_t;

typedef struct ms_enclave_tensor_backward_flatten_t {
	uint32_t ms_retval;
	sealed_divided_matrix_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
} ms_enclave_tensor_backward_flatten_t;

typedef struct ms_enclave_backward_convolute_tensor_kernal_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
} ms_enclave_backward_convolute_tensor_kernal_t;

typedef struct ms_enclave_backward_convolute_tensor_input_t {
	uint32_t ms_retval;
	sealed_tensor_t* ms_sealed_input;
	size_t ms_sz_sealed_input;
	sealed_tensor_t* ms_sealed_kernal;
	size_t ms_sz_sealed_kernal;
	sealed_tensor_t* ms_sealed_output;
	size_t ms_sz_sealed_output;
	uint32_t ms_padding_y;
	uint32_t ms_padding_x;
	uint32_t ms_stride_y;
	uint32_t ms_stride_x;
	float ms_alpha;
	float ms_beta;
} ms_enclave_backward_convolute_tensor_input_t;

typedef struct ms_get_memory_ocall_t {
	uint32_t ms_retval;
	uint8_t* ms_memory_ptr;
	uint8_t* ms_receive_memory;
	size_t ms_sz_memory;
} ms_get_memory_ocall_t;

typedef struct ms_place_memory_ocall_t {
	uint32_t ms_retval;
	uint8_t* ms_memory_from_enclave;
	uint8_t* ms_memory_in_untrusted;
	size_t ms_sz_memory;
} ms_place_memory_ocall_t;

typedef struct ms_sgx_oc_cpuidex_t {
	int* ms_cpuinfo;
	int ms_leaf;
	int ms_subleaf;
} ms_sgx_oc_cpuidex_t;

typedef struct ms_sgx_thread_wait_untrusted_event_ocall_t {
	int ms_retval;
	void* ms_self;
} ms_sgx_thread_wait_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_set_untrusted_event_ocall_t {
	int ms_retval;
	void* ms_waiter;
} ms_sgx_thread_set_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_setwait_untrusted_events_ocall_t {
	int ms_retval;
	void* ms_waiter;
	void* ms_self;
} ms_sgx_thread_setwait_untrusted_events_ocall_t;

typedef struct ms_sgx_thread_set_multiple_untrusted_events_ocall_t {
	int ms_retval;
	void** ms_waiters;
	size_t ms_total;
} ms_sgx_thread_set_multiple_untrusted_events_ocall_t;

static sgx_status_t SGX_CDECL enclave_mmc_get_memory_ocall(void* pms)
{
	ms_get_memory_ocall_t* ms = SGX_CAST(ms_get_memory_ocall_t*, pms);
	ms->ms_retval = get_memory_ocall(ms->ms_memory_ptr, ms->ms_receive_memory, ms->ms_sz_memory);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_place_memory_ocall(void* pms)
{
	ms_place_memory_ocall_t* ms = SGX_CAST(ms_place_memory_ocall_t*, pms);
	ms->ms_retval = place_memory_ocall(ms->ms_memory_from_enclave, ms->ms_memory_in_untrusted, ms->ms_sz_memory);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_sgx_oc_cpuidex(void* pms)
{
	ms_sgx_oc_cpuidex_t* ms = SGX_CAST(ms_sgx_oc_cpuidex_t*, pms);
	sgx_oc_cpuidex(ms->ms_cpuinfo, ms->ms_leaf, ms->ms_subleaf);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_sgx_thread_wait_untrusted_event_ocall(void* pms)
{
	ms_sgx_thread_wait_untrusted_event_ocall_t* ms = SGX_CAST(ms_sgx_thread_wait_untrusted_event_ocall_t*, pms);
	ms->ms_retval = sgx_thread_wait_untrusted_event_ocall((const void*)ms->ms_self);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_sgx_thread_set_untrusted_event_ocall(void* pms)
{
	ms_sgx_thread_set_untrusted_event_ocall_t* ms = SGX_CAST(ms_sgx_thread_set_untrusted_event_ocall_t*, pms);
	ms->ms_retval = sgx_thread_set_untrusted_event_ocall((const void*)ms->ms_waiter);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_sgx_thread_setwait_untrusted_events_ocall(void* pms)
{
	ms_sgx_thread_setwait_untrusted_events_ocall_t* ms = SGX_CAST(ms_sgx_thread_setwait_untrusted_events_ocall_t*, pms);
	ms->ms_retval = sgx_thread_setwait_untrusted_events_ocall((const void*)ms->ms_waiter, (const void*)ms->ms_self);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL enclave_mmc_sgx_thread_set_multiple_untrusted_events_ocall(void* pms)
{
	ms_sgx_thread_set_multiple_untrusted_events_ocall_t* ms = SGX_CAST(ms_sgx_thread_set_multiple_untrusted_events_ocall_t*, pms);
	ms->ms_retval = sgx_thread_set_multiple_untrusted_events_ocall((const void**)ms->ms_waiters, ms->ms_total);

	return SGX_SUCCESS;
}

static const struct {
	size_t nr_ocall;
	void * func_addr[7];
} ocall_table_enclave_mmc = {
	7,
	{
		(void*)(uintptr_t)enclave_mmc_get_memory_ocall,
		(void*)(uintptr_t)enclave_mmc_place_memory_ocall,
		(void*)(uintptr_t)enclave_mmc_sgx_oc_cpuidex,
		(void*)(uintptr_t)enclave_mmc_sgx_thread_wait_untrusted_event_ocall,
		(void*)(uintptr_t)enclave_mmc_sgx_thread_set_untrusted_event_ocall,
		(void*)(uintptr_t)enclave_mmc_sgx_thread_setwait_untrusted_events_ocall,
		(void*)(uintptr_t)enclave_mmc_sgx_thread_set_multiple_untrusted_events_ocall,
	}
};

sgx_status_t enclave_calc_sealed_block_matrix_size(sgx_enclave_id_t eid, uint32_t* retval, uint32_t n_rows, uint32_t n_cols)
{
	sgx_status_t status;
	ms_enclave_calc_sealed_block_matrix_size_t ms;
	ms.ms_n_rows = n_rows;
	ms.ms_n_cols = n_cols;
	status = sgx_ecall(eid, 0, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_create_sealed_block_matrix(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, uint32_t n_rows, uint32_t n_cols, float* plain_matrix, size_t sz_plain_matrix)
{
	sgx_status_t status;
	ms_enclave_create_sealed_block_matrix_t ms;
	ms.ms_sealed_matrix = sealed_matrix;
	ms.ms_sz_sealed_matrix = sz_sealed_matrix;
	ms.ms_n_rows = n_rows;
	ms.ms_n_cols = n_cols;
	ms.ms_plain_matrix = plain_matrix;
	ms.ms_sz_plain_matrix = sz_plain_matrix;
	status = sgx_ecall(eid, 1, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_unseal_block_matrix(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, float* plain_matrix, size_t sz_plain_matrix)
{
	sgx_status_t status;
	ms_enclave_unseal_block_matrix_t ms;
	ms.ms_sealed_matrix = sealed_matrix;
	ms.ms_sz_sealed_matrix = sz_sealed_matrix;
	ms.ms_plain_matrix = plain_matrix;
	ms.ms_sz_plain_matrix = sz_plain_matrix;
	status = sgx_ecall(eid, 2, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_block_gemm(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_A, size_t sz_sealed_A, sealed_block_matrix_t* sealed_B, size_t sz_sealed_B, sealed_block_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta)
{
	sgx_status_t status;
	ms_enclave_sealed_block_gemm_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_is_trans_A = is_trans_A;
	ms.ms_is_trans_B = is_trans_B;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	status = sgx_ecall(eid, 3, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_gemm_with_functions(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta, elem_funcname_t preA_function_name, elem_funcname_t preB_function_name, elem_funcname_t post_function_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_gemm_with_functions_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_is_trans_A = is_trans_A;
	ms.ms_is_trans_B = is_trans_B;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_preA_function_name = preA_function_name;
	ms.ms_preB_function_name = preB_function_name;
	ms.ms_post_function_name = post_function_name;
	status = sgx_ecall(eid, 4, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_gemm(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_gemm_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_is_trans_A = is_trans_A;
	ms.ms_is_trans_B = is_trans_B;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	status = sgx_ecall(eid, 5, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_elem_bopab(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_elem_bopab_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 6, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_elem_copab_alpha_beta(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_elem_copab_alpha_beta_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 7, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_geadd(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, float alpha, float beta, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_geadd_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 8, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_elem_bopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_elem_bopa_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 9, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_elem_aopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_elem_aopa_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 10, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_aggregate_rows(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_sealed_res_agg, elem_funcname_t agg_func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_aggregate_rows_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_res_agg = res_agg;
	ms.ms_sz_sealed_res_agg = sz_sealed_res_agg;
	ms.ms_agg_func_name = agg_func_name;
	status = sgx_ecall(eid, 11, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_divided_aggregate_rows_exponetial_add(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_res_agg, float* pre_add, size_t sz_pre_add)
{
	sgx_status_t status;
	ms_enclave_sealed_divided_aggregate_rows_exponetial_add_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_res_agg = res_agg;
	ms.ms_sz_res_agg = sz_res_agg;
	ms.ms_pre_add = pre_add;
	ms.ms_sz_pre_add = sz_pre_add;
	status = sgx_ecall(eid, 12, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_gemm(sgx_enclave_id_t eid, uint32_t* retval, uint8_t* sealed_A, uint8_t* sealed_B, uint8_t* sealed_C, uint32_t is_A_trans, uint32_t is_B_trans, float alpha, float beta, size_t sz_sealed_A, size_t sz_sealed_B, size_t sz_sealed_C, size_t sz_A, size_t sz_B, size_t sz_C, int m, int n, int k)
{
	sgx_status_t status;
	ms_enclave_sealed_gemm_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_is_A_trans = is_A_trans;
	ms.ms_is_B_trans = is_B_trans;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_sz_A = sz_A;
	ms.ms_sz_B = sz_B;
	ms.ms_sz_C = sz_C;
	ms.ms_m = m;
	ms.ms_n = n;
	ms.ms_k = k;
	status = sgx_ecall(eid, 13, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_output_layer(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, sealed_divided_matrix_t* sealed_diff_preY, size_t sz_sealed_diff_preY, float* loss)
{
	sgx_status_t status;
	ms_enclave_output_layer_t ms;
	ms.ms_sealed_trueY = sealed_trueY;
	ms.ms_sz_sealed_trueY = sz_sealed_trueY;
	ms.ms_sealed_preY = sealed_preY;
	ms.ms_sz_sealed_preY = sz_sealed_preY;
	ms.ms_sealed_diff_preY = sealed_diff_preY;
	ms.ms_sz_sealed_diff_preY = sz_sealed_diff_preY;
	ms.ms_loss = loss;
	status = sgx_ecall(eid, 14, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_output_layer_acc(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, float* acc)
{
	sgx_status_t status;
	ms_enclave_output_layer_acc_t ms;
	ms.ms_sealed_trueY = sealed_trueY;
	ms.ms_sz_sealed_trueY = sz_sealed_trueY;
	ms.ms_sealed_preY = sealed_preY;
	ms.ms_sz_sealed_preY = sz_sealed_preY;
	ms.ms_acc = acc;
	status = sgx_ecall(eid, 15, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_to_zeros(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, int n_elem)
{
	sgx_status_t status;
	ms_enclave_to_zeros_t ms;
	ms.ms_A = A;
	ms.ms_sz_A = sz_A;
	ms.ms_n_elem = n_elem;
	status = sgx_ecall(eid, 16, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_size(sgx_enclave_id_t eid, uint32_t* retval, uint32_t add_mac_txt_size, uint32_t txt_encrypt_size)
{
	sgx_status_t status;
	ms_enclave_sealed_size_t ms;
	ms.ms_add_mac_txt_size = add_mac_txt_size;
	ms.ms_txt_encrypt_size = txt_encrypt_size;
	status = sgx_ecall(eid, 17, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_seal_matrix(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, uint8_t* sealed_data, uint32_t sz_sealed_data)
{
	sgx_status_t status;
	ms_enclave_seal_matrix_t ms;
	ms.ms_A = A;
	ms.ms_sz_A = sz_A;
	ms.ms_sealed_data = sealed_data;
	ms.ms_sz_sealed_data = sz_sealed_data;
	status = sgx_ecall(eid, 18, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_unseal_matrix(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, uint8_t* sealed_data, size_t sz_sealed_data)
{
	sgx_status_t status;
	ms_enclave_unseal_matrix_t ms;
	ms.ms_A = A;
	ms.ms_sz_A = sz_A;
	ms.ms_sealed_data = sealed_data;
	ms.ms_sz_sealed_data = sz_sealed_data;
	status = sgx_ecall(eid, 19, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_unseal_block_pointer(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_block, sealed_block_matrix_t* retrieve_sealed_block, size_t sz_sealed_block)
{
	sgx_status_t status;
	ms_enclave_unseal_block_pointer_t ms;
	ms.ms_sealed_block = sealed_block;
	ms.ms_retrieve_sealed_block = retrieve_sealed_block;
	ms.ms_sz_sealed_block = sz_sealed_block;
	status = sgx_ecall(eid, 20, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_convolute_tensor_kernal(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta, uint32_t is_rot180_input, uint32_t is_rot180_kernal)
{
	sgx_status_t status;
	ms_enclave_convolute_tensor_kernal_t ms;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	ms.ms_sealed_kernal = sealed_kernal;
	ms.ms_sz_sealed_kernal = sz_sealed_kernal;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	ms.ms_padding_y = padding_y;
	ms.ms_padding_x = padding_x;
	ms.ms_stride_y = stride_y;
	ms.ms_stride_x = stride_x;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_is_rot180_input = is_rot180_input;
	ms.ms_is_rot180_kernal = is_rot180_kernal;
	status = sgx_ecall(eid, 21, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_tensor_elem_copab_alpha_beta(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, sealed_tensor_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_tensor_elem_copab_alpha_beta_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_sealed_C = sealed_C;
	ms.ms_sz_sealed_C = sz_sealed_C;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 22, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_sealed_tensor_elem_bopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name)
{
	sgx_status_t status;
	ms_enclave_sealed_tensor_elem_bopa_t ms;
	ms.ms_sealed_A = sealed_A;
	ms.ms_sz_sealed_A = sz_sealed_A;
	ms.ms_sealed_B = sealed_B;
	ms.ms_sz_sealed_B = sz_sealed_B;
	ms.ms_func_name = func_name;
	status = sgx_ecall(eid, 23, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_tensor_pooling(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_auxiliary, size_t sz_sealed_auxiliary, uint32_t field_y, uint32_t field_x, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, elem_funcname_t pooling_name)
{
	sgx_status_t status;
	ms_enclave_tensor_pooling_t ms;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	ms.ms_sealed_auxiliary = sealed_auxiliary;
	ms.ms_sz_sealed_auxiliary = sz_sealed_auxiliary;
	ms.ms_field_y = field_y;
	ms.ms_field_x = field_x;
	ms.ms_padding_y = padding_y;
	ms.ms_padding_x = padding_x;
	ms.ms_stride_y = stride_y;
	ms.ms_stride_x = stride_x;
	ms.ms_pooling_name = pooling_name;
	status = sgx_ecall(eid, 24, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_tensor_flatten(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output)
{
	sgx_status_t status;
	ms_enclave_tensor_flatten_t ms;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	status = sgx_ecall(eid, 25, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_tensor_backward_flatten(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_input, size_t sz_sealed_input)
{
	sgx_status_t status;
	ms_enclave_tensor_backward_flatten_t ms;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	status = sgx_ecall(eid, 26, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_backward_convolute_tensor_kernal(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta)
{
	sgx_status_t status;
	ms_enclave_backward_convolute_tensor_kernal_t ms;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	ms.ms_sealed_kernal = sealed_kernal;
	ms.ms_sz_sealed_kernal = sz_sealed_kernal;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	ms.ms_padding_y = padding_y;
	ms.ms_padding_x = padding_x;
	ms.ms_stride_y = stride_y;
	ms.ms_stride_x = stride_x;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	status = sgx_ecall(eid, 27, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t enclave_backward_convolute_tensor_input(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta)
{
	sgx_status_t status;
	ms_enclave_backward_convolute_tensor_input_t ms;
	ms.ms_sealed_input = sealed_input;
	ms.ms_sz_sealed_input = sz_sealed_input;
	ms.ms_sealed_kernal = sealed_kernal;
	ms.ms_sz_sealed_kernal = sz_sealed_kernal;
	ms.ms_sealed_output = sealed_output;
	ms.ms_sz_sealed_output = sz_sealed_output;
	ms.ms_padding_y = padding_y;
	ms.ms_padding_x = padding_x;
	ms.ms_stride_y = stride_y;
	ms.ms_stride_x = stride_x;
	ms.ms_alpha = alpha;
	ms.ms_beta = beta;
	status = sgx_ecall(eid, 28, &ocall_table_enclave_mmc, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

