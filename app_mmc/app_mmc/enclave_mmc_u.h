#ifndef ENCLAVE_MMC_U_H__
#define ENCLAVE_MMC_U_H__

#include <stdint.h>
#include <wchar.h>
#include <stddef.h>
#include <string.h>
#include "sgx_edger8r.h" /* for sgx_status_t etc. */

#include "stdlib.h"
#include "datatypes.h"

#define SGX_CAST(type, item) ((type)(item))

#ifdef __cplusplus
extern "C" {
#endif

uint32_t SGX_UBRIDGE(SGX_NOCONVENTION, get_memory_ocall, (uint8_t* memory_ptr, uint8_t* receive_memory, size_t sz_memory));
uint32_t SGX_UBRIDGE(SGX_NOCONVENTION, place_memory_ocall, (uint8_t* memory_from_enclave, uint8_t* memory_in_untrusted, size_t sz_memory));
void SGX_UBRIDGE(SGX_CDECL, sgx_oc_cpuidex, (int cpuinfo[4], int leaf, int subleaf));
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_wait_untrusted_event_ocall, (const void* self));
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_set_untrusted_event_ocall, (const void* waiter));
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_setwait_untrusted_events_ocall, (const void* waiter, const void* self));
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_set_multiple_untrusted_events_ocall, (const void** waiters, size_t total));

sgx_status_t enclave_calc_sealed_block_matrix_size(sgx_enclave_id_t eid, uint32_t* retval, uint32_t n_rows, uint32_t n_cols);
sgx_status_t enclave_create_sealed_block_matrix(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, uint32_t n_rows, uint32_t n_cols, float* plain_matrix, size_t sz_plain_matrix);
sgx_status_t enclave_unseal_block_matrix(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_matrix, size_t sz_sealed_matrix, float* plain_matrix, size_t sz_plain_matrix);
sgx_status_t enclave_sealed_block_gemm(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_A, size_t sz_sealed_A, sealed_block_matrix_t* sealed_B, size_t sz_sealed_B, sealed_block_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta);
sgx_status_t enclave_sealed_divided_gemm_with_functions(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta, elem_funcname_t preA_function_name, elem_funcname_t preB_function_name, elem_funcname_t post_function_name);
sgx_status_t enclave_sealed_divided_gemm(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, uint32_t is_trans_A, uint32_t is_trans_B, float alpha, float beta);
sgx_status_t enclave_sealed_divided_elem_bopab(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
sgx_status_t enclave_sealed_divided_elem_copab_alpha_beta(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, sealed_divided_matrix_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name);
sgx_status_t enclave_sealed_divided_geadd(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, float alpha, float beta, elem_funcname_t func_name);
sgx_status_t enclave_sealed_divided_elem_bopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, sealed_divided_matrix_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
sgx_status_t enclave_sealed_divided_elem_aopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, elem_funcname_t func_name);
sgx_status_t enclave_sealed_divided_aggregate_rows(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_sealed_res_agg, elem_funcname_t agg_func_name);
sgx_status_t enclave_sealed_divided_aggregate_rows_exponetial_add(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_A, size_t sz_sealed_A, float* res_agg, size_t sz_res_agg, float* pre_add, size_t sz_pre_add);
sgx_status_t enclave_sealed_gemm(sgx_enclave_id_t eid, uint32_t* retval, uint8_t* sealed_A, uint8_t* sealed_B, uint8_t* sealed_C, uint32_t is_A_trans, uint32_t is_B_trans, float alpha, float beta, size_t sz_sealed_A, size_t sz_sealed_B, size_t sz_sealed_C, size_t sz_A, size_t sz_B, size_t sz_C, int m, int n, int k);
sgx_status_t enclave_output_layer(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, sealed_divided_matrix_t* sealed_diff_preY, size_t sz_sealed_diff_preY, float* loss);
sgx_status_t enclave_output_layer_acc(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_trueY, size_t sz_sealed_trueY, sealed_divided_matrix_t* sealed_preY, size_t sz_sealed_preY, float* acc);
sgx_status_t enclave_to_zeros(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, int n_elem);
sgx_status_t enclave_sealed_size(sgx_enclave_id_t eid, uint32_t* retval, uint32_t add_mac_txt_size, uint32_t txt_encrypt_size);
sgx_status_t enclave_seal_matrix(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, uint8_t* sealed_data, uint32_t sz_sealed_data);
sgx_status_t enclave_unseal_matrix(sgx_enclave_id_t eid, uint32_t* retval, float* A, size_t sz_A, uint8_t* sealed_data, size_t sz_sealed_data);
sgx_status_t enclave_unseal_block_pointer(sgx_enclave_id_t eid, uint32_t* retval, sealed_block_matrix_t* sealed_block, sealed_block_matrix_t* retrieve_sealed_block, size_t sz_sealed_block);
sgx_status_t enclave_convolute_tensor_kernal(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta, uint32_t is_rot180_input, uint32_t is_rot180_kernal);
sgx_status_t enclave_sealed_tensor_elem_copab_alpha_beta(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, sealed_tensor_t* sealed_C, size_t sz_sealed_C, float alpha, float beta, elem_funcname_t func_name);
sgx_status_t enclave_sealed_tensor_elem_bopa(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_A, size_t sz_sealed_A, sealed_tensor_t* sealed_B, size_t sz_sealed_B, elem_funcname_t func_name);
sgx_status_t enclave_tensor_pooling(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_auxiliary, size_t sz_sealed_auxiliary, uint32_t field_y, uint32_t field_x, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, elem_funcname_t pooling_name);
sgx_status_t enclave_tensor_flatten(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output);
sgx_status_t enclave_tensor_backward_flatten(sgx_enclave_id_t eid, uint32_t* retval, sealed_divided_matrix_t* sealed_output, size_t sz_sealed_output, sealed_tensor_t* sealed_input, size_t sz_sealed_input);
sgx_status_t enclave_backward_convolute_tensor_kernal(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta);
sgx_status_t enclave_backward_convolute_tensor_input(sgx_enclave_id_t eid, uint32_t* retval, sealed_tensor_t* sealed_input, size_t sz_sealed_input, sealed_tensor_t* sealed_kernal, size_t sz_sealed_kernal, sealed_tensor_t* sealed_output, size_t sz_sealed_output, uint32_t padding_y, uint32_t padding_x, uint32_t stride_y, uint32_t stride_x, float alpha, float beta);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
