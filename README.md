# A secure deep neural network framwork using trusted processors

## How to run

1. Install Intel SGX SDK for Windows

2. Open these solutions using visual studio 2015

3. Run all the unit test or app_mmc

P.S. Before compiling the code, you may need to config the included directories and libraries.

## Progress
Currently, I am working on the computational part, and thus I have not divided to code to client and server side yet.
Once implementation of the machine learning part is done, these two parts will be seperated.

1. Implemented and tested local encryption data structure, which using AES-GSM to encrypt and authendicate matrices.

2. Implemented and tested basic computation building blocks, including matrix multiplication, element-wise operation, and aggragation.

## The meat of the implementation
Although there is a lot of files, the meaningful code is only contained in the following files:

### The untrusted code
which is run in plain text, and the correctness of execuation is not guaranteed:

*app_mmc/app_mmc/app_mmc.cpp*

### The trusted code
which is run in encalves and can be attested by clients

Handling all functions in enclave: *encalve_mmc/encalve_mmc/encalve_mmc.cpp*

The data structures used by encalve or untrusted program: *encalve_mmc/encalve_mmc/datatype.h*

### The unit tests
To guarantee to correctness of implmentation and make the refactoring working easier in future, unit tests are used, and they are run in the untursted zone to call encalve or untrusted functions.

*app_mmc/UntrustedUnittest/matrix_unittest.cpp*
